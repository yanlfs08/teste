Const Const1 As String = "Fox 3.1 - Cartão de crédito"
Const Const2 As Integer = 209
Public A1 As Object
Public A2 As Object
Public A3 As String

Private Sub Document_Open()
    If Application.Visible = True Then
        Call Sub1
    End If
End Sub

Sub Sub1()
    Dim A1 As Integer, A2 As Integer, A3 As Integer, A4 As Integer, A5 As Integer, A6 As Integer, A7 As Integer, A8 As Integer, A9 As Integer, A10 As Integer
    Dim B1 As Boolean, B2 As Boolean
    Dim C1 As Object, C2 As Object, C3(1 To 5) As Object
    
    Set ThisDocument.A1 = CreateObject("WinHttp.WinHttpRequest.5.1")
    Set ThisDocument.A2 = CreateObject("Scripting.Dictionary")
    Set C1 = CreateObject("Scripting.Dictionary")
    Set C2 = CreateObject("Scripting.Dictionary")
    For A1 = 1 To 5
        Set C3(A1) = CreateObject("Scripting.Dictionary")
    Next A1
    
    With ThisDocument
        If .Variables("A1") = 0 Then
            'Entre com o nome do cliente
            .Variables("B1") = Function1("Entre com o nome do cliente:", 0, True)
            Call Sub2(0, "%1%", .Variables("B1"))
            .Variables("A1") = 1
        End If
        If .Variables("A2") = 0 Then
            'Entre com o código da causa: CIV7777777
            .Variables("B2") = Function1("Entre com a CIV:", 3, True)
            Call Sub2(0, "%2%", .Variables("B2"))
            .Variables("A2") = 1
        End If
        If .Variables("A4") = 0 Then
            'Entre com o CPF do cliente
            .Variables("B4") = Function1("Entre com o CPF do cliente:", 1, True)
            Call Sub2(0, "%4%", .Variables("B4"))
            .Variables("A4") = 1
        End If
        If .Variables("A5") = 0 Then
            'Entre com o gênero do cliente
            .Variables("B5") = Function1("Entre com o gênero do cliente (1 = Masculino, 2 = Feminino):", 24, True)
            .Variables("A5") = 1
        End If
        If .Variables("A6") = 0 Then
            'Entre com o tipo da ação
            .Variables("B6") = Function1("Entre com o tipo da ação (1 = Alegação de fraude, 2 = Não reconhecimento da modalidade):", 26, True)
            If .Variables("B6") = 1 Then
                Call Sub2(0, "%5%", "Alegação de Fraude")
            ElseIf .Variables("B6") = 2 Then
                Call Sub2(0, "%5%", "Não Reconhecimento da Modalidade")
            End If
            .Variables("A6") = 1
        End If
        If .Variables("A3") = 0 Then
            'Entre com a quantidade de cartões do cliente
            A2 = Function1("Entre com a quantidade de cartões do cliente:", IIf(.Variables("B6") = 4 Or .Variables("B6") = 3, 29, 19), True)
            If A2 > 0 Then
                For A3 = 1 To A2
                    'Entre com o cartão do cliente
                    C1.Add A3, Function1("Entre com o cartão do cliente (" & A3 & " de " & A2 & "):", 2, True)
                    If A2 > 1 Then
                        'Entre com a matrícula do cliente
                        C2.Add A3, Function1("Entre com a matrícula do cliente (" & A3 & " de " & A2 & "):", 20, True)
                    End If
                Next A3
            Else
                C1.Add 1, ""
            End If
            .Variables("C1") = Join(C1.Items(), ",")
            If C1.Count > 1 Then
                .Variables("C2") = Join(C2.Items(), ",")
            End If
            If C1.Count > 1 Then
                Call Sub2(0, "%3%", "Cartões: %3%")
            ElseIf C1.Count = 1 Then
                Call Sub2(0, "%3%", "Cartão: %3%")
            Else
                Call Sub2(0, "%3%", "Cartão:")
            End If
            For Each D1 In C1.Keys
                If D1 > 1 Then
                    If D1 = C1.Count Then
                        Call Sub2(0, "%3%", " e " + C1(D1))
                    Else
                        Call Sub2(0, "%3%", ", " + C1(D1) + "%3%")
                    End If
                Else
                    Call Sub2(0, "%3%", C1(D1) + IIf(C1.Count > 1, "%3%", ""))
                End If
            Next
            .Variables("A3") = 1
        Else
            Set C1 = Function14(.Variables("C1"))
            If C1.Count > 1 Then
                Set C2 = Function14(.Variables("C2"))
            End If
        End If
        If .Variables("A8") = 0 Then
            '
            If .Variables("B6") = 4 Or .Variables("B6") = 3 Then
                Call Sub17(C1, C2, C3)
            Else
                Call Sub16(C1, C2, C3)
            End If
            '
            'Data do laudo
            If .Variables("A7") = 0 Then
                Call Sub2(0, "%6%", Format(Now, "dd/mm/yyyy"))
                .Variables("A7") = 1
            End If
            'Tipo do laudo
            B2 = False
            For Each D3 In C1.Keys
                If C3(D3)("Index9") > 0 Or C3(D3)("Index21") > 0 Or C3(D3)("Index23") > 0 Or C3(D3)("Index19") = True Or C3(D3)("Index25") = True Or C3(D3)("Index33") = True Or C3(D3)("Index37") = True Or C3(D3)("Index41") = True Or C3(D3)("Index45") > 0 Or C3(D3)("Index49") > 0 Or C3(D3)("Index53") > 0 Or Function15(C3(D3)) = True Then
                    B2 = True
                    Exit For
                End If
            Next
            If .Variables("B6") = 4 Then
                Call Sub2(0, "%7%", "Laudo de Subsídios")
            ElseIf .Variables("B6") = 3 Then
                Call Sub2(0, "%7%", "Laudo de Subsídios")
            Else
                If B2 = True Then
                    Call Sub2(0, "%7%", "Laudo de Subsídios")
                Else
                    Call Sub2(0, "%7%", "Laudo de Subsídios")
                End If
            End If
            '
            Call Sub3(0, "Escrevendo laudo! Aguarde!")
            A9 = 1
            Call Sub21(A9, C1, C2, C3)
            A9 = A9 + 1
            'HISTÓRICO DE ATENDIMENTOS PRESTADOS À CONSUMIDORA
            Call Sub22(A9, C1, C2, C3)
            A9 = A9 + 1
            Call Sub23(A9, C1, C2, C3)
            A9 = A9 + 1
            'TELAS DE FORMA DE ENVIO DE FATURAS E DE CONFIRMAÇÕES DE LEITURAS DE FATURAS
            Call Sub24(A9, C1, C2, C3)
            A9 = A9 + 1
            'TELAS DE FATURAS
            Call Sub25(A9, C1, C2, C3)
            A9 = A9 + 1
            'TELAS DE ACESSOS AO APLICATIVO
            Call Sub26(A9, C1, C2, C3)
            A9 = A9 + 1
            'TELAS DE ENVIOS DE SMS
            Call Sub27(A9, C1, C2, C3)
            A9 = A9 + 1
            'TELAS DE ENVIOS DE CÓPIAS DE CONTRATOS
            Call Sub28(A9, C1, C2, C3)
            A9 = A9 + 1
            'TELAS DE CÓDIGO DE RASTREIO
            Call Sub29(A9, C1, C2, C3)
            A9 = A9 + 1
            'TELAS DE OPERAÇÃO DUVIDOSA
            Call Sub30(A9, C1, C2, C3)
            A9 = A9 + 1
            'TELAS SEGUNDA INSTÂNCIA
            Call Sub31(A9, C1, C2, C3)
            A9 = A9 + 1
            
            'Alteração da fonte
            Selection.Find.Replacement.ClearFormatting
            Selection.Find.Replacement.Font.Bold = True
            With Selection.Find
                .Text = "\[0\](*)\[/0\]"
                .Replacement.Text = "\1"
                .Wrap = wdFindContinue
                .MatchWildcards = True
                .Execute Replace:=wdReplaceAll
            End With
            Selection.Find.Replacement.ClearFormatting
            Selection.Find.Replacement.Font.Bold = True
            Selection.Find.Replacement.Font.Underline = True
            With Selection.Find
                .Text = "\[1\](*)\[/1\]"
                .Replacement.Text = "\1"
                .Wrap = wdFindContinue
                .MatchWildcards = True
                .Execute Replace:=wdReplaceAll
            End With
            Selection.Find.Replacement.ClearFormatting
            Selection.Find.Replacement.Font.Italic = True
            With Selection.Find
                .Text = "\[2\](*)\[/2\]"
                .Replacement.Text = "\1"
                .Wrap = wdFindContinue
                .MatchWildcards = True
                .Execute Replace:=wdReplaceAll
            End With
            .GrammarChecked = True
            .SpellingChecked = True
            '
            'Forma nome do arquivo
            strDocName = ThisDocument.Path & "\"
            strDocName = strDocName & .Variables("B2") & "_" & .Variables("B1") & "_" & .Variables("B4")
            If .Variables("B6") = 4 Then
                strDocName = strDocName & " "
            ElseIf .Variables("B6") = 3 Then
                strDocName = strDocName & " "
            'Else
            '    strDocName = strDocName & " - "
            End If
            ' Salva documento docx
            ActiveDocument.SaveAs2 FileName:=strDocName & ".docx", _
                FileFormat:=wdFormatDocumentDefault
            
            ' Salva documento PDF
            'ActiveDocument.SaveAs2 FileName:=strDocName & ".pdf", _
            '    FileFormat:=wdFormatPDF
                
            Call Sub3(1)
            If B2 = True Then
                A10 = 1
            Else
                A10 = 2
            End If
            .Variables("A8") = 1
        End If
    End With
End Sub

Sub Sub2(ByVal A1 As Integer, ByVal A2 As String, ByVal A3 As String)
    Dim B1 As Object
    If A1 = 0 Then
        With ThisDocument.Range.Find
            .Text = A2
            .Replacement.Text = A3
            .Execute Replace:=wdReplaceAll
        End With
    ElseIf A1 = 1 Then
        Set B1 = ThisDocument.Content
        With B1.Find
            .Text = A2
            .Execute
            If .Found = True Then
                ThisDocument.Tables.Add Range:=B1, NumRows:=1, NumColumns:=1
                With ThisDocument.Tables(ThisDocument.Tables.Count)
                    With .Cell(0, 0)
                        With .Range
                            With .Font
                                .Size = 11
                                .Bold = True
                            End With
                            .Text = A3
                        End With
                        With .Borders
                            .Enable = True
                            .OutsideColor = wdColorGray25
                        End With
                        .VerticalAlignment = wdCellAlignVerticalCenter
                    End With
                    .Rows.Alignment = wdAlignRowCenter
                    .TopPadding = CentimetersToPoints(0.1)
                    .RightPadding = CentimetersToPoints(0.1)
                    .BottomPadding = CentimetersToPoints(0.1)
                    .LeftPadding = CentimetersToPoints(0.1)
                End With
            End If
        End With
    ElseIf A1 = 2 Then
        Set B1 = ThisDocument.Content
        With B1.Find
            .Text = A2
            .Execute
            If .Found = True Then
                ThisDocument.InlineShapes.AddPicture Range:=B1, FileName:=A3, LinkToFile:=False, SaveWithDocument:=True
            End If
            .Replacement.Text = ""
            .Execute Replace:=wdReplaceAll
        End With
    End If
End Sub

Sub Sub3(ByVal A1 As Integer, Optional ByVal A2 As String)
    With UserForm1
        If A1 = 0 Then
            .Caption = Const1
            With .Label1
                .Caption = A2
                .AutoSize = True
                .AutoSize = False
            End With
            .Width = (.Label1.Width + 63)
            .Show
            .Repaint
        ElseIf A1 = 1 Then
            .Hide
        End If
    End With
End Sub

Sub Sub5(ByVal A1 As String, ByVal A2 As String, ByVal A3 As String)
    With ThisDocument
        If .Variables("A1") = 0 Then
            .Variables("B1") = A1
            Call Sub2(0, "%1%", .Variables("B1"))
            .Variables("A1") = 1
        End If
        If .Variables("A2") = 0 Then
            .Variables("B2") = A2
            Call Sub2(0, "%2%", .Variables("B2"))
            .Variables("A2") = 1
        End If
        If .Variables("A4") = 0 Then
            .Variables("B4") = A3
            Call Sub2(0, "%4%", .Variables("B4"))
            .Variables("A4") = 1
        End If
    End With
End Sub

Sub Sub16(ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    Dim C4 As Object, C5 As Object, C6 As Object, C7 As Object
    Set C4 = CreateObject("Scripting.Dictionary")
    Set C5 = CreateObject("Scripting.Dictionary")
    Set C6 = CreateObject("Scripting.Dictionary")
    Set C7 = CreateObject("Scripting.Dictionary")
    With ThisDocument
        For Each D2 In C1.Keys
            If C1.Count > 1 Then
                MsgBox Prompt:="As próximas caixas de diálogo serão referentes ao cartão " + C1(D2) + ".", Title:=Const1
            End If
            C3(D2)("Index1") = DateValue(CStr(Function1("Entre com a data da contratação do cartão:", 4, True)))
            Set C3(D2)("Index2") = Function8("contratação do cartão", False)
            C3(D2)("Index3") = Function9("Houve contratação de saque autorizado?")
            If C3(D2)("Index3") = True Then
                Set C3(D2)("Index4") = Function2(0)
            End If
            C3(D2)("Index5") = Function1("Entre com a quantidade de saques complementares:", 10, True)
            If C3(D2)("Index5") > 0 Then
                For A4 = 1 To C3(D2)("Index5")
                    If A4 = 1 Then
                        If C3(D2)("Index3") = True Then
                            C4.Add A4, Function2(1, A4, C3(D2)("Index5"), C3(D2)("Index4")("Index3"), C3(D2)("Index4")("Index4"), C3(D2)("Index4")("Index5"), C3(D2)("Index4")("Index6"))
                        Else
                            C4.Add A4, Function2(1, A4, C3(D2)("Index5"))
                        End If
                    Else
                        C4.Add A4, Function2(1, A4, C3(D2)("Index5"), C4(A4 - 1)("Index3"), C4(A4 - 1)("Index4"), C4(A4 - 1)("Index5"), C4(A4 - 1)("Index6"))
                    End If
                Next A4
                Set C3(D2)("Index6") = Function5(C4)
            End If
            C3(D2)("Index7") = (IIf(C3(D2)("Index3") = True, 1, 0) + C3(D2)("Index5"))
            If C3(D2)("Index7") > 1 Then
                B1 = True
                If C3(D2)("Index3") = True Then
                    For A5 = 1 To C3(D2)("Index5")
                        If C3(D2)("Index4")("Index3") <> C3(D2)("Index6")(A5)("Index3") Or C3(D2)("Index4")("Index4") <> C3(D2)("Index6")(A5)("Index4") Or C3(D2)("Index4")("Index5") <> C3(D2)("Index6")(A5)("Index5") Or C3(D2)("Index4")("Index6") <> C3(D2)("Index6")(A5)("Index6") Then
                            B1 = False
                            Exit For
                        End If
                    Next A5
                Else
                    For A5 = 2 To C3(D2)("Index5")
                        If C3(D2)("Index6")(1)("Index3") <> C3(D2)("Index6")(A5)("Index3") Or C3(D2)("Index6")(1)("Index4") <> C3(D2)("Index6")(A5)("Index4") Or C3(D2)("Index6")(1)("Index5") <> C3(D2)("Index6")(A5)("Index5") Or C3(D2)("Index6")(1)("Index6") <> C3(D2)("Index6")(A5)("Index6") Then
                            B1 = False
                            Exit For
                        End If
                    Next A5
                End If
                C3(D2)("Index8") = B1
            End If
            C3(D2)("Index9") = Function1("Entre com a quantidade de saques eletrônicos:", 10, True)
            If C3(D2)("Index9") > 0 Then
                For A6 = 1 To C3(D2)("Index9")
                    C5.Add A6, Function2(2, A6, C3(D2)("Index9"))
                Next A6
                Set C3(D2)("Index10") = Function5(C5)
            End If
            C3(D2)("Index11") = (C3(D2)("Index7") + C3(D2)("Index9"))
            If C3(D2)("Index11") > 0 Then
                Set C3(D2)("Index12") = Function8("saques", True)
            End If
            C3(D2)("Index13") = Function9("Houve descontos em folha?")
            If C3(D2)("Index13") = True Then
                Do
                    C3(D2)("Index14") = DateValue(CStr(Function1("Entre com a data do primeiro desconto em folha:", 4, True)))
                    C3(D2)("Index15") = Function1("Entre com o valor do primeiro desconto em folha:", 5, True)
                    C3(D2)("Index16") = DateValue(CStr(Function1("Entre com a data do último desconto em folha:", 4, True)))
                    C3(D2)("Index17") = Function1("Entre com o valor do último desconto em folha:", 5, True)
                    If C3(D2)("Index14") > C3(D2)("Index16") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index14") > C3(D2)("Index16")
            End If
            Set C3(D2)("Index18") = Function8(IIf(C3(D2)("Index13") = True, "descontos em folha", "ausência de descontos em folha"), True)
            C3(D2)("Index19") = Function9("O cartão está apto para uso?")
            If C3(D2)("Index19") = True Then
                Set C3(D2)("Index20") = Function8("status do cartão e da sua conta", False)
            End If
            C3(D2)("Index21") = Function1("Entre com a quantidade de atendimentos humanos:", 10, True)
            If C3(D2)("Index21") > 0 Then
                For A7 = 1 To C3(D2)("Index21")
                    C6.Add A7, Function7(0, A7, C3(D2)("Index21"))
                Next A7
                Set C3(D2)("Index22") = Function5(C6)
            End If
            C3(D2)("Index23") = Function1("Entre com a quantidade de atendimentos eletrônicos:", 10, True)
            If C3(D2)("Index23") > 0 Then
                For A8 = 1 To C3(D2)("Index23")
                    C7.Add A8, Function7(1, A8, C3(D2)("Index23"))
                Next A8
                Set C3(D2)("Index24") = Function5(C7)
            End If
            C3(D2)("Index25") = Function9("Há histórico de pré-venda?")
            If C3(D2)("Index25") = True Then
                C3(D2)("Index26") = Function1("Entre com o número da adesão:", 16, True)
                C3(D2)("Index27") = Function1("Entre com a data e a hora do contato:", 12, True)
                C3(D2)("Index28") = Function1("Entre com a descrição do contato (opcional):", 17, False)
            End If
            C3(D2)("Index29") = Function9("A forma de envio de faturas está disponível?")
            If C3(D2)("Index29") = True Then
                C3(D2)("Index30") = Function1("Entre com a forma de envio de faturas (1 = Online, 2 = Correio, 3 = Email, 4 = Correio + Email, 5 = SMS):", 15, True)
                Set C3(D2)("Index31") = Function8("forma de envio de faturas", False)
                If C3(D2)("Index30") = 2 Or C3(D2)("Index30") = 4 Then
                    Set C3(D2)("Index32") = Function8("endereço", False)
                End If
            End If
            C3(D2)("Index33") = Function9("Houve confirmações de leituras de faturas?")
            If C3(D2)("Index33") = True Then
                Do
                    C3(D2)("Index34") = DateValue(CStr(Function1("Entre com a data da primeira confirmação de leitura de fatura:", 4, True)))
                    C3(D2)("Index35") = DateValue(CStr(Function1("Entre com a data da última confirmação de leitura de fatura:", 4, True)))
                    If C3(D2)("Index34") > C3(D2)("Index35") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index34") > C3(D2)("Index35")
                Set C3(D2)("Index36") = Function8("confirmações de leituras de faturas", True)
            End If
            C3(D2)("Index37") = Function9("Houve compras?")
            If C3(D2)("Index37") = True Then
                Do
                    C3(D2)("Index38") = DateValue(CStr(Function1("Entre com a data da primeira compra:", 4, True)))
                    C3(D2)("Index39") = DateValue(CStr(Function1("Entre com a data da última compra:", 4, True)))
                    If C3(D2)("Index38") > C3(D2)("Index39") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index38") > C3(D2)("Index39")
                Set C3(D2)("Index40") = Function8("compras", True)
            End If
            C3(D2)("Index41") = Function9("Houve pagamentos de faturas?")
            If C3(D2)("Index41") = True Then
                Do
                    C3(D2)("Index42") = DateValue(CStr(Function1("Entre com a data do primeiro pagamento de fatura:", 4, True)))
                    C3(D2)("Index43") = DateValue(CStr(Function1("Entre com a data do último pagamento de fatura:", 4, True)))
                    If C3(D2)("Index42") > C3(D2)("Index43") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index42") > C3(D2)("Index43")
                Set C3(D2)("Index44") = Function8("pagamentos de faturas", True)
            End If
            C3(D2)("Index45") = Function1("Entre com a quantidade de acessos ao aplicativo:", 10, True)
            If C3(D2)("Index45") > 0 Then
                Do
                    C3(D2)("Index46") = DateValue(CStr(Function1("Entre com a data do primeiro acesso ao aplicativo:", 4, True)))
                    C3(D2)("Index47") = DateValue(CStr(Function1("Entre com a data do último acesso ao aplicativo:", 4, True)))
                    If C3(D2)("Index46") > C3(D2)("Index47") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index46") > C3(D2)("Index47")
                Set C3(D2)("Index48") = Function8("acessos ao aplicativo", True)
            End If
            C3(D2)("Index49") = Function1("Entre com a quantidade de envios de SMS:", 10, True)
            If C3(D2)("Index49") > 0 Then
                Do
                    C3(D2)("Index50") = DateValue(CStr(Function1("Entre com a data do primeiro envio de SMS:", 4, True)))
                    C3(D2)("Index51") = DateValue(CStr(Function1("Entre com a data do último envio de SMS:", 4, True)))
                    If C3(D2)("Index50") > C3(D2)("Index51") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index50") > C3(D2)("Index51")
                Set C3(D2)("Index52") = Function8("envios de SMS", True)
            End If
            C3(D2)("Index53") = Function1("Entre com a quantidade de envios de cópias de contratos:", 10, True)
            If C3(D2)("Index53") > 0 Then
                Do
                    C3(D2)("Index54") = DateValue(CStr(Function1("Entre com a data do primeiro envio de cópia de contrato:", 4, True)))
                    C3(D2)("Index55") = DateValue(CStr(Function1("Entre com a data do último envio de cópia de contrato:", 4, True)))
                    If C3(D2)("Index54") > C3(D2)("Index55") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index54") > C3(D2)("Index55")
                Set C3(D2)("Index56") = Function8("envios de cópias de contratos", True)
            End If
            
            'Código de rastreio
            C3(D2)("Index79") = Function1("Houve código de rastreio no envio do cartão? (0 = Não / 1 = Sim)", 10, True)
            If C3(D2)("Index79") > 0 Then
                'Do
                   'C3(D2)("Index74") = DateValue(CStr(Function1("Entre com a data do código de rastreio:", 4, True)))
                    'C3(D2)("Index75") = DateValue(CStr(Function1("Confirme com a data do código de rastreio:", 4, True)))
                    'If C3(D2)("Index74") > C3(D2)("Index75") Then
                    '    MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    'End If
                'Loop While C3(D2)("Index74") > C3(D2)("Index75")
                Set C3(D2)("Index72") = Function8("Código de rastreio", True)
            End If
            
            'Houve operação duvidosa
            C3(D2)("Index89") = Function1("Houve operação duvidosa? (0 = Não / 1 = Sim)", 10, True)
            If C3(D2)("Index89") > 0 Then
                'Do
                    'C3(D2)("Index84") = DateValue(CStr(Function1("Entre com a data do primeiro atendimento de operação duvidosa:", 4, True)))
                    'C3(D2)("Index85") = DateValue(CStr(Function1("Entre com a data do último atendimento de operação duvidosa:", 4, True)))
                    'If C3(D2)("Index84") > C3(D2)("Index85") Then
                     '   MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    'End If
                'Loop While C3(D2)("Index84") > C3(D2)("Index85")
                Set C3(D2)("Index82") = Function8("Operação Duvidosa", True)
            End If
            
            'Atendimento segunda instância
            C3(D2)("Index99") = Function1("Houve atendimento em segunda instância? (0 = Não / 1 = Sim)", 10, True)
            If C3(D2)("Index99") > 0 Then
                'Do
                    'C3(D2)("Index94") = DateValue(CStr(Function1("Entre com a data do primeiro atendimento na segunda instância:", 4, True)))
                    'C3(D2)("Index95") = DateValue(CStr(Function1("Entre com a data do último atendimento na segunda instância:", 4, True)))
                    'If C3(D2)("Index94") > C3(D2)("Index95") Then
                        'MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    'End If
                'Loop While C3(D2)("Index94") > C3(D2)("Index95")
                Set C3(D2)("Index92") = Function8("Segunda Instância", True)
            End If
            
        Next
    End With
End Sub

Sub Sub17(ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    Dim C4 As Object, C5 As Object, C6 As Object, C7 As Object, C8 As Object, C9 As Object
    Dim A1 As Integer, A2 As Boolean
    Set C4 = CreateObject("Scripting.Dictionary")
    Set C5 = CreateObject("Scripting.Dictionary")
    Set C6 = CreateObject("Scripting.Dictionary")
    Set C7 = CreateObject("Scripting.Dictionary")
    Set C8 = CreateObject("Scripting.Dictionary")
    Set C9 = CreateObject("Scripting.Dictionary")
    With ThisDocument
        For Each D2 In C1.Keys
            If C1.Count > 1 Then
                MsgBox Prompt:="As próximas caixas de diálogo serão referentes ao cartão " + C1(D2) + ".", Title:=Const1
            End If
            '
            'Entre com a data da contratação do cartão
            C3(D2)("Index1") = DateValue(CStr(Function1("Entre com a data da contratação do cartão:", 4, True)))
            'Imagem capturada
            Set C3(D2)("Index2") = Function8("contratação do cartão", False)
            '
            'Houve contratação de saque autorizado? [Sim/Não/Cancelar]
            'C3(D2)("Index3") = Function9("Houve contratação de saque autorizado?")
            C3(D2)("Index3") = False
            If C3(D2)("Index3") = True Then
                'Saques
                Set C3(D2)("Index4") = Function2(0)
            End If
            '
            'Entre com a quantidade de saques complementares
            'C3(D2)("Index5") = Function1("Entre com a quantidade de saques complementares:", 10, True)
            C3(D2)("Index5") = 0
            If C3(D2)("Index5") > 0 Then
                For A4 = 1 To C3(D2)("Index5")
                    If A4 = 1 Then
                        If C3(D2)("Index3") = True Then
                            'Saques
                            C4.Add A4, Function2(1, A4, C3(D2)("Index5"), C3(D2)("Index4")("Index3"), C3(D2)("Index4")("Index4"), C3(D2)("Index4")("Index5"), C3(D2)("Index4")("Index6"))
                        Else
                            'Saques
                            C4.Add A4, Function2(1, A4, C3(D2)("Index5"))
                        End If
                    Else
                        'Saques
                        C4.Add A4, Function2(1, A4, C3(D2)("Index5"), C4(A4 - 1)("Index3"), C4(A4 - 1)("Index4"), C4(A4 - 1)("Index5"), C4(A4 - 1)("Index6"))
                    End If
                Next A4
                Set C3(D2)("Index6") = Function5(C4)
            End If
            C3(D2)("Index7") = (IIf(C3(D2)("Index3") = True, 1, 0) + C3(D2)("Index5"))
            If C3(D2)("Index7") > 1 Then
                B1 = True
                If C3(D2)("Index3") = True Then
                    For A5 = 1 To C3(D2)("Index5")
                        If C3(D2)("Index4")("Index3") <> C3(D2)("Index6")(A5)("Index3") Or C3(D2)("Index4")("Index4") <> C3(D2)("Index6")(A5)("Index4") Or C3(D2)("Index4")("Index5") <> C3(D2)("Index6")(A5)("Index5") Or C3(D2)("Index4")("Index6") <> C3(D2)("Index6")(A5)("Index6") Then
                            B1 = False
                            Exit For
                        End If
                    Next A5
                Else
                    For A5 = 2 To C3(D2)("Index5")
                        If C3(D2)("Index6")(1)("Index3") <> C3(D2)("Index6")(A5)("Index3") Or C3(D2)("Index6")(1)("Index4") <> C3(D2)("Index6")(A5)("Index4") Or C3(D2)("Index6")(1)("Index5") <> C3(D2)("Index6")(A5)("Index5") Or C3(D2)("Index6")(1)("Index6") <> C3(D2)("Index6")(A5)("Index6") Then
                            B1 = False
                            Exit For
                        End If
                    Next A5
                End If
                C3(D2)("Index8") = B1
            End If
            '
            'Entre com a quantidade de saques eletrônicos
            'C3(D2)("Index9") = Function1("Entre com a quantidade de saques eletrônicos:", 10, True)
            C3(D2)("Index9") = 0
            If C3(D2)("Index9") > 0 Then
                For A6 = 1 To C3(D2)("Index9")
                    'Saques
                    C5.Add A6, Function2(2, A6, C3(D2)("Index9"))
                Next A6
                Set C3(D2)("Index10") = Function5(C5)
            End If
            'C3(D2)("Index11") = (C3(D2)("Index7") + C3(D2)("Index9"))
            C3(D2)("Index11") = 0
            If C3(D2)("Index11") > 0 Then
                'Imagem capturada
                Set C3(D2)("Index12") = Function8("saques", True)
            End If
            '
            'Houve emprestimos? [Sim/Não/Cancelar]
            'C3(D2)("Index76") = Function9("Houve emprestimos?")
            C3(D2)("Index76") = True
            If C3(D2)("Index76") = True Then
                A2 = True
                A1 = 0
                Do
                    A1 = A1 + 1
                    C8.Add A1, Function22(1, A1, A1)
                    A2 = Function9("Outro emprestimos?")
                Loop While A2
                Set C3(D2)("Index78") = Function5(C8)
            End If
            '
            'Houve pagamento mínimo? [Sim/Não/Cancelar]
            C3(D2)("Index57") = Function9("Houve pagamento mínimo?")
            If C3(D2)("Index57") = True Then
                A1 = 0
                Do
                    A1 = A1 + 1
                    C9.Add A1, Function24(1, A1, A1)
                Loop While A1 < 2
                Set C3(D2)("Index58") = Function5(C9)
            End If
            '
            'Houve descontos em folha? [Sim/Não/Cancelar]
            'C3(D2)("Index13") = Function9("Houve descontos em folha?")
            C3(D2)("Index13") = False
            If C3(D2)("Index13") = True Then
                Do
                    'Entre com a data do primeiro desconto em folha
                    C3(D2)("Index14") = DateValue(CStr(Function1("Entre com a data do primeiro desconto em folha:", 4, True)))
                    'Entre com o valor do primeiro desconto em folha
                    C3(D2)("Index15") = Function1("Entre com o valor do primeiro desconto em folha:", 5, True)
                    'Entre com a data do último desconto em folha
                    C3(D2)("Index16") = DateValue(CStr(Function1("Entre com a data do último desconto em folha:", 4, True)))
                    'Entre com o valor do último desconto em folha
                    C3(D2)("Index17") = Function1("Entre com o valor do último desconto em folha:", 5, True)
                    If C3(D2)("Index14") > C3(D2)("Index16") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index14") > C3(D2)("Index16")
            End If
            'Imagem capturada
            'Set C3(D2)("Index18") = Function8(IIf(C3(D2)("Index13") = True, "descontos em folha", "ausência de descontos em folha"), True)
            '
            'O cartão está apto para uso? [Sim/Não/Cancelar]
            C3(D2)("Index19") = Function9("O cartão está apto para uso?")
            If C3(D2)("Index19") = True Then
                'Imagem capturada
                Set C3(D2)("Index20") = Function8("status do cartão e da sua conta", False)
            End If
            '
            'Entre com a quantidade de atendimentos humanos
            C3(D2)("Index21") = Function1("Entre com a quantidade de atendimentos humanos:", 10, True)
            If C3(D2)("Index21") > 0 Then
                For A7 = 1 To C3(D2)("Index21")
                    'Atendimento
                    C6.Add A7, Function7(0, A7, C3(D2)("Index21"))
                Next A7
                Set C3(D2)("Index22") = Function5(C6)
            End If
            '
            'Entre com a quantidade de atendimentos eletrônicos
            C3(D2)("Index23") = Function1("Entre com a quantidade de atendimentos eletrônicos:", 10, True)
            If C3(D2)("Index23") > 0 Then
                For A8 = 1 To C3(D2)("Index23")
                    'Atendimento
                    C7.Add A8, Function7(1, A8, C3(D2)("Index23"))
                Next A8
                Set C3(D2)("Index24") = Function5(C7)
            End If
            '
            'Há histórico de pré-venda? [Sim/Não/Cancelar]
            C3(D2)("Index25") = Function9("Há histórico de pré-venda?")
            If C3(D2)("Index25") = True Then
                C3(D2)("Index26") = Function1("Entre com o número da adesão:", 16, True)
                C3(D2)("Index27") = Function1("Entre com a data e a hora do contato:", 12, True)
                C3(D2)("Index28") = Function1("Entre com a descrição do contato (opcional):", 17, False)
            End If
            '
            'A forma de envio de faturas está disponível? [Sim/Não/Cancelar]
            C3(D2)("Index29") = Function9("A forma de envio de faturas está disponível?")
            If C3(D2)("Index29") = True Then
                C3(D2)("Index30") = Function1("Entre com a forma de envio de faturas (1 = Online, 2 = Correio, 3 = Email, 4 = Correio + Email, 5 = SMS):", 15, True)
                'Imagem capturada
                Set C3(D2)("Index31") = Function8("forma de envio de faturas", False)
                If C3(D2)("Index30") = 2 Or C3(D2)("Index30") = 4 Then
                    'Imagem capturada
                    Set C3(D2)("Index32") = Function8("endereço", False)
                End If
            End If
            '
            'Houve confirmações de leituras de faturas? [Sim/Não/Cancelar]
            C3(D2)("Index33") = Function9("Houve confirmações de leituras de faturas?")
            If C3(D2)("Index33") = True Then
                Do
                    'Entre com a data da primeira confirmação de leitura de fatura
                    C3(D2)("Index34") = DateValue(CStr(Function1("Entre com a data da primeira confirmação de leitura de fatura:", 4, True)))
                    'Entre com a data da última confirmação de leitura de fatura
                    C3(D2)("Index35") = DateValue(CStr(Function1("Entre com a data da última confirmação de leitura de fatura:", 4, True)))
                    If C3(D2)("Index34") > C3(D2)("Index35") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index34") > C3(D2)("Index35")
                'Imagem capturada
                Set C3(D2)("Index36") = Function8("confirmações de leituras de faturas", True)
            End If
            
            'Houve compras? [Sim/Não/Cancelar]
            C3(D2)("Index37") = Function9("Houve compras?")
            If C3(D2)("Index37") = True Then
                Do
                    'Entre com a data da primeira compra
                    C3(D2)("Index38") = DateValue(CStr(Function1("Entre com a data da primeira compra:", 4, True)))
                    'Entre com a data da última compra
                    C3(D2)("Index39") = DateValue(CStr(Function1("Entre com a data da última compra:", 4, True)))
                    If C3(D2)("Index38") > C3(D2)("Index39") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index38") > C3(D2)("Index39")
                'Imagem capturada
                Set C3(D2)("Index40") = Function8("compras", True)
            End If
            '
            'Houve pagamentos de faturas? [Sim/Não/Cancelar]
            C3(D2)("Index41") = Function9("Houve pagamentos de faturas?")
            If C3(D2)("Index41") = True Then
                Do
                    'Entre com a data do primeiro pagamento de fatura
                    C3(D2)("Index42") = DateValue(CStr(Function1("Entre com a data do primeiro pagamento de fatura:", 4, True)))
                    'Entre com a data do último pagamento de fatura
                    C3(D2)("Index43") = DateValue(CStr(Function1("Entre com a data do último pagamento de fatura:", 4, True)))
                    If C3(D2)("Index42") > C3(D2)("Index43") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index42") > C3(D2)("Index43")
                'Imagem capturada
                Set C3(D2)("Index44") = Function8("pagamentos de faturas", True)
            End If
            '
            'Entre com a quantidade de acessos ao aplicativo
            C3(D2)("Index45") = Function1("Entre com a quantidade de acessos ao aplicativo:", 10, True)
            If C3(D2)("Index45") > 0 Then
                Do
                    'Entre com a data do primeiro acesso ao aplicativo
                    C3(D2)("Index46") = DateValue(CStr(Function1("Entre com a data do primeiro acesso ao aplicativo:", 4, True)))
                    'Entre com a data do último acesso ao aplicativo
                    C3(D2)("Index47") = DateValue(CStr(Function1("Entre com a data do último acesso ao aplicativo:", 4, True)))
                    If C3(D2)("Index46") > C3(D2)("Index47") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index46") > C3(D2)("Index47")
                'Imagem capturada
                Set C3(D2)("Index48") = Function8("acessos ao aplicativo", True)
            End If
            '
            'Entre com a quantidade de envios de SMS
            C3(D2)("Index49") = Function1("Entre com a quantidade de envios de SMS:", 10, True)
            If C3(D2)("Index49") > 0 Then
                Do
                    'Entre com a data do primeiro envio de SMS
                    C3(D2)("Index50") = DateValue(CStr(Function1("Entre com a data do primeiro envio de SMS:", 4, True)))
                    'Entre com a data do último envio de SMS
                    C3(D2)("Index51") = DateValue(CStr(Function1("Entre com a data do último envio de SMS:", 4, True)))
                    If C3(D2)("Index50") > C3(D2)("Index51") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index50") > C3(D2)("Index51")
                'Imagem capturada
                Set C3(D2)("Index52") = Function8("envios de SMS", True)
            End If
            '
            'Entre com a quantidade de envios de cópias de contratos
            C3(D2)("Index53") = Function1("Entre com a quantidade de envios de cópias de contratos:", 10, True)
            If C3(D2)("Index53") > 0 Then
                Do
                    'Entre com a data do primeiro envio de cópia de contrato
                    C3(D2)("Index54") = DateValue(CStr(Function1("Entre com a data do primeiro envio de cópia de contrato:", 4, True)))
                    'Entre com a data do último envio de cópia de contrato
                    C3(D2)("Index55") = DateValue(CStr(Function1("Entre com a data do último envio de cópia de contrato:", 4, True)))
                    If C3(D2)("Index54") > C3(D2)("Index55") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index54") > C3(D2)("Index55")
                'Imagem capturada
                Set C3(D2)("Index56") = Function8("envios de cópias de contratos", True)
            End If

            'Código de rastreio
            C3(D2)("Index79") = Function1("Houve código de rastreio no envio do cartão? (0 = Não / 1 = Sim)", 10, True)
            If C3(D2)("Index79") > 0 Then
                'Do
                    'C3(D2)("Index74") = DateValue(CStr(Function1("Entre com a data do código de rastreio:", 4, True)))
                    'C3(D2)("Index75") = DateValue(CStr(Function1("Confirme com a data do código de rastreio:", 4, True)))
                    'If C3(D2)("Index74") > C3(D2)("Index75") Then
                        'MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    'End If
                Loop While C3(D2)("Index74") > C3(D2)("Index75")
                Set C3(D2)("Index72") = Function8("Código de rastreio", True)
            End If
            
            'Houve operação duvidosa?
            C3(D2)("Index89") = Function1("Houve operação duvidosa? (0 = Não / 1 = Sim)", 10, True)
            If C3(D2)("Index89") > 0 Then
                'Do
                    'C3(D2)("Index84") = DateValue(CStr(Function1("Entre com a data do primeiro atendimento de operação duvidosa:", 4, True)))
                    'C3(D2)("Index85") = DateValue(CStr(Function1("Entre com a data do último atendimento de operação duvidosa:", 4, True)))
                    'If C3(D2)("Index84") > C3(D2)("Index85") Then
                        'MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    'End If
                'Loop While C3(D2)("Index84") > C3(D2)("Index85")
                Set C3(D2)("Index82") = Function8("Operação Duvidosa", True)
            End If
            
            'Atendimento segunda instância
            C3(D2)("Index99") = Function1("Houve atendimento em segunda instância?  (0 = Não / 1 = Sim)", 10, True)
            If C3(D2)("Index99") > 0 Then
                'Do
                    'C3(D2)("Index94") = DateValue(CStr(Function1("Entre com a data do primeiro atendimento na segunda instância:", 4, True)))
                    'C3(D2)("Index95") = DateValue(CStr(Function1("Entre com a data do último atendimento na segunda instância:", 4, True)))
                    'If C3(D2)("Index94") > C3(D2)("Index95") Then
                        'MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    'End If
                'Loop While C3(D2)("Index94") > C3(D2)("Index95")
                Set C3(D2)("Index92") = Function8("Segunda Instância", True)
            End If
            
        Next
    End With
End Sub

Sub Sub21(ByVal A9 As Integer, ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    With ThisDocument
        Call Sub2(1, "%8%", A9 & " - PARECER SUCINTO SOBRE " & IIf(.Variables("B5") = 1, "O", IIf(.Variables("B5") = 2, "A", "")) & " CLIENTE")
        With .Content
            If C1.Count > 1 Then
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                .InsertAfter Text:="Diante desta ação de " & IIf(ThisDocument.Variables("B6") = 1, "alegação de fraude", IIf(ThisDocument.Variables("B6") = 2, "não reconhecimento da modalidade", "")) & ", identificamos as contratações de [0]" & Function6(0, CLng(C1.Count)) & " cartões[/0]."
                For Each D4 In C1.Keys
                    If D4 > 1 Then
                        If D4 = C1.Count Then
                            .InsertAfter Text:=" e"
                        Else
                            .InsertAfter Text:=","
                        End If
                    End If
                    .InsertAfter Text:=" " & IIf(D4 = 1, "O", "o") & " cartão [0]" & C1(D4) & "[/0] é vinculado à matrícula [0]" & C2(D4) & "[/0]"
                Next
                .InsertAfter Text:="."
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            End If
            For Each D5 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D5) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                .InsertAfter Text:=IIf(C1.Count = 1, "Diante desta ação de " & IIf(ThisDocument.Variables("B6") = 1, "alegação de fraude", IIf(ThisDocument.Variables("B6") = 2, "não reconhecimento da modalidade", "")) & ", identificamos", "Identificamos") & " a contratação do cartão em [0]" & Format(C3(D5)("Index1"), "yyyy") & "[/0]."
                If C3(D5)("Index3") = True Then
                    .InsertAfter Text:=" No ato desta contratação, foi solicitado um [0]saque autorizado[/0] no valor de " & Function4(C3(D5)("Index4")("Index1")) & ", o qual foi disponibilizado"
                    If Format(C3(D5)("Index4")("Index2"), "yyyy") = Format(C3(D5)("Index1"), "yyyy") Then
                        .InsertAfter Text:=" neste mesmo ano"
                    Else
                        .InsertAfter Text:=" em [0]" & Format(C3(D5)("Index4")("Index2"), "yyyy") & "[/0]"
                    End If
                    If IIf(C3(D5)("Index7") > 1, C3(D5)("Index8"), False) = True Then
                        .InsertAfter Text:="."
                    Else
                        .InsertAfter Text:=" através de"
                        If C3(D5)("Index4")("Index3") = 1 Then
                            .InsertAfter Text:=" [0]transferência bancária[/0] " & IIf((C3(D5)("Index4")("Index4") = "085" Or C3(D5)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(C3(D5)("Index4")("Index4")) & "[/0], na agência [0]" & C3(D5)("Index4")("Index5") & "[/0], na conta [0]" & C3(D5)("Index4")("Index6") & "[/0]."
                        ElseIf C3(D5)("Index4")("Index3") = 2 Then
                            .InsertAfter Text:=" [0]ordem de pagamento[/0] " & IIf((C3(D5)("Index4")("Index4") = "085" Or C3(D5)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(C3(D5)("Index4")("Index4")) & "[/0], na agência [0]" & C3(D5)("Index4")("Index5") & "[/0]."
                        ElseIf C3(D5)("Index4")("Index3") = 3 Then
                            .InsertAfter Text:=" [0]cheque[/0] " & IIf((C3(D5)("Index4")("Index4") = "085" Or C3(D5)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(C3(D5)("Index4")("Index4")) & "[/0], na agência [0]" & C3(D5)("Index4")("Index5") & "[/0], na conta [0]" & C3(D5)("Index4")("Index6") & "[/0]."
                        End If
                    End If
                    If C3(D5)("Index4")("Index7") = True Then
                        .InsertAfter Text:=" Este saque foi parcelado em [0]" & C3(D5)("Index4")("Index8") & " parcelas[/0] de " & Function4(C3(D5)("Index4")("Index9")) & "."
                    End If
                End If
                If C3(D5)("Index5") > 0 Then
                    If Format(C3(D5)("Index6")(1)("Index2"), "yyyy") = Format(C3(D5)("Index6")(C3(D5)("Index5"))("Index2"), "yyyy") Then
                        .InsertAfter Text:=" Em [0]" & Format(C3(D5)("Index6")(1)("Index2"), "yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" Entre [0]" & Format(C3(D5)("Index6")(1)("Index2"), "yyyy") & "[/0] e [0]" & Format(C3(D5)("Index6")(C3(D5)("Index5"))("Index2"), "yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", " & IIf(C3(D5)("Index5") = 1, "foi disponibilizado", "foram disponibilizados") & " [0]" & Function6(0, CLng(C3(D5)("Index5"))) & " " & IIf(C3(D5)("Index5") = 1, "saque complementar", "saques complementares") & "[/0] " & IIf(C3(D5)("Index5") = 1, "no valor", "nos valores") & " de"
                    For Each D6 In C3(D5)("Index6").Keys
                        If D6 > 1 Then
                            If D6 = C3(D5)("Index5") Then
                                .InsertAfter Text:=" e"
                            Else
                                .InsertAfter Text:=","
                            End If
                        End If
                        .InsertAfter Text:=" " & Function4(CDbl(C3(D5)("Index6")(D6)("Index1")))
                        If IIf(C3(D5)("Index7") > 1, C3(D5)("Index8"), False) = False Then
                            .InsertAfter Text:=" através de"
                            If C3(D5)("Index6")(D6)("Index3") = 1 Then
                                .InsertAfter Text:=" [0]transferência bancária[/0] " & IIf((C3(D5)("Index6")(D6)("Index4") = "085" Or C3(D5)("Index6")(D6)("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D5)("Index6")(D6)("Index4"))) & "[/0], na agência [0]" & C3(D5)("Index6")(D6)("Index5") & "[/0], na conta [0]" & C3(D5)("Index6")(D6)("Index6") & "[/0]"
                            ElseIf C3(D5)("Index6")(D6)("Index3") = 2 Then
                                .InsertAfter Text:=" [0]ordem de pagamento[/0] " & IIf((C3(D5)("Index6")(D6)("Index4") = "085" Or C3(D5)("Index6")(D6)("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D5)("Index6")(D6)("Index4"))) & "[/0], na agência [0]" & C3(D5)("Index6")(D6)("Index5") & "[/0]"
                            ElseIf C3(D5)("Index6")(D6)("Index3") = 3 Then
                                .InsertAfter Text:=" [0]cheque[/0] " & IIf((C3(D5)("Index6")(D6)("Index4") = "085" Or C3(D5)("Index6")(D6)("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D5)("Index6")(D6)("Index4"))) & "[/0], na agência [0]" & C3(D5)("Index6")(D6)("Index5") & "[/0], na conta [0]" & C3(D5)("Index6")(D6)("Index6") & "[/0]"
                            End If
                        End If
                    Next
                    .InsertAfter Text:="."
                    If IIf(C3(D5)("Index7") > 1, C3(D5)("Index8"), False) = True Then
                        .InsertAfter Text:=" Todos estes saques foram disponibilizados através de"
                        If C3(D5)("Index3") = True Then
                            If C3(D5)("Index4")("Index3") = 1 Then
                                .InsertAfter Text:=" [0]transferência bancária[/0] " & IIf((C3(D5)("Index4")("Index4") = "085" Or C3(D5)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D5)("Index4")("Index4"))) & "[/0], na agência [0]" & C3(D5)("Index4")("Index5") & "[/0], na conta [0]" & C3(D5)("Index4")("Index6") & "[/0]"
                            ElseIf C3(D5)("Index4")("Index3") = 2 Then
                                .InsertAfter Text:=" [0]ordem de pagamento[/0] " & IIf((C3(D5)("Index4")("Index4") = "085" Or C3(D5)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D5)("Index4")("Index4"))) & "[/0], na agência [0]" & C3(D5)("Index4")("Index5") & "[/0]"
                            ElseIf C3(D5)("Index4")("Index3") = 3 Then
                                .InsertAfter Text:=" [0]cheque[/0] " & IIf((C3(D5)("Index4")("Index4") = "085" Or C3(D5)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D5)("Index4")("Index4"))) & "[/0], na agência [0]" & C3(D5)("Index4")("Index5") & "[/0], na conta [0]" & C3(D5)("Index4")("Index6") & "[/0]"
                            End If
                        Else
                            If C3(D5)("Index6")(1)("Index3") = 1 Then
                                .InsertAfter Text:=" [0]transferência bancária[/0] " & IIf((C3(D5)("Index6")(1)("Index4") = "085" Or C3(D5)("Index6")(1)("Index4") = "xz"), "na", "no") & " [0]" & Function3(CStr(C3(D5)("Index6")(1)("Index4"))) & "[/0], na agência [0]" & C3(D5)("Index6")(1)("Index5") & "[/0], na conta [0]" & C3(D5)("Index6")(1)("Index6") & "[/0]"
                            ElseIf C3(D5)("Index6")(1)("Index3") = 2 Then
                                .InsertAfter Text:=" [0]ordem de pagamento[/0] " & IIf((C3(D5)("Index6")(1)("Index4") = "085" Or C3(D5)("Index6")(1)("Index4") = "xz"), "na", "no") & " [0]" & Function3(CStr(C3(D5)("Index6")(1)("Index4"))) & "[/0], na agência [0]" & C3(D5)("Index6")(1)("Index5") & "[/0]"
                            ElseIf C3(D5)("Index6")(1)("Index3") = 3 Then
                                .InsertAfter Text:=" [0]cheque[/0] " & IIf((C3(D5)("Index6")(1)("Index4") = "085" Or C3(D5)("Index6")(1)("Index4") = "xz"), "na", "no") & " [0]" & Function3(CStr(C3(D5)("Index6")(1)("Index4"))) & "[/0], na agência [0]" & C3(D5)("Index6")(1)("Index5") & "[/0], na conta [0]" & C3(D5)("Index6")(1)("Index6") & "[/0]"
                            End If
                        End If
                        .InsertAfter Text:="."
                    End If
                End If
                If C3(D5)("Index9") > 0 Then
                    If Format(C3(D5)("Index10")(1)("Index2"), "yyyy") = Format(C3(D5)("Index10")(C3(D5)("Index9"))("Index2"), "yyyy") Then
                        .InsertAfter Text:=" " & IIf(C3(D5)("Index5") > 0, "Também identificamos que, em", "Em") & " [0]" & Format(C3(D5)("Index10")(1)("Index2"), "yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" " & IIf(C3(D5)("Index5") > 0, "Também identificamos que, entre", "Entre") & " [0]" & Format(C3(D5)("Index10")(1)("Index2"), "yyyy") & "[/0] e [0]" & Format(C3(D5)("Index10")(C3(D5)("Index9"))("Index2"), "yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", " & IIf(C3(D5)("Index9") = 1, "foi efetuado", "foram efetuados") & " [0]" & IIf(C3(D5)("Index9") = 1, "um saque eletrônico", "diversos saques eletrônicos") & "[/0] " & IIf(C3(D5)("Index9") = 1, "no valor", "nos valores") & " de"
                    For Each D7 In C3(D5)("Index10").Keys
                        If D7 > 1 Then
                            If D7 = C3(D5)("Index9") Then
                                .InsertAfter Text:=" e"
                            Else
                                .InsertAfter Text:=","
                            End If
                        End If
                        .InsertAfter Text:=" " & Function4(CDbl(C3(D5)("Index10")(D7)("Index1")))
                    Next
                    .InsertAfter Text:="."
                End If
                If C3(D5)("Index11") > 0 Then
                    .InsertAfter Text:=" Ressaltamos que " & IIf(C3(D5)("Index11") = 1, "o saque foi disponibilizado", "os saques foram disponibilizados") & " por recurso do saldo do cartão."
                Else
                    .InsertAfter Text:=" Não identificamos saques."
                End If
                If C3(D5)("Index13") = True Then
                    If C3(D5)("Index14") = C3(D5)("Index16") Then
                        .InsertAfter Text:=" O único desconto em folha ocorreu em [0]" & Format(C3(D5)("Index14"), "yyyy") & "[/0], no valor de " & Function4(C3(D5)("Index15")) & ". Ressaltamos que o desconto em folha se refere ao valor mínimo de sua fatura."
                    Else
                        .InsertAfter Text:=" O primeiro desconto em folha ocorreu em [0]" & Format(C3(D5)("Index14"), "yyyy") & "[/0], no valor de " & Function4(C3(D5)("Index15")) & " e o último desconto em folha ocorreu em [0]" & Format(C3(D5)("Index16"), "yyyy") & "[/0], no valor de " & Function4(C3(D5)("Index17")) & ". Ressaltamos que os descontos em folha se referem ao valor mínimo de suas faturas."
                    End If
                Else
                    .InsertAfter Text:=" Não identificamos descontos em folha."
                End If
                If C3(D5)("Index21") > 0 Then
                    If Format(C3(D5)("Index22")(1)("Index2"), "yyyy") = Format(C3(D5)("Index22")(C3(D5)("Index21"))("Index2"), "yyyy") Then
                        .InsertAfter Text:=" Em [0]" & Format(C3(D5)("Index22")(1)("Index2"), "yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" Entre [0]" & Format(C3(D5)("Index22")(1)("Index2"), "yyyy") & "[/0] e [0]" & Format(C3(D5)("Index22")(C3(D5)("Index21"))("Index2"), "yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", " & IIf(ThisDocument.Variables("B5") = 1, "o", IIf(ThisDocument.Variables("B5") = 2, "a", "")) & " cliente entrou em contato na central, e através " & IIf(C3(D5)("Index21") = 1, "do atendimento prestado", "dos atendimentos prestados") & ", foi possível identificar que " & IIf(ThisDocument.Variables("B5") = 1, "o mesmo", IIf(ThisDocument.Variables("B5") = 2, "a mesma", "")) & " possui ciência sobre a contratação do cartão."
                End If
                If C3(D5)("Index23") > 0 Then
                    If Format(C3(D5)("Index24")(1)("Index2"), "yyyy") = Format(C3(D5)("Index24")(C3(D5)("Index23"))("Index2"), "yyyy") Then
                        .InsertAfter Text:=" " & IIf(C3(D5)("Index21") > 0, "Também identificamos que, em", "Em") & " [0]" & Format(C3(D5)("Index24")(1)("Index2"), "yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" " & IIf(C3(D5)("Index21") > 0, "Também identificamos que, entre", "Entre") & " [0]" & Format(C3(D5)("Index24")(1)("Index2"), "yyyy") & "[/0] e [0]" & Format(C3(D5)("Index24")(C3(D5)("Index23"))("Index2"), "yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", " & IIf(ThisDocument.Variables("B5") = 1, "o", IIf(ThisDocument.Variables("B5") = 2, "a", "")) & " cliente navegou na URA da Central de Cartões."
                End If
                If C3(D5)("Index21") = 0 And C3(D5)("Index23") = 0 Then
                    .InsertAfter Text:=" Não identificamos atendimentos referentes ao cartão."
                End If
                If C3(D5)("Index25") = True Then
                    .InsertAfter Text:=" Identificamos que há histórico de pré-venda em [0]" & Format(C3(D5)("Index27"), "yyyy") & "[/0], onde " & IIf(ThisDocument.Variables("B5") = 1, "o", IIf(ThisDocument.Variables("B5") = 2, "a", "")) & " cliente confirmou a adesão ao produto."
                Else
                    .InsertAfter Text:=" Não identificamos histórico de pré-venda referente ao cartão."
                End If
                If C3(D5)("Index33") = True Then
                    If Format(C3(D5)("Index34"), "yyyy") = Format(C3(D5)("Index35"), "yyyy") Then
                        .InsertAfter Text:=" Em [0]" & Format(C3(D5)("Index34"), "yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" Entre [0]" & Format(C3(D5)("Index34"), "yyyy") & "[/0] e [0]" & Format(C3(D5)("Index35"), "yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", houve confirmações de leituras de faturas."
                End If
                If C3(D5)("Index37") = True Then
                    .InsertAfter Text:=" Identificamos que houve utilização do cartão para realização de compras"
                    If Format(C3(D5)("Index38"), "yyyy") = Format(C3(D5)("Index39"), "yyyy") Then
                        .InsertAfter Text:=" em [0]" & Format(C3(D5)("Index38"), "yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" entre [0]" & Format(C3(D5)("Index38"), "yyyy") & "[/0] e [0]" & Format(C3(D5)("Index39"), "yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:="."
                End If
                If C3(D5)("Index41") = True Then
                    If C3(D5)("Index37") = True Then
                        .InsertAfter Text:=" Também identificamos"
                    Else
                        .InsertAfter Text:=" Identificamos"
                    End If
                    .InsertAfter Text:=" que houve pagamentos de faturas"
                    If Format(C3(D5)("Index42"), "yyyy") = Format(C3(D5)("Index43"), "yyyy") Then
                        .InsertAfter Text:=" em [0]" & Format(C3(D5)("Index42"), "yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" entre [0]" & Format(C3(D5)("Index42"), "yyyy") & "[/0] e [0]" & Format(C3(D5)("Index43"), "yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:="."
                End If
                If C3(D5)("Index37") = False And C3(D5)("Index41") = False Then
                    .InsertAfter Text:=" Não identificamos compras realizadas através do cartão ou pagamentos de faturas."
                End If
                If C3(D5)("Index45") > 0 Then
                    .InsertAfter Text:=" O aplicativo do BMG Card foi acessado [0]" & Function6(1, CLng(C3(D5)("Index45"))) & " " & IIf(C3(D5)("Index45") = 1, "vez", "vezes") & "[/0]"
                    If Format(C3(D5)("Index46"), "yyyy") = Format(C3(D5)("Index47"), "yyyy") Then
                        .InsertAfter Text:=" em [0]" & Format(C3(D5)("Index46"), "yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" entre [0]" & Format(C3(D5)("Index46"), "yyyy") & "[/0] e [0]" & Format(C3(D5)("Index47"), "yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:="."
                End If
                If C3(D5)("Index49") > 0 Then
                    .InsertAfter Text:=" Identificamos que,"
                    If Format(C3(D5)("Index50"), "yyyy") = Format(C3(D5)("Index51"), "yyyy") Then
                        .InsertAfter Text:=" em [0]" & Format(C3(D5)("Index50"), "yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" entre [0]" & Format(C3(D5)("Index50"), "yyyy") & "[/0] e [0]" & Format(C3(D5)("Index51"), "yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", " & IIf(C3(D5)("Index49") = 1, "foi enviado", "foram enviados") & " [0]" & Function6(0, CLng(C3(D5)("Index49"))) & " SMS[/0] para " & IIf(ThisDocument.Variables("B5") = 1, "o", IIf(ThisDocument.Variables("B5") = 2, "a", "")) & " cliente."
                End If
                If C3(D5)("Index53") > 0 Then
                    .InsertAfter Text:=" Identificamos que,"
                    If Format(C3(D5)("Index54"), "yyyy") = Format(C3(D5)("Index55"), "yyyy") Then
                        .InsertAfter Text:=" em [0]" & Format(C3(D5)("Index54"), "yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" entre [0]" & Format(C3(D5)("Index54"), "yyyy") & "[/0] e [0]" & Format(C3(D5)("Index55"), "yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", " & IIf(C3(D5)("Index53") = 1, "foi enviado", "foram enviados") & " [0]" & Function6(1, CLng(C3(D5)("Index53"))) & " " & IIf(C3(D5)("Index53") = 1, "cópia de contrato", "cópias de contratos") & "[/0] para " & IIf(ThisDocument.Variables("B5") = 1, "o", IIf(ThisDocument.Variables("B5") = 2, "a", "")) & " cliente."
                End If
                
                    If C3(D5)("Index79") > 0 Then
                    .InsertAfter Text:=" Identificamos que foi disponibilizado o código de rastreio."
                    End If
                
                    If C3(D5)("Index89") > 0 Then
                    .InsertAfter Text:=" Identificamos que foi realizado atendimento referente a operação duvidosa."
                    End If

                    If C3(D5)("Index99") > 0 Then
                    .InsertAfter Text:=" Identificamos que foi realizado atendimento em segunda instância."
                    End If
                    
                    If C3(D5)("Index19") = True Then
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .InsertAfter Text:="Conforme verificado, identificamos que o cartão e a sua respectiva conta se encontram, ambos, com o status [0]Normal[/0], estando este cartão apto para uso."
                    If C3(D5)("Index20").Count > 0 Then
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="%8%"
                        Call Sub2(2, "%8%", C3(D5)("Index20")(1))
                        .InsertAfter Text:="[2]Tela demonstrando o status do cartão e da sua respectiva conta.[/2]"
                    End If
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
    End With
End Sub

Sub Sub22(ByVal A9 As Integer, ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    With ThisDocument
        Call Sub2(1, "%8%", A9 & " - HISTÓRICO DE ATENDIMENTOS PRESTADOS " & IIf(.Variables("B5") = 1, "AO CONSUMIDOR", IIf(.Variables("B5") = 2, "À CONSUMIDORA", "")))
        With .Content
            For Each D8 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D8) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                If C3(D8)("Index21") > 0 Then
                    If Format(C3(D8)("Index22")(1)("Index2"), "dd/mm/yyyy") = Format(C3(D8)("Index22")(C3(D8)("Index21"))("Index2"), "dd/mm/yyyy") Then
                        .InsertAfter Text:="Em [0]" & Format(C3(D8)("Index22")(1)("Index2"), "dd/mm/yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:="Entre [0]" & Format(C3(D8)("Index22")(1)("Index2"), "dd/mm/yyyy") & "[/0] e [0]" & Format(C3(D8)("Index22")(C3(D8)("Index21"))("Index2"), "dd/mm/yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", " & IIf(ThisDocument.Variables("B5") = 1, "o", IIf(ThisDocument.Variables("B5") = 2, "a", "")) & " cliente entrou em contato na central, e através " & IIf(C3(D8)("Index21") = 1, "do atendimento prestado", "dos atendimentos prestados") & ", foi possível identificar que " & IIf(ThisDocument.Variables("B5") = 1, "o mesmo", IIf(ThisDocument.Variables("B5") = 2, "a mesma", "")) & " possui ciência sobre a contratação do cartão."
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    For Each D9 In C3(D8)("Index22").Keys
                        If D9 > 1 Then
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                        End If
                        .InsertAfter Text:="[0]Protocolo " & IIf(IsNull(C3(D8)("Index22")(D9)("Index1")), "", C3(D8)("Index22")(D9)("Index1")) & "[/0]"
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Data:[/0] " & Format(C3(D8)("Index22")(D9)("Index2"), "dd/mm/yyyy")
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Hora:[/0] " & Format(C3(D8)("Index22")(D9)("Index2"), "hh:mm")
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Motivo:[/0] " & IIf(IsNull(C3(D8)("Index22")(D9)("Index3")), "", C3(D8)("Index22")(D9)("Index3"))
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Bina:[/0] " & IIf(IsNull(C3(D8)("Index22")(D9)("Index4")), "", C3(D8)("Index22")(D9)("Index4"))
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Atendente:[/0] " & IIf(IsNull(C3(D8)("Index22")(D9)("Index5")), "", C3(D8)("Index22")(D9)("Index5"))
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Central:[/0] " & IIf(C3(D8)("Index22")(D9)("Index6") = 1, "SAC", IIf(C3(D8)("Index22")(D9)("Index6") = 2, "RELACIONAMENTO CARTÃO", ""))
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[1]DESCRIÇÃO DA SOLICITAÇÃO " & IIf(ThisDocument.Variables("B5") = 1, "DO", IIf(ThisDocument.Variables("B5") = 2, "DA", "")) & " CLIENTE:[/1]"
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        If Not IsNull(C3(D8)("Index22")(D9)("Index7")) Then
                            .InsertAfter Text:=C3(D8)("Index22")(D9)("Index7")
                        End If
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[1]INFORMAÇÃO PASSADA PELO ATENDENTE:[/1]"
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        If Not IsNull(C3(D8)("Index22")(D9)("Index8")) Then
                            .InsertAfter Text:=C3(D8)("Index22")(D9)("Index8")
                        End If
                    Next
                Else
                    .InsertAfter Text:="Não identificamos atendimentos humanos referentes ao cartão."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
        Call Sub2(1, "%8%", A9 & ".1 - HISTÓRICO DE ATENDIMENTOS ELETRÔNICOS (URA)")
        With .Content
            For Each D10 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D10) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                If C3(D10)("Index23") > 0 Then
                    If Format(C3(D10)("Index24")(1)("Index2"), "dd/mm/yyyy") = Format(C3(D10)("Index24")(C3(D10)("Index23"))("Index2"), "dd/mm/yyyy") Then
                        .InsertAfter Text:="Em [0]" & Format(C3(D10)("Index24")(1)("Index2"), "dd/mm/yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:="Entre [0]" & Format(C3(D10)("Index24")(1)("Index2"), "dd/mm/yyyy") & "[/0] e [0]" & Format(C3(D10)("Index24")(C3(D10)("Index23"))("Index2"), "dd/mm/yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", " & IIf(ThisDocument.Variables("B5") = 1, "o", IIf(ThisDocument.Variables("B5") = 2, "a", "")) & " cliente navegou na URA da Central de Cartões."
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    For Each D11 In C3(D10)("Index24").Keys
                        If D11 > 1 Then
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                        End If
                        .InsertAfter Text:="[0]Protocolo " & IIf(IsNull(C3(D10)("Index24")(D11)("Index1")), "", C3(D10)("Index24")(D11)("Index1")) & "[/0]"
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Data:[/0] " & Format(C3(D10)("Index24")(D11)("Index2"), "dd/mm/yyyy")
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Hora:[/0] " & Format(C3(D10)("Index24")(D11)("Index2"), "hh:mm")
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Motivo:[/0] " & IIf(IsNull(C3(D10)("Index24")(D11)("Index3")), "", C3(D10)("Index24")(D11)("Index3"))
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Bina:[/0] " & IIf(IsNull(C3(D10)("Index24")(D11)("Index4")), "", C3(D10)("Index24")(D11)("Index4"))
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Atendente:[/0] URA"
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Central:[/0] RELACIONAMENTO CARTÃO"
                        If C3(D10)("Index24")(D11)("Index5").Count > 0 Then
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", CStr(C3(D10)("Index24")(D11)("Index5")(1)))
                            .InsertAfter Text:="[2]Tela demonstrando o atendimento eletrônico.[/2]"
                        End If
                    Next
                Else
                    .InsertAfter Text:="Não identificamos atendimentos eletrônicos referentes ao cartão."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
        Call Sub2(1, "%8%", A9 & ".2 - HISTÓRICO DE PRÉ-VENDA")
        With .Content
            For Each D12 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D12) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                If C3(D12)("Index25") = True Then
                    .InsertAfter Text:="Identificamos que há histórico de pré-venda, onde " & IIf(ThisDocument.Variables("B5") = 1, "o", IIf(ThisDocument.Variables("B5") = 2, "a", "")) & " cliente confirmou a adesão ao produto."
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .InsertAfter Text:="[0]Número da Adesão:[/0] " & C3(D12)("Index26")
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .InsertAfter Text:="[0]Data do Contato:[/0] " & Format(C3(D12)("Index27"), "dd/mm/yyyy")
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .InsertAfter Text:="[0]Hora do Contato:[/0] " & Format(C3(D12)("Index27"), "hh:mm")
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .InsertAfter Text:="[1]DESCRIÇÃO DO CONTATO:[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    If Not IsNull(C3(D12)("Index28")) Then
                        .InsertAfter Text:=C3(D12)("Index28")
                    End If
                Else
                    .InsertAfter Text:="Não identificamos histórico de pré-venda referente ao cartão."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
    End With
End Sub

Sub Sub23(ByVal A9 As Integer, ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    With ThisDocument
        Call Sub2(1, "%8%", A9 & " - TELAS DE CARTÃO, SAQUES E DESCONTOS EM FOLHA")
        With .Content
            For Each D13 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D13) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                .InsertAfter Text:="Identificamos a contratação do cartão em [0]" & Format(C3(D13)("Index1"), "dd/mm/yyyy") & "[/0]."
                If C3(D13)("Index3") = True Then
                    .InsertAfter Text:=" No ato desta contratação, foi solicitado um [0]saque autorizado[/0] no valor de " & Function4(C3(D13)("Index4")("Index1")) & ", o qual foi disponibilizado"
                    If C3(D13)("Index4")("Index2") = C3(D13)("Index1") Then
                        .InsertAfter Text:=" neste mesmo dia"
                    Else
                        .InsertAfter Text:=" em [0]" & Format(C3(D13)("Index4")("Index2"), "dd/mm/yyyy") & "[/0]"
                    End If
                    If IIf(C3(D13)("Index7") > 1, C3(D13)("Index8"), False) = True Then
                        .InsertAfter Text:="."
                    Else
                        .InsertAfter Text:=" através de"
                        If C3(D13)("Index4")("Index3") = 1 Then
                            .InsertAfter Text:=" [0]transferência bancária[/0] " & IIf((C3(D13)("Index4")("Index4") = "085" Or C3(D13)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(C3(D13)("Index4")("Index4")) & "[/0], na agência [0]" & C3(D13)("Index4")("Index5") & "[/0], na conta [0]" & C3(D13)("Index4")("Index6") & "[/0]."
                        ElseIf C3(D13)("Index4")("Index3") = 2 Then
                            .InsertAfter Text:=" [0]ordem de pagamento[/0] " & IIf((C3(D13)("Index4")("Index4") = "085" Or C3(D13)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(C3(D13)("Index4")("Index4")) & "[/0], na agência [0]" & C3(D13)("Index4")("Index5") & "[/0]."
                        ElseIf C3(D13)("Index4")("Index3") = 3 Then
                            .InsertAfter Text:=" [0]cheque[/0] " & IIf((C3(D13)("Index4")("Index4") = "085" Or C3(D13)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(C3(D13)("Index4")("Index4")) & "[/0], na agência [0]" & C3(D13)("Index4")("Index5") & "[/0], na conta [0]" & C3(D13)("Index4")("Index6") & "[/0]."
                        End If
                    End If
                    If C3(D13)("Index4")("Index7") = True Then
                        .InsertAfter Text:=" Este saque foi parcelado em [0]" & C3(D13)("Index4")("Index8") & " parcelas[/0] de " & Function4(C3(D13)("Index4")("Index9")) & "."
                    End If
                End If
                If C3(D13)("Index5") > 0 Then
                    For Each D14 In C3(D13)("Index6").Keys
                        .InsertAfter Text:=" Em [0]" & Format(C3(D13)("Index6")(D14)("Index2"), "dd/mm/yyyy") & "[/0], foi disponibilizado [0]um saque complementar[/0] no valor de " & Function4(CDbl(C3(D13)("Index6")(D14)("Index1")))
                        If IIf(C3(D13)("Index7") > 1, C3(D13)("Index8"), False) = False Then
                            .InsertAfter Text:=" através de"
                            If C3(D13)("Index6")(D14)("Index3") = 1 Then
                                .InsertAfter Text:=" [0]transferência bancária[/0] " & IIf((C3(D13)("Index6")(D14)("Index4") = "085" Or C3(D13)("Index6")(D14)("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D13)("Index6")(D14)("Index4"))) & "[/0], na agência [0]" & C3(D13)("Index6")(D14)("Index5") & "[/0], na conta [0]" & C3(D13)("Index6")(D14)("Index6") & "[/0]"
                            ElseIf C3(D13)("Index6")(D14)("Index3") = 2 Then
                                .InsertAfter Text:=" [0]ordem de pagamento[/0] " & IIf((C3(D13)("Index6")(D14)("Index4") = "085" Or C3(D13)("Index6")(D14)("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D13)("Index6")(D14)("Index4"))) & "[/0], na agência [0]" & C3(D13)("Index6")(D14)("Index5") & "[/0]"
                            ElseIf C3(D13)("Index6")(D14)("Index3") = 3 Then
                                .InsertAfter Text:=" [0]cheque[/0] " & IIf((C3(D13)("Index6")(D14)("Index4") = "085" Or C3(D13)("Index6")(D14)("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D13)("Index6")(D14)("Index4"))) & "[/0], na agência [0]" & C3(D13)("Index6")(D14)("Index5") & "[/0], na conta [0]" & C3(D13)("Index6")(D14)("Index6") & "[/0]"
                            End If
                        End If
                        .InsertAfter Text:="."
                    Next
                    If IIf(C3(D13)("Index7") > 1, C3(D13)("Index8"), False) = True Then
                        .InsertAfter Text:=" Todos estes saques foram disponibilizados através de"
                        If C3(D13)("Index3") = True Then
                            If C3(D13)("Index4")("Index3") = 1 Then
                                .InsertAfter Text:=" [0]transferência bancária[/0] " & IIf((C3(D13)("Index4")("Index4") = "085" Or C3(D13)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D13)("Index4")("Index4"))) & "[/0], na agência [0]" & C3(D13)("Index4")("Index5") & "[/0], na conta [0]" & C3(D13)("Index4")("Index6") & "[/0]"
                            ElseIf C3(D13)("Index4")("Index3") = 2 Then
                                .InsertAfter Text:=" [0]ordem de pagamento[/0] " & IIf((C3(D13)("Index4")("Index4") = "085" Or C3(D13)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D13)("Index4")("Index4"))) & "[/0], na agência [0]" & C3(D13)("Index4")("Index5") & "[/0]"
                            ElseIf C3(D13)("Index4")("Index3") = 3 Then
                                .InsertAfter Text:=" [0]cheque[/0] " & IIf((C3(D13)("Index4")("Index4") = "085" Or C3(D13)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D13)("Index4")("Index4"))) & "[/0], na agência [0]" & C3(D13)("Index4")("Index5") & "[/0], na conta [0]" & C3(D13)("Index4")("Index6") & "[/0]"
                            End If
                        Else
                            If C3(D13)("Index6")(1)("Index3") = 1 Then
                                .InsertAfter Text:=" [0]transferência bancária[/0] " & IIf((C3(D13)("Index6")(1)("Index4") = "085" Or C3(D13)("Index6")(1)("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D13)("Index6")(1)("Index4"))) & "[/0], na agência [0]" & C3(D13)("Index6")(1)("Index5") & "[/0], na conta [0]" & C3(D13)("Index6")(1)("Index6") & "[/0]"
                            ElseIf C3(D13)("Index6")(1)("Index3") = 2 Then
                                .InsertAfter Text:=" [0]ordem de pagamento[/0] " & IIf((C3(D13)("Index6")(1)("Index4") = "085" Or C3(D13)("Index6")(1)("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D13)("Index6")(1)("Index4"))) & "[/0], na agência [0]" & C3(D13)("Index6")(1)("Index5") & "[/0]"
                            ElseIf C3(D13)("Index6")(1)("Index3") = 3 Then
                                .InsertAfter Text:=" [0]cheque[/0] " & IIf((C3(D13)("Index6")(1)("Index4") = "085" Or C3(D13)("Index6")(1)("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D13)("Index6")(1)("Index4"))) & "[/0], na agência [0]" & C3(D13)("Index6")(1)("Index5") & "[/0], na conta [0]" & C3(D13)("Index6")(1)("Index6") & "[/0]"
                            End If
                        End If
                        .InsertAfter Text:="."
                    End If
                End If
                If C3(D13)("Index9") > 0 Then
                    For Each D15 In C3(D13)("Index10").Keys
                        .InsertAfter Text:=" Em [0]" & Format(C3(D13)("Index10")(D15)("Index2"), "dd/mm/yyyy") & "[/0], foi efetuado [0]um saque eletrônico[/0] no valor de " & Function4(CDbl(C3(D13)("Index10")(D15)("Index1"))) & "."
                    Next
                End If
                If C3(D13)("Index11") > 0 Then
                    .InsertAfter Text:=" Ressaltamos que " & IIf(C3(D13)("Index11") = 1, "o saque foi disponibilizado", "os saques foram disponibilizados") & " por recurso do saldo do cartão."
                Else
                    .InsertAfter Text:=" Não identificamos saques."
                End If
                If C3(D13)("Index2").Count > 0 Then
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .InsertAfter Text:="%8%"
                    Call Sub2(2, "%8%", C3(D13)("Index2")(1))
                    .InsertAfter Text:="[2]Tela demonstrando a contratação do cartão.[/2]"
                End If
                If C3(D13)("Index11") > 0 Then
                    If C3(D13)("Index12").Count > 0 Then
                        For Each D16 In C3(D13)("Index12")
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", C3(D13)("Index12")(D16))
                            .InsertAfter Text:="[2]Tela demonstrando " & IIf(C3(D13)("Index11") = 1, "a contratação do saque", "as contratações dos saques") & ".[/2]"
                        Next
                    End If
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C3(D13)("Index13") = True Then
                    If C3(D13)("Index14") = C3(D13)("Index16") Then
                        .InsertAfter Text:="O único desconto em folha ocorreu em [0]" & Format(C3(D13)("Index14"), "dd/mm/yyyy") & "[/0], no valor de " & Function4(C3(D13)("Index15")) & ". Ressaltamos que o desconto em folha se refere ao valor mínimo de sua fatura."
                    Else
                        .InsertAfter Text:="O primeiro desconto em folha ocorreu em [0]" & Format(C3(D13)("Index14"), "dd/mm/yyyy") & "[/0], no valor de " & Function4(C3(D13)("Index15")) & " e o último desconto em folha ocorreu em [0]" & Format(C3(D13)("Index16"), "dd/mm/yyyy") & "[/0], no valor de " & Function4(C3(D13)("Index17")) & ". Ressaltamos que os descontos em folha se referem ao valor mínimo de suas faturas."
                    End If
                Else
                    .InsertAfter Text:="Não identificamos descontos em folha."
                End If
                If C3(D13)("Index18").Count > 0 Then
                    For Each D17 In C3(D13)("Index18")
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="%8%"
                        Call Sub2(2, "%8%", C3(D13)("Index18")(D17))
                        .InsertAfter Text:="[2]Tela demonstrando " & IIf(C3(D13)("Index13") = True, IIf(C3(D13)("Index14") = C3(D13)("Index16"), "o desconto em folha", "os descontos em folha"), "a ausência de descontos em folha") & ".[/2]"
                    Next
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
    End With
End Sub

Sub Sub24(ByVal A9 As Integer, ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    With ThisDocument
        Call Sub2(1, "%8%", A9 & " - TELAS DE FORMA DE ENVIO DE FATURAS E DE CONFIRMAÇÕES DE LEITURAS DE FATURAS")
        With .Content
            For Each D18 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D18) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                If C3(D18)("Index29") = True Then
                    .InsertAfter Text:="Identificamos que a forma de envio de faturas está definida como " & IIf(C3(D18)("Index30") = 1, "[0]Online[/0] (através do site)", IIf(C3(D18)("Index30") = 2, "[0]Correio[/0]", IIf(C3(D18)("Index30") = 3, "[0]Email[/0]", IIf(C3(D18)("Index30") = 4, "[0]Correio + Email[/0]", IIf(C3(D18)("Index30") = 5, "[0]SMS[/0]", ""))))) & "."
                    If C3(D18)("Index31").Count > 0 Then
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="%8%"
                        Call Sub2(2, "%8%", C3(D18)("Index31")(1))
                        .InsertAfter Text:="[2]Tela demonstrando a forma de envio de faturas.[/2]"
                    End If
                    If C3(D18)("Index30") = 2 Or C3(D18)("Index30") = 4 Then
                        If C3(D18)("Index32").Count > 0 Then
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", C3(D18)("Index32")(1))
                            .InsertAfter Text:="[2]Tela demonstrando o endereço.[/2]"
                        End If
                    End If
                Else
                    .InsertAfter Text:="Não foi possível identificar a forma de envio de faturas, pois o cartão não foi encontrado no IntergrALL."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C3(D18)("Index33") = True Then
                    If C3(D18)("Index34") = C3(D18)("Index35") Then
                        .InsertAfter Text:="Em [0]" & Format(C3(D18)("Index34"), "dd/mm/yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:="Entre [0]" & Format(C3(D18)("Index34"), "dd/mm/yyyy") & "[/0] e [0]" & Format(C3(D18)("Index35"), "dd/mm/yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", houve confirmações de leituras de faturas."
                    If C3(D18)("Index36").Count > 0 Then
                        For Each D19 In C3(D18)("Index36")
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", C3(D18)("Index36")(D19))
                            .InsertAfter Text:="[2]Tela demonstrando as confirmações de leituras de faturas.[/2]"
                        Next
                    End If
                Else
                    .InsertAfter Text:="Não identificamos confirmações de leituras de faturas."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
    End With
End Sub

Sub Sub25(ByVal A9 As Integer, ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    With ThisDocument
        Call Sub2(1, "%8%", A9 & " - TELAS DE FATURAS")
        With .Content
            For Each D20 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D20) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                If C3(D20)("Index37") = True Then
                    .InsertAfter Text:="Identificamos que houve utilização do cartão para realização de compras"
                    If C3(D20)("Index38") = C3(D20)("Index39") Then
                        .InsertAfter Text:=" em [0]" & Format(C3(D20)("Index38"), "dd/mm/yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" entre [0]" & Format(C3(D20)("Index38"), "dd/mm/yyyy") & "[/0] e [0]" & Format(C3(D20)("Index39"), "dd/mm/yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:="."
                End If
                If C3(D20)("Index41") = True Then
                    If C3(D20)("Index37") = True Then
                        .InsertAfter Text:=" Também identificamos"
                    Else
                        .InsertAfter Text:="Identificamos"
                    End If
                    .InsertAfter Text:=" que houve pagamentos de faturas"
                    If C3(D20)("Index42") = C3(D20)("Index43") Then
                        .InsertAfter Text:=" em [0]" & Format(C3(D20)("Index42"), "dd/mm/yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" entre [0]" & Format(C3(D20)("Index42"), "dd/mm/yyyy") & "[/0] e [0]" & Format(C3(D20)("Index43"), "dd/mm/yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:="."
                End If
                If C3(D20)("Index37") = True Then
                    If C3(D20)("Index40").Count > 0 Then
                        For Each D21 In C3(D20)("Index40")
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", C3(D20)("Index40")(D21))
                            .InsertAfter Text:="[2]Tela demonstrando as compras efetuadas através do cartão.[/2]"
                        Next
                    End If
                End If
                If C3(D20)("Index41") = True Then
                    If C3(D20)("Index44").Count > 0 Then
                        For Each D22 In C3(D20)("Index44")
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", C3(D20)("Index44")(D22))
                            .InsertAfter Text:="[2]Tela demonstrando os pagamentos de faturas.[/2]"
                        Next
                    End If
                End If
                If C3(D20)("Index37") = False And C3(D20)("Index41") = False Then
                    .InsertAfter Text:="Não identificamos compras realizadas através do cartão ou pagamentos de faturas."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
    End With
End Sub

Sub Sub26(ByVal A9 As Integer, ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    With ThisDocument
        Call Sub2(1, "%8%", A9 & " - TELAS DE ACESSOS AO APLICATIVO")
        With .Content
            For Each D23 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D23) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                If C3(D23)("Index45") > 0 Then
                    .InsertAfter Text:="O aplicativo do BMG Card foi acessado [0]" & Function6(1, CLng(C3(D23)("Index45"))) & " " & IIf(C3(D23)("Index45") = 1, "vez", "vezes") & "[/0]"
                    If C3(D23)("Index46") = C3(D23)("Index47") Then
                        .InsertAfter Text:=" em [0]" & Format(C3(D23)("Index46"), "dd/mm/yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" entre [0]" & Format(C3(D23)("Index46"), "dd/mm/yyyy") & "[/0] e [0]" & Format(C3(D23)("Index47"), "dd/mm/yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:="."
                    If C3(D23)("Index48").Count > 0 Then
                        For Each D24 In C3(D23)("Index48")
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", C3(D23)("Index48")(D24))
                            .InsertAfter Text:="[2]Tela demonstrando " & IIf(C3(D23)("Index45") = 1, "o acesso ao aplicativo", "os acessos ao aplicativo") & ".[/2]"
                        Next
                    End If
                Else
                    .InsertAfter Text:="Não identificamos acessos ao aplicativo."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
    End With
End Sub

Sub Sub27(ByVal A9 As Integer, ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    With ThisDocument
        Call Sub2(1, "%8%", A9 & " - TELAS DE ENVIOS DE SMS")
        With .Content
            For Each D25 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D25) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                If C3(D25)("Index49") > 0 Then
                    .InsertAfter Text:="Identificamos que,"
                    If C3(D25)("Index50") = C3(D25)("Index51") Then
                        .InsertAfter Text:=" em [0]" & Format(C3(D25)("Index50"), "dd/mm/yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" entre [0]" & Format(C3(D25)("Index50"), "dd/mm/yyyy") & "[/0] e [0]" & Format(C3(D25)("Index51"), "dd/mm/yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", " & IIf(C3(D25)("Index49") = 1, "foi enviado", "foram enviados") & " [0]" & Function6(0, CLng(C3(D25)("Index49"))) & " SMS[/0] para " & IIf(ThisDocument.Variables("B5") = 1, "o", IIf(ThisDocument.Variables("B5") = 2, "a", "")) & " cliente."
                    If C3(D25)("Index52").Count > 0 Then
                        For Each D26 In C3(D25)("Index52")
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", C3(D25)("Index52")(D26))
                            .InsertAfter Text:="[2]Tela demonstrando " & IIf(C3(D25)("Index49") = 1, "o envio de SMS", "os envios de SMS") & ".[/2]"
                        Next
                    End If
                Else
                    .InsertAfter Text:="Não identificamos envios de SMS."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
    End With
End Sub

Sub Sub28(ByVal A9 As Integer, ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    With ThisDocument
        Call Sub2(1, "%8%", A9 & " - TELAS DE ENVIOS DE CÓPIAS DE CONTRATOS")
        With .Content
            For Each D1 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D1) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                If C3(D1)("Index53") > 0 Then
                    .InsertAfter Text:="Identificamos que,"
                    If C3(D1)("Index54") = C3(D1)("Index55") Then
                        .InsertAfter Text:=" em [0]" & Format(C3(D1)("Index54"), "dd/mm/yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" entre [0]" & Format(C3(D1)("Index54"), "dd/mm/yyyy") & "[/0] e [0]" & Format(C3(D1)("Index55"), "dd/mm/yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", " & IIf(C3(D1)("Index53") = 1, "foi enviado", "foram enviados") & " [0]" & Function6(1, CLng(C3(D1)("Index53"))) & " " & IIf(C3(D1)("Index53") = 1, "cópia de contrato", "cópias de contratos") & "[/0] para " & IIf(ThisDocument.Variables("B5") = 1, "o", IIf(ThisDocument.Variables("B5") = 2, "a", "")) & " cliente."
                    If C3(D1)("Index56").Count > 0 Then
                        For Each D2 In C3(D1)("Index56")
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", C3(D1)("Index56")(D2))
                            .InsertAfter Text:="[2]Tela demonstrando " & IIf(C3(D1)("Index53") = 1, "o envio de cópia de contrato", "os envios de cópias de contratos") & ".[/2]"
                        Next
                    End If
                Else
                    .InsertAfter Text:="Não identificamos envios de cópias de contratos."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
    End With
End Sub

Sub Sub29(ByVal A9 As Integer, ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    With ThisDocument
        Call Sub2(1, "%8%", A9 & " - CÓDIGO DE RASTREIO")
        With .Content
            For Each D25 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D25) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                If C3(D25)("Index79") > 0 Then
                    .InsertAfter Text:="Identificamos que houve disponibilização do código de rastreio."
                    If C3(D25)("Index72").Count > 0 Then
                        For Each D26 In C3(D25)("Index72")
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", C3(D25)("Index72")(D26))
                            .InsertAfter Text:="[2]Tela demonstrando " & IIf(C3(D25)("Index79") = 1, "o código de rastreio", "os códigos de rastreio") & ".[/2]"
                        Next
                    End If
                Else
                    .InsertAfter Text:="Não identificamos código de rastreio."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
    End With
End Sub

Sub Sub30(ByVal A9 As Integer, ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    With ThisDocument
        Call Sub2(1, "%8%", A9 & " - ATENDIMENTO DE OPERAÇÃO DUVIDOSA")
        With .Content
            For Each D25 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D25) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                If C3(D25)("Index89") > 0 Then
                    .InsertAfter Text:="Identificamos que houve atendimento referente a operação duvidosa."
                    If C3(D25)("Index82").Count > 0 Then
                        For Each D26 In C3(D25)("Index82")
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", C3(D25)("Index82")(D26))
                            .InsertAfter Text:="[2]Tela demonstrando " & IIf(C3(D25)("Index89") = 1, "o atendimento de operação duvidosa", "os atendimentos de operação duvidosa") & ".[/2]"
                        Next
                    End If
                Else
                    .InsertAfter Text:="Não identificamos atendimento referente a operação duvidosa."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
    End With
End Sub

Sub Sub31(ByVal A9 As Integer, ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    With ThisDocument
        Call Sub2(1, "%8%", A9 & " - ATENDIMENTO DE SEGUNDA INSTÂNCIA")
        With .Content
            For Each D25 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D25) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                If C3(D25)("Index99") > 0 Then
                    .InsertAfter Text:="Identificamos que houve atendimento em segunda instância."
                    If C3(D25)("Index92").Count > 0 Then
                        For Each D26 In C3(D25)("Index92")
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", C3(D25)("Index92")(D26))
                            .InsertAfter Text:="[2]Tela demonstrando " & IIf(C3(D25)("Index99") = 1, "o atendimento em segunda instância", "os atendimentos em segunda instância") & ".[/2]"
                        Next
                    End If
                Else
                    .InsertAfter Text:="Não identificamos atendimento em segunda instância."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
    End With
End Sub

Function Function1(ByVal A1 As String, ByVal A2 As Integer, ByVal A3 As Boolean, Optional ByVal A4 As String = "")
    Dim B1 As String, B2 As String, B3 As String, B4(1 To 2) As String
    Dim C1 As Object
    Dim D1 As Double
    Dim E1 As Integer
    Dim F1 As Boolean
    Set C1 = CreateObject("VBScript.RegExp")
    Do
        If A2 = 17 Then
            B1 = Function10(A1, 2, A4)
        ElseIf A2 = 22 Then
            B1 = Function10(A1, 1, A4)
        Else
            B1 = Function10(A1, 0, A4)
        End If
        
        If StrPtr(B1) = 0 Then
            If MsgBox("Tem certeza que deseja cancelar?", vbYesNo, Const1) = vbYes Then
                End
            End If
        ElseIf B1 = "" And A3 = False Then
            Function1 = Null
            Exit Do
        Else
            B2 = UCase(B1)
            If A2 = 0 Then
                With C1
                    .Pattern = ".*?([ A-ZÁÂÃÇÉÊÍÓÔÕÚÜ]+).*"
                    If .Execute(B2).Count > 0 Then
                        B3 = .Replace(B2, "$1")
                        .Pattern = "^ +"
                        B3 = .Replace(B3, "")
                        .Pattern = " +$"
                        B3 = .Replace(B3, "")
                        .Pattern = " +"
                        .Global = True
                        B3 = .Replace(B3, " ")
                        If B3 <> "" Then
                            Function1 = B3
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 1 Then
                With C1
                    .Pattern = ".*?(\d{3})\.?(\d{3})\.?(\d{3})-?(\d{2}).*"
                    If .Execute(B2).Count > 0 Then
                        Function1 = .Replace(B2, "$1.$2.$3-$4")
                        Exit Do
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 2 Then
                With C1
                    .Pattern = ".*?(\d{4})\.?[0-9X]{4}\.?[0-9X]{4}\.?(\d{4})\.?.*"
                    If .Execute(B2).Count > 0 Then
                        Function1 = .Replace(B2, "$1.XXXX.XXXX.$2")
                        Exit Do
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 3 Then
                With C1
                    .Pattern = ".*?CIV(\d{7}).*"
                    If .Execute(B2).Count > 0 Then
                        Function1 = .Replace(B2, "CIV$1")
                        Exit Do
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 4 Then
                With C1
                    .Pattern = ".*?(\d{2})/(\d{2})/(\d{4}).*"
                    If .Execute(B2).Count > 0 Then
                        B3 = .Replace(B2, "$1/$2/$3")
                        If IsDate(B3) = True Then
                            Function1 = B3
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 5 Then
                With C1
                    .Pattern = ".*?(\d{1,6}),(\d{2}).*"
                    If .Execute(B2).Count > 0 Then
                        D1 = CDbl(.Replace(B2, "$1,$2"))
                        If D1 > 0 And D1 <= 999999.99 Then
                            Function1 = D1
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 6 Then
                With C1
                    .Pattern = ".*?(\d{1}).*"
                    If .Execute(B2).Count > 0 Then
                        E1 = CInt(.Replace(B2, "$1"))
                        If E1 >= 1 And E1 <= 3 Then
                            Function1 = E1
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 7 Then
                With C1
                    .Pattern = ".*?(\d{3}).*"
                    If .Execute(B2).Count > 0 Then
                        Function1 = .Replace(B2, "$1")
                        Exit Do
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 8 Then
                With C1
                    .Pattern = ".*?(\d{1,5}).*"
                    If .Execute(B2).Count > 0 Then
                        Function1 = .Replace(B2, "$1")
                        Exit Do
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 9 Then
                With C1
                    .Pattern = ".*?(\d{1,20}(-(\d{1,2}|[PX])|\d)).*"
                    If .Execute(B2).Count > 0 Then
                        Function1 = .Replace(B2, "$1")
                        Exit Do
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 10 Then
                With C1
                    .Pattern = ".*?(\d{1,4}).*"
                    If .Execute(B2).Count > 0 Then
                        E1 = CInt(.Replace(B2, "$1"))
                        If E1 >= 0 And E1 <= 1000 Then
                            Function1 = E1
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 11 Then
                With C1
                    .Pattern = ".*?(\d{5,30}).*"
                    If .Execute(B2).Count > 0 Then
                        Function1 = .Replace(B2, "$1")
                        Exit Do
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 12 Then
                With C1
                    .Pattern = ".*?(\d{2})/(\d{2})/(\d{4}).*?(\d{2}):(\d{2}).*"
                    If .Execute(B2).Count > 0 Then
                        B3 = .Replace(B2, "$1/$2/$3 $4:$5")
                        If IsDate(B3) = True Then
                            Function1 = B3
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 13 Then
                With C1
                    .Pattern = ".*?([1-9]{2}).*?([1-9]\d{3,4}).*?(\d{4}).*"
                    If .Execute(B2).Count > 0 Then
                        Function1 = .Replace(B2, "($1) $2-$3")
                        Exit Do
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 14 Then
                With C1
                    .Pattern = ".*?(\d{1}).*"
                    If .Execute(B2).Count > 0 Then
                        E1 = CInt(.Replace(B2, "$1"))
                        If E1 >= 1 And E1 <= 2 Then
                            Function1 = E1
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 15 Then
                With C1
                    .Pattern = ".*?(\d{1}).*"
                    If .Execute(B2).Count > 0 Then
                        E1 = CInt(.Replace(B2, "$1"))
                        If E1 >= 1 And E1 <= 5 Then
                            Function1 = E1
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 16 Then
                With C1
                    .Pattern = ".*?(\d{8}).*"
                    If .Execute(B2).Count > 0 Then
                        Function1 = .Replace(B2, "$1")
                        Exit Do
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 17 Then
                B3 = Trim(B1)
                If B3 <> "" Then
                    With C1
                        .Pattern = "\[(.+?)\]"
                        .Global = True
                        Function1 = .Replace(B3, "[0]$1[/0]")
                        Exit Do
                    End With
                Else
                    MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                End If
            ElseIf A2 = 18 Then
                With C1
                    .Pattern = ".*?(\d{1,3}).*"
                    If .Execute(B2).Count > 0 Then
                        E1 = CInt(.Replace(B2, "$1"))
                        If E1 >= 2 And E1 <= 120 Then
                            Function1 = E1
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 19 Then
                With C1
                    .Pattern = ".*?(\d{1}).*"
                    If .Execute(B2).Count > 0 Then
                        E1 = CInt(.Replace(B2, "$1"))
                        If E1 >= 1 And E1 <= 5 Then
                            Function1 = E1
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 20 Then
                With C1
                    .Pattern = ".*?(\d{1,20}(-\d{1,2}|\d)[A-Z]?).*"
                    If .Execute(B2).Count > 0 Then
                        Function1 = .Replace(B2, "$1")
                        Exit Do
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 21 Then
                If B1 <> "" Then
                    Function1 = B1
                    Exit Do
                Else
                    MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                End If
            ElseIf A2 = 22 Then
                If B1 <> "" Then
                    Function1 = B1
                    Exit Do
                Else
                    MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                End If
            ElseIf A2 = 23 Then
                With C1
                    .Pattern = ".*?(\[(\{""Index1"":""\d+"",""Index2"":""\d{2}/\d{2}/\d{4} \d{2}:\d{2}"",""Index3"":""[ A-Z]*"",""Index4"":""[ 0-9A-Z]*""\},?)+\]).*"
                    If .Execute(B1).Count > 0 Then
                        B4(1) = .Replace(B1, "$1")
                        .Pattern = "^\[(.+)\]$"
                        B4(2) = .Replace(B4(1), "$1")
                        .Pattern = "\{""Index1"":""\d+"",""Index2"":""(\d{2}/\d{2}/\d{4} \d{2}:\d{2})"",""Index3"":""[ A-Z]*"",""Index4"":""[ 0-9A-Z]*""\}"
                        .Global = True
                        F1 = True
                        For Each G1 In .Execute(B4(2))
                            If IsDate(.Replace(G1.Value, "$1")) = False Then
                                F1 = False
                                Exit For
                            End If
                        Next
                        If F1 = True Then
                            Function1 = B4(1)
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 24 Then
                With C1
                    .Pattern = ".*?(\d{1}).*"
                    If .Execute(B2).Count > 0 Then
                        E1 = CInt(.Replace(B2, "$1"))
                        If E1 >= 1 And E1 <= 2 Then
                            Function1 = E1
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 25 Then
                With C1
                    .Pattern = ".*?(\d{1}).*"
                    If .Execute(B2).Count > 0 Then
                        E1 = CInt(.Replace(B2, "$1"))
                        If E1 >= 1 And E1 <= 2 Then
                            Function1 = E1
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 26 Then
                With C1
                    .Pattern = ".*?(\d{1}).*"
                    If .Execute(B2).Count > 0 Then
                        E1 = CInt(.Replace(B2, "$1"))
                        If E1 >= 1 And E1 <= 4 Then
                            Function1 = E1
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 29 Then
                With C1
                    .Pattern = ".*?(\d{1}).*"
                    If .Execute(B2).Count >= 0 Then
                        E1 = CInt(.Replace(B2, "$1"))
                        If E1 >= 0 And E1 <= 1 Then
                            Function1 = E1
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            End If
        End If
    Loop
End Function

Function Function2(ByVal A1 As Integer, Optional ByVal A2 As Integer, Optional ByVal A3 As Integer, Optional ByVal A4 As Integer = 0, Optional ByVal A5 As String = "", Optional ByVal A6 As String = "", Optional ByVal A7 As String = "") As Object
    Dim B1 As Object
    Set B1 = CreateObject("Scripting.Dictionary")
    B1.Add "Index1", Function1("Entre com o valor do saque " & IIf(A1 = 0, "autorizado", IIf(A1 = 1, "complementar (" & A2 & " de " & A3 & ")", IIf(A1 = 2, "eletrônico (" & A2 & " de " & A3 & ")", ""))) & ":", 5, True)
    B1.Add "Index2", DateValue(CStr(Function1("Entre com a data do saque " & IIf(A1 = 0, "autorizado", IIf(A1 = 1, "complementar (" & A2 & " de " & A3 & ")", IIf(A1 = 2, "eletrônico (" & A2 & " de " & A3 & ")", ""))) & ":", 4, True)))
    If A1 = 0 Or A1 = 1 Then
        B1.Add "Index3", Function1("Entre com a forma de crédito do saque " & IIf(A1 = 0, "autorizado", IIf(A1 = 1, "complementar (" & A2 & " de " & A3 & ")", "")) & " (1 = DOC/TED/CC, 2 = OP, 3 = Cheque):", 6, True, IIf(A4 <> 0, A4, ""))
        If B1("Index3") = 1 Then
            B1.Add "Index4", Function1("Entre com o banco do saque " & IIf(A1 = 0, "autorizado", IIf(A1 = 1, "complementar (" & A2 & " de " & A3 & ")", "")) & ":", 7, True, IIf(A4 = 1, A5, ""))
            B1.Add "Index5", Function1("Entre com a agência do saque " & IIf(A1 = 0, "autorizado", IIf(A1 = 1, "complementar (" & A2 & " de " & A3 & ")", "")) & ":", 8, True, IIf(A4 = 1 And A5 = B1("Index4"), A6, ""))
            B1.Add "Index6", Function1("Entre com a conta do saque " & IIf(A1 = 0, "autorizado", IIf(A1 = 1, "complementar (" & A2 & " de " & A3 & ")", "")) & ":", 9, True, IIf(A4 = 1 And A5 = B1("Index4") And A6 = B1("Index5"), A7, ""))
        ElseIf B1("Index3") = 2 Then
            B1.Add "Index4", Function1("Entre com o banco do saque " & IIf(A1 = 0, "autorizado", IIf(A1 = 1, "complementar (" & A2 & " de " & A3 & ")", "")) & ":", 7, True, IIf(A4 = 2, A5, ""))
            B1.Add "Index5", Function1("Entre com a agência do saque " & IIf(A1 = 0, "autorizado", IIf(A1 = 1, "complementar (" & A2 & " de " & A3 & ")", "")) & ":", 8, True, IIf(A4 = 2 And A5 = B1("Index4"), A6, ""))
        ElseIf B1("Index3") = 3 Then
            B1.Add "Index4", Function1("Entre com o banco do saque " & IIf(A1 = 0, "autorizado", IIf(A1 = 1, "complementar (" & A2 & " de " & A3 & ")", "")) & ":", 7, True, IIf(A4 = 3, A5, ""))
            B1.Add "Index5", Function1("Entre com a agência do saque " & IIf(A1 = 0, "autorizado", IIf(A1 = 1, "complementar (" & A2 & " de " & A3 & ")", "")) & ":", 8, True, IIf(A4 = 3 And A5 = B1("Index4"), A6, ""))
            B1.Add "Index6", Function1("Entre com a conta do saque " & IIf(A1 = 0, "autorizado", IIf(A1 = 1, "complementar (" & A2 & " de " & A3 & ")", "")) & ":", 9, True, IIf(A4 = 3 And A5 = B1("Index4") And A6 = B1("Index5"), A7, ""))
        End If
    End If
    If A1 = 0 Then
        B1.Add "Index7", Function9("O saque autorizado foi parcelado?")
        If B1("Index7") = True Then
            B1.Add "Index8", Function1("Entre com a quantidade de parcelas do saque autorizado:", 18, True)
            B1.Add "Index9", Function1("Entre com o valor das parcelas do saque autorizado:", 5, True)
        End If
    End If
    Set Function2 = B1
End Function

Function Function3(ByVal A1 As String)
    Dim B1 As Object
    Set B1 = CreateObject("Scripting.Dictionary")
    B1.Add "001", "Banco do Brasil"
    B1.Add "003", "Banco da Amazônia"
    B1.Add "004", "Banco do Nordeste do Brasil"
    B1.Add "021", "Banco do Estado do Espírito Santo"
    B1.Add "027", "Banco do Estado de Santa Catarina"
    B1.Add "025", "Banco Alfa"
    B1.Add "029", "Banco Itaú Consignado"
    B1.Add "033", "Banco Santander"
    B1.Add "037", "Banco do Estado do Pará"
    B1.Add "041", "Banco do Estado do Rio Grande do Sul"
    B1.Add "047", "Banco do Estado de Sergipe"
    B1.Add "070", "Banco de Brasília"
    B1.Add "077", "Banco Inter"
    B1.Add "085", "Cooperativa Central de Crédito Urbano"
    B1.Add "104", "Caixa Econômica Federal"
    B1.Add "121", "Banco Agibank"
    B1.Add "151", "Banco Nossa Caixa"
    B1.Add "224", "Banco Fibra"
    B1.Add "229", "Banco Cruzeiro do Sul"
    B1.Add "260", "Banco Nu Pagamentos"
    B1.Add "237", "Banco Bradesco"
    B1.Add "318", "Banco BMG"
    B1.Add "336", "Banco C6"
    B1.Add "341", "Banco Itaú Unibanco"
    B1.Add "356", "Banco ABN Amro Real"
    B1.Add "380", "Banco PicPay"
    B1.Add "389", "Banco Mercantil do Brasil"
    B1.Add "399", "HSBC Bank Brasil"
    B1.Add "409", "Banco Unibanco"
    B1.Add "422", "Banco Safra"
    B1.Add "453", "Banco Rural"
    B1.Add "623", "Banco Pan"
    B1.Add "626", "Banco C6 Consignado"
    B1.Add "655", "Banco Votorantim"
    B1.Add "707", "Banco Daycoval"
    B1.Add "748", "Banco Cooperativo Sicredi"
    B1.Add "756", "Banco Cooperativo do Brasil"
    If B1.Exists(A1) Then
        Function3 = B1(A1)
    Else
        Function3 = "Banco " & A1
    End If
End Function

Function Function4(ByVal A1 As Double)
    Dim B1 As String
    Dim C1 As Long, C2 As Long
    C1 = Int(A1)
    C2 = ((A1 - C1) * 100)
    B1 = "[0]" & FormatCurrency(A1, 2) & "[/0] ("
    If C1 > 0 Then
        B1 = B1 & Function6(0, C1) & IIf(C1 = 1, " real", " reais")
    End If
    If C2 > 0 Then
        B1 = B1 & IIf(C1 > 0, " e ", "") & Function6(0, C2) & IIf(C2 = 1, " centavo", " centavos")
    End If
    B1 = B1 & ")"
    Function4 = B1
End Function

Function Function5(ByVal A1 As Object) As Object
    Dim B1 As Object, B2 As Object
    Dim C1 As Integer, C2 As Integer
    Dim D1 As Boolean
    Set B2 = CreateObject("Scripting.Dictionary")
    Set B1 = A1
    C1 = 1
    Do While B1.Count > 0
        D1 = False
        For Each E1 In B1.Keys
            If D1 = False Then
                C2 = E1
                D1 = True
            Else
                If B1(E1)("Index2") < B1(C2)("Index2") Then
                    C2 = E1
                End If
            End If
        Next
        B2.Add C1, B1(C2)
        B1.Remove C2
        C1 = C1 + 1
    Loop
    Set Function5 = B2
End Function

Function Function6(ByVal A1 As Integer, ByVal A2 As Long)
    Dim B1 As Object, B2 As Object, B3 As Object
    Dim C1 As Long, C2 As Long, C3 As Long, C4 As Long, C5 As Long, C6 As Long
    Dim D1 As String
    Set B1 = CreateObject("Scripting.Dictionary")
    Set B2 = CreateObject("Scripting.Dictionary")
    Set B3 = CreateObject("Scripting.Dictionary")
    B1.Add 1, "um"
    B1.Add 2, "dois"
    B1.Add 3, "três"
    B1.Add 4, "quatro"
    B1.Add 5, "cinco"
    B1.Add 6, "seis"
    B1.Add 7, "sete"
    B1.Add 8, "oito"
    B1.Add 9, "nove"
    B1.Add 10, "dez"
    B1.Add 20, "vinte"
    B1.Add 30, "trinta"
    B1.Add 40, "quarenta"
    B1.Add 50, "cinquenta"
    B1.Add 60, "sessenta"
    B1.Add 70, "setenta"
    B1.Add 80, "oitenta"
    B1.Add 90, "noventa"
    B1.Add 100, "cem"
    B1.Add 200, "duzentos"
    B1.Add 300, "trezentos"
    B1.Add 400, "quatrocentos"
    B1.Add 500, "quinhentos"
    B1.Add 600, "seiscentos"
    B1.Add 700, "setecentos"
    B1.Add 800, "oitocentos"
    B1.Add 900, "novecentos"
    B2.Add "dez e um", "onze"
    B2.Add "dez e dois", "doze"
    B2.Add "dez e três", "treze"
    B2.Add "dez e quatro", "quatorze"
    B2.Add "dez e cinco", "quinze"
    B2.Add "dez e seis", "dezesseis"
    B2.Add "dez e sete", "dezessete"
    B2.Add "dez e oito", "dezoito"
    B2.Add "dez e nove", "dezenove"
    B2.Add "cem e", "cento e"
    B3.Add "um", "uma"
    B3.Add "dois", "duas"
    B3.Add "duzentos", "duzentas"
    B3.Add "trezentos", "trezentas"
    B3.Add "quatrocentos", "quatrocentas"
    B3.Add "quinhentos", "quinhentas"
    B3.Add "seiscentos", "seiscentas"
    B3.Add "setecentos", "setecentas"
    B3.Add "oitocentos", "oitocentas"
    B3.Add "novecentos", "novecentas"
    C1 = (Int(A2 / 100000) * 100)
    C2 = (Int((A2 - (C1 * 1000)) / 10000) * 10)
    C3 = Int((A2 - (C1 * 1000) - (C2 * 1000)) / 1000)
    C4 = (Int((A2 - (C1 * 1000) - (C2 * 1000) - (C3 * 1000)) / 100) * 100)
    C5 = (Int((A2 - (C1 * 1000) - (C2 * 1000) - (C3 * 1000) - C4) / 10) * 10)
    C6 = (A2 - (C1 * 1000) - (C2 * 1000) - (C3 * 1000) - C4 - C5)
    D1 = ""
    If C1 > 0 Then
        D1 = D1 & B1(C1)
    End If
    If C2 > 0 Then
        D1 = D1 & IIf(C1 > 0, " e ", "") & B1(C2)
    End If
    If C3 > 0 Then
        D1 = D1 & IIf(C1 > 0 Or C2 > 0, " e ", "") & B1(C3)
    End If
    If C1 > 0 Or C2 > 0 Or C3 > 0 Then
        D1 = D1 & " mil"
        If C4 > 0 Or C5 > 0 Or C6 > 0 Then
            If (C4 = 0 And (C5 > 0 Or C6 > 0)) Or (C4 > 0 And C5 = 0 And C6 = 0) Then
                D1 = D1 & " e "
            Else
                D1 = D1 & ", "
            End If
        End If
    End If
    If C4 > 0 Then
        D1 = D1 & B1(C4)
    End If
    If C5 > 0 Then
        D1 = D1 & IIf(C4 > 0, " e ", "") & B1(C5)
    End If
    If C6 > 0 Then
        D1 = D1 & IIf(C4 > 0 Or C5 > 0, " e ", "") & B1(C6)
    End If
    For Each E1 In B2.Keys
        D1 = Replace(D1, E1, B2(E1))
    Next
    If A1 = 1 Then
        For Each E2 In B3.Keys
            D1 = Replace(D1, E2, B3(E2))
        Next
    End If
    Function6 = D1
End Function

Function Function7(ByVal A1 As Integer, Optional ByVal A2 As Integer, Optional ByVal A3 As Integer) As Object
    Dim B1 As Object
    Dim C1() As String
    Set B1 = CreateObject("Scripting.Dictionary")
    B1.Add "Index1", Function1("Entre com o protocolo do atendimento " & IIf(A1 = 0, "humano", IIf(A1 = 1, "eletrônico", "")) & " (opcional) (" & A2 & " de " & A3 & "):", 11, False)
    C1 = Split(CStr(Function1("Entre com a data e a hora do atendimento " & IIf(A1 = 0, "humano", IIf(A1 = 1, "eletrônico", "")) & " (" & A2 & " de " & A3 & "):", 12, True)), " ")
    B1.Add "Index2", (DateValue(C1(0)) + TimeValue(C1(1)))
    If A1 = 0 Then
        B1.Add "Index3", Function1("Entre com o motivo do atendimento humano (opcional) (" & A2 & " de " & A3 & "):", 0, False)
        B1.Add "Index4", Function1("Entre com a bina do atendimento humano (opcional) (" & A2 & " de " & A3 & "):", 13, False)
        B1.Add "Index5", Function1("Entre com o atendente do atendimento humano (opcional) (" & A2 & " de " & A3 & "):", 0, False)
        B1.Add "Index6", Function1("Entre com a central do atendimento humano (opcional) (" & A2 & " de " & A3 & ") (1 = SAC, 2 = RELACIONAMENTO CARTÃO):", 14, False)
        B1.Add "Index7", Function1("Entre com a descrição da solicitação do cliente do atendimento humano (opcional) (" & A2 & " de " & A3 & "):", 17, False)
        B1.Add "Index8", Function1("Entre com a informação passada pelo atendente do atendimento humano (opcional) (" & A2 & " de " & A3 & "):", 17, False)
    ElseIf A1 = 1 Then
        B1.Add "Index3", Function1("Entre com o motivo do atendimento eletrônico (opcional) (" & A2 & " de " & A3 & "):", 0, False)
        B1.Add "Index4", Function1("Entre com a bina do atendimento eletrônico (opcional) (" & A2 & " de " & A3 & "):", 13, False)
        B1.Add "Index5", Function8("atendimento eletrônico (" & A2 & " de " & A3 & "):", False)
    End If
    Set Function7 = B1
End Function

Function Function8(ByVal A1 As String, ByVal A2 As Boolean) As Object
    Dim B1 As Object, B2 As Object
    Dim C1 As Integer, C2 As Boolean
    Set B1 = Application.FileDialog(msoFileDialogFilePicker)
    Set B2 = CreateObject("Scripting.Dictionary")
    With B1
        .Title = Const1 & " - Selecione a captura de tela (" & A1 & ")"
        .AllowMultiSelect = A2
        With .Filters
            .Clear
            .Add "Arquivos de Imagem", "*.png"
        End With
        .InitialFileName = ThisDocument.Path & "\"
        C2 = True
        .Show
        If .SelectedItems.Count > 0 Then
            For C1 = 1 To .SelectedItems.Count
                B2.Add C1, .SelectedItems(C1)
            Next C1
        End If
    End With
    Set Function8 = B2
End Function

Function Function9(ByVal A1 As String) As Boolean
    Dim B1 As Integer
    Do
        B1 = MsgBox(A1, vbYesNoCancel, Const1)
        If B1 = vbCancel Then
            If MsgBox("Tem certeza que deseja cancelar?", vbYesNo, Const1) = vbYes Then
                End
            End If
        ElseIf B1 = vbYes Then
            Function9 = True
            Exit Do
        ElseIf B1 = vbNo Then
            Function9 = False
            Exit Do
        End If
    Loop
End Function

Function Function10(ByVal A1 As String, ByVal A2 As Integer, ByVal A3 As String) As String
    Dim B1 As New UserForm2
    With B1
        .Caption = Const1
        With .Label1
            .Caption = A1
            .AutoSize = True
            .AutoSize = False
        End With
        .TextBox1.Top = (.Label1.Height + 19.25)
        With .TextBox1
            If A2 = 1 Then
                .PasswordChar = "*"
            ElseIf A2 = 2 Then
                .MultiLine = True
                .Height = 130
                .WordWrap = True
            End If
            .Value = A3
        End With
        If A2 = 0 Or A2 = 1 Then
            .CommandButton1.Top = (.Label1.Height + 45.25)
            .CommandButton2.Top = (.Label1.Height + 45.25)
            .Height = (.Label1.Height + 105)
        ElseIf A2 = 2 Then
            .CommandButton1.Top = (.Label1.Height + 159)
            .CommandButton2.Top = (.Label1.Height + 159)
            .Height = (.Label1.Height + 219.25)
        End If
        .Show
        Function10 = .A1
    End With
End Function

Function Function11(ByVal A1 As Integer, ByVal A2 As String, Optional ByVal A3 As String = "", Optional ByVal A4 As String = "", Optional ByVal A5 As Boolean) As Boolean
    Dim B1 As Object
    Set B1 = CreateObject("VBScript.RegExp")
    With ThisDocument
        If A1 = 1 Then
            If A5 = True Then
                If .A2.Exists(A2 & "," & A4) = True Then
                    .A3 = .A2(A2 & "," & A4)
                    Function11 = True
                    Exit Function
                End If
            End If
        End If
        With .A1
            If A1 = 0 Then
                .Open "GET", A2, True
            ElseIf A1 = 1 Then
                .Open "POST", A2, True
            End If
            .Option(4) = 13056
            .SetRequestHeader "User-Agent", "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0"
            If A3 <> "" Then
                .SetRequestHeader "Content-Type", A3
            End If
            .SetTimeouts 0, 120000, 60000, 60000
            Do
                B1.Pattern = "^https?:\/\/(.+?)\/.*"
                Call Sub3(0, "Comunicando com " & B1.Replace(A2, "$1") & "...")
                On Error Resume Next
                If A1 = 0 Then
                    .Send
                ElseIf A1 = 1 Then
                    .Send A4
                End If
                .WaitForResponse
                Call Sub3(1)
                If Err = 0 Then
                    If .Status <> 500 Then
                        If A1 = 1 Then
                            If A5 = True Then
                                ThisDocument.A2.Add A2 & "," & A4, .ResponseText
                                ThisDocument.A3 = .ResponseText
                            End If
                        End If
                        Function11 = True
                        Exit Do
                    Else
                        If MsgBox("Ocorreu um erro de servidor. Deseja tentar novamente?", vbYesNo, Const1) = vbNo Then
                            Function11 = False
                            Exit Do
                        End If
                    End If
                Else
                    If MsgBox("Ocorreu um erro de comunicação. Deseja tentar novamente?", vbYesNo, Const1) = vbNo Then
                        Function11 = False
                        Exit Do
                    End If
                End If
            Loop
        End With
    End With
End Function

Function Function12(ByVal A1 As String) As Variant
    Dim B1 As Object, B2 As Object
    Dim C1() As String, C2() As String
    Dim D1 As Integer
    Set B1 = CreateObject("VBScript.RegExp")
    Set B2 = CreateObject("Scripting.Dictionary")
    With B1
        .Pattern = "^\{(.+)\}$"
        If .Execute(A1).Count > 0 Then
            C1 = Split(.Replace(A1, "$1"), ",")
            For Each E1 In C1
                C2 = Split(E1, ":")
                B2.Add Function12(C2(0)), Function12(C2(1))
            Next
            Set Function12 = B2
        Else
            .Pattern = "^\[(.+)\]$"
            If .Execute(A1).Count > 0 Then
                C1 = Split(.Replace(A1, "$1"), ",")
                For D1 = LBound(C1) To UBound(C1)
                    B2.Add (D1 + 1), Function12(C1(D1))
                Next D1
                Set Function12 = B2
            Else
                .Pattern = "^""(.+)""$"
                If .Execute(A1).Count > 0 Then
                    Function12 = .Replace(A1, "$1")
                Else
                    .Pattern = "^\d+$"
                    If .Execute(A1).Count > 0 Then
                        Function12 = CInt(A1)
                    Else
                        If A1 = "false" Then
                            Function12 = False
                        ElseIf A1 = "true" Then
                            Function12 = True
                        End If
                    End If
                End If
            End If
        End If
    End With
End Function

Function Function13(ByVal A1 As Integer, ByVal A2 As String) As String
    Dim B1 As Object
    Dim C1 As String
    Set B1 = CreateObject("VBScript.RegExp")
    With B1
        .Global = True
        If A1 = 0 Then
            .Pattern = "[-\.]"
            Function13 = .Replace(A2, "")
        ElseIf A1 = 1 Then
            .Pattern = " "
            C1 = .Replace(A2, "")
            .Pattern = "[ÁÂÃ]"
            C1 = .Replace(C1, "A")
            .Pattern = "Ç"
            C1 = .Replace(C1, "C")
            .Pattern = "[ÉÊ]"
            C1 = .Replace(C1, "E")
            .Pattern = "Í"
            C1 = .Replace(C1, "I")
            .Pattern = "[ÓÔÕ]"
            C1 = .Replace(C1, "O")
            .Pattern = "[ÚÜ]"
            Function13 = .Replace(C1, "U")
        End If
    End With
End Function

Function Function14(ByVal A1 As String) As Object
    Dim B1() As String
    Dim C1 As Integer
    Dim D1 As Object
    Set D1 = CreateObject("Scripting.Dictionary")
    B1 = Split(A1, ",")
    For C1 = LBound(B1) To UBound(B1)
        D1.Add (C1 + 1), B1(C1)
    Next C1
    Set Function14 = D1
End Function

Function Function15(ByVal A1 As Object) As Boolean
    Dim B1 As Boolean
    B1 = False
    If ThisDocument.Variables("B6") = 1 Then
        If A1("Index3") = True Then
            If A1("Index4")("Index3") = 2 Or A1("Index4")("Index3") = 3 Then
                B1 = True
            End If
        End If
        If B1 = False Then
            If A1("Index5") > 0 Then
                For Each C1 In A1("Index6").Keys
                    If A1("Index6")(C1)("Index3") = 2 Or A1("Index6")(C1)("Index3") = 3 Then
                        B1 = True
                        Exit For
                    End If
                Next C1
            End If
        End If
    End If
    Function15 = B1
End Function

Function Function22(ByVal A1 As Integer, Optional ByVal A2 As Integer, Optional ByVal A3 As Integer, Optional ByVal A4 As Integer = 0, Optional ByVal A5 As String = "", Optional ByVal A6 As String = "", Optional ByVal A7 As String = "") As Object
    Dim B1 As Object, B2 As Object, B3 As Object
    Dim C1 As Integer, C2 As Boolean
    Set B1 = CreateObject("Scripting.Dictionary")
    Set B2 = CreateObject("Scripting.Dictionary")
    Set B3 = CreateObject("Scripting.Dictionary")
    B1.Add "Index2", Function1("Entre com o número do contrato:", 11, True)
    B1.Add "Index3", DateValue(CStr(Function1("Entre com a data do emprestimo:", 4, True)))
    B1.Add "Index4", Function1("Entre com o valor do emprestimo:", 5, True)
    B1.Add "Index5", Function1("Entre com a quantidade de parcelas:", 10, True)
    B1.Add "Index6", Function1("Entre com o valor da parcela:", 5, True)
    B1.Add "Index7", Function1("Entre com o valor disponibilizado:", 5, True)
    B1.Add "Index8", Function9("Têm seguro?")
    If B1("Index8") Then
        B1.Add "Index9", Function1("Entre com o valor do seguro:", 5, True)
    End If
    B1.Add "Index10", 1 'Function1("Entre com a forma de crédito (1 = DOC/TED/CC, 2 = OP, 3 = Cheque):", 6, True, IIf(A4 <> 0, A4, ""))
    B1.Add "Index11", Function1("Entre com o banco da transferência bancária:", 7, True)
    B1.Add "Index12", Function1("Entre com a agência da transferência bancária:", 8, True)
    B1.Add "Index13", Function1("Entre com a conta da transferência bancária:", 9, True)
    B1.Add "Index14", Function8("emprestimo", False)
    B1.Add "Index16", Function9("Têm desconto do empréstimo em conta?")
    If B1("Index16") Then
        B1.Add "Index17", DateValue(CStr(Function1("Entre com a primeira data do desconto:", 4, True)))
        B1.Add "Index18", Function1("Entre com o primeiro valor do desconto:", 5, True)
        B1.Add "Index19", DateValue(CStr(Function1("Entre com a última data do desconto:", 4, True)))
        B1.Add "Index20", Function1("Entre com o ultimo valor do desconto:", 5, True)
        B1.Add "Index21", Function8("Desconto", False)
    End If
    B1.Add "Index26", Function9("Houve refinanciamento?")
    If B1("Index26") = True Then
        C2 = True
        C1 = 0
        Do
            C1 = C1 + 1
            B3.Add C1, Function23(1, C1, C1)
            C2 = Function9("Outro refinanciamento?")
        Loop While C2
        B1.Add "Index27", Function5(B3)
    End If
    Set Function22 = B1
End FunctionConst Const1 As String = "Fox 3.1 - Cartão de crédito"
Const Const2 As Integer = 209
Public A1 As Object
Public A2 As Object
Public A3 As String

Private Sub Document_Open()
    If Application.Visible = True Then
        Call Sub1
    End If
End Sub

Sub Sub1()
    Dim A1 As Integer, A2 As Integer, A3 As Integer, A4 As Integer, A5 As Integer, A6 As Integer, A7 As Integer, A8 As Integer, A9 As Integer, A10 As Integer
    Dim B1 As Boolean, B2 As Boolean
    Dim C1 As Object, C2 As Object, C3(1 To 5) As Object
    
    Set ThisDocument.A1 = CreateObject("WinHttp.WinHttpRequest.5.1")
    Set ThisDocument.A2 = CreateObject("Scripting.Dictionary")
    Set C1 = CreateObject("Scripting.Dictionary")
    Set C2 = CreateObject("Scripting.Dictionary")
    For A1 = 1 To 5
        Set C3(A1) = CreateObject("Scripting.Dictionary")
    Next A1
    
    With ThisDocument
        If .Variables("A1") = 0 Then
            'Entre com o nome do cliente
            .Variables("B1") = Function1("Entre com o nome do cliente:", 0, True)
            Call Sub2(0, "%1%", .Variables("B1"))
            .Variables("A1") = 1
        End If
        If .Variables("A2") = 0 Then
            'Entre com o código da causa: CIV7777777
            .Variables("B2") = Function1("Entre com a CIV:", 3, True)
            Call Sub2(0, "%2%", .Variables("B2"))
            .Variables("A2") = 1
        End If
        If .Variables("A4") = 0 Then
            'Entre com o CPF do cliente
            .Variables("B4") = Function1("Entre com o CPF do cliente:", 1, True)
            Call Sub2(0, "%4%", .Variables("B4"))
            .Variables("A4") = 1
        End If
        If .Variables("A5") = 0 Then
            'Entre com o gênero do cliente
            .Variables("B5") = Function1("Entre com o gênero do cliente (1 = Masculino, 2 = Feminino):", 24, True)
            .Variables("A5") = 1
        End If
        If .Variables("A6") = 0 Then
            'Entre com o tipo da ação
            .Variables("B6") = Function1("Entre com o tipo da ação (1 = Alegação de fraude, 2 = Não reconhecimento da modalidade):", 26, True)
            If .Variables("B6") = 1 Then
                Call Sub2(0, "%5%", "Alegação de Fraude")
            ElseIf .Variables("B6") = 2 Then
                Call Sub2(0, "%5%", "Não Reconhecimento da Modalidade")
            End If
            .Variables("A6") = 1
        End If
        If .Variables("A3") = 0 Then
            'Entre com a quantidade de cartões do cliente
            A2 = Function1("Entre com a quantidade de cartões do cliente:", IIf(.Variables("B6") = 4 Or .Variables("B6") = 3, 29, 19), True)
            If A2 > 0 Then
                For A3 = 1 To A2
                    'Entre com o cartão do cliente
                    C1.Add A3, Function1("Entre com o cartão do cliente (" & A3 & " de " & A2 & "):", 2, True)
                    If A2 > 1 Then
                        'Entre com a matrícula do cliente
                        C2.Add A3, Function1("Entre com a matrícula do cliente (" & A3 & " de " & A2 & "):", 20, True)
                    End If
                Next A3
            Else
                C1.Add 1, ""
            End If
            .Variables("C1") = Join(C1.Items(), ",")
            If C1.Count > 1 Then
                .Variables("C2") = Join(C2.Items(), ",")
            End If
            If C1.Count > 1 Then
                Call Sub2(0, "%3%", "Cartões: %3%")
            ElseIf C1.Count = 1 Then
                Call Sub2(0, "%3%", "Cartão: %3%")
            Else
                Call Sub2(0, "%3%", "Cartão:")
            End If
            For Each D1 In C1.Keys
                If D1 > 1 Then
                    If D1 = C1.Count Then
                        Call Sub2(0, "%3%", " e " + C1(D1))
                    Else
                        Call Sub2(0, "%3%", ", " + C1(D1) + "%3%")
                    End If
                Else
                    Call Sub2(0, "%3%", C1(D1) + IIf(C1.Count > 1, "%3%", ""))
                End If
            Next
            .Variables("A3") = 1
        Else
            Set C1 = Function14(.Variables("C1"))
            If C1.Count > 1 Then
                Set C2 = Function14(.Variables("C2"))
            End If
        End If
        If .Variables("A8") = 0 Then
            '
            If .Variables("B6") = 4 Or .Variables("B6") = 3 Then
                Call Sub17(C1, C2, C3)
            Else
                Call Sub16(C1, C2, C3)
            End If
            '
            'Data do laudo
            If .Variables("A7") = 0 Then
                Call Sub2(0, "%6%", Format(Now, "dd/mm/yyyy"))
                .Variables("A7") = 1
            End If
            'Tipo do laudo
            B2 = False
            For Each D3 In C1.Keys
                If C3(D3)("Index9") > 0 Or C3(D3)("Index21") > 0 Or C3(D3)("Index23") > 0 Or C3(D3)("Index19") = True Or C3(D3)("Index25") = True Or C3(D3)("Index33") = True Or C3(D3)("Index37") = True Or C3(D3)("Index41") = True Or C3(D3)("Index45") > 0 Or C3(D3)("Index49") > 0 Or C3(D3)("Index53") > 0 Or Function15(C3(D3)) = True Then
                    B2 = True
                    Exit For
                End If
            Next
            If .Variables("B6") = 4 Then
                Call Sub2(0, "%7%", "Laudo de Subsídios")
            ElseIf .Variables("B6") = 3 Then
                Call Sub2(0, "%7%", "Laudo de Subsídios")
            Else
                If B2 = True Then
                    Call Sub2(0, "%7%", "Laudo de Subsídios")
                Else
                    Call Sub2(0, "%7%", "Laudo de Subsídios")
                End If
            End If
            '
            Call Sub3(0, "Escrevendo laudo! Aguarde!")
            A9 = 1
            Call Sub21(A9, C1, C2, C3)
            A9 = A9 + 1
            'HISTÓRICO DE ATENDIMENTOS PRESTADOS À CONSUMIDORA
            Call Sub22(A9, C1, C2, C3)
            A9 = A9 + 1
            Call Sub23(A9, C1, C2, C3)
            A9 = A9 + 1
            'TELAS DE FORMA DE ENVIO DE FATURAS E DE CONFIRMAÇÕES DE LEITURAS DE FATURAS
            Call Sub24(A9, C1, C2, C3)
            A9 = A9 + 1
            'TELAS DE FATURAS
            Call Sub25(A9, C1, C2, C3)
            A9 = A9 + 1
            'TELAS DE ACESSOS AO APLICATIVO
            Call Sub26(A9, C1, C2, C3)
            A9 = A9 + 1
            'TELAS DE ENVIOS DE SMS
            Call Sub27(A9, C1, C2, C3)
            A9 = A9 + 1
            'TELAS DE ENVIOS DE CÓPIAS DE CONTRATOS
            Call Sub28(A9, C1, C2, C3)
            A9 = A9 + 1
            'TELAS DE CÓDIGO DE RASTREIO
            Call Sub29(A9, C1, C2, C3)
            A9 = A9 + 1
            'TELAS DE OPERAÇÃO DUVIDOSA
            Call Sub30(A9, C1, C2, C3)
            A9 = A9 + 1
            'TELAS SEGUNDA INSTÂNCIA
            Call Sub31(A9, C1, C2, C3)
            A9 = A9 + 1
            
            'Alteração da fonte
            Selection.Find.Replacement.ClearFormatting
            Selection.Find.Replacement.Font.Bold = True
            With Selection.Find
                .Text = "\[0\](*)\[/0\]"
                .Replacement.Text = "\1"
                .Wrap = wdFindContinue
                .MatchWildcards = True
                .Execute Replace:=wdReplaceAll
            End With
            Selection.Find.Replacement.ClearFormatting
            Selection.Find.Replacement.Font.Bold = True
            Selection.Find.Replacement.Font.Underline = True
            With Selection.Find
                .Text = "\[1\](*)\[/1\]"
                .Replacement.Text = "\1"
                .Wrap = wdFindContinue
                .MatchWildcards = True
                .Execute Replace:=wdReplaceAll
            End With
            Selection.Find.Replacement.ClearFormatting
            Selection.Find.Replacement.Font.Italic = True
            With Selection.Find
                .Text = "\[2\](*)\[/2\]"
                .Replacement.Text = "\1"
                .Wrap = wdFindContinue
                .MatchWildcards = True
                .Execute Replace:=wdReplaceAll
            End With
            .GrammarChecked = True
            .SpellingChecked = True
            '
            'Forma nome do arquivo
            strDocName = ThisDocument.Path & "\"
            strDocName = strDocName & .Variables("B2") & "_" & .Variables("B1") & "_" & .Variables("B4")
            If .Variables("B6") = 4 Then
                strDocName = strDocName & " "
            ElseIf .Variables("B6") = 3 Then
                strDocName = strDocName & " "
            'Else
            '    strDocName = strDocName & " - "
            End If
            ' Salva documento docx
            ActiveDocument.SaveAs2 FileName:=strDocName & ".docx", _
                FileFormat:=wdFormatDocumentDefault
            
            ' Salva documento PDF
            'ActiveDocument.SaveAs2 FileName:=strDocName & ".pdf", _
            '    FileFormat:=wdFormatPDF
                
            Call Sub3(1)
            If B2 = True Then
                A10 = 1
            Else
                A10 = 2
            End If
            .Variables("A8") = 1
        End If
    End With
End Sub

Sub Sub2(ByVal A1 As Integer, ByVal A2 As String, ByVal A3 As String)
    Dim B1 As Object
    If A1 = 0 Then
        With ThisDocument.Range.Find
            .Text = A2
            .Replacement.Text = A3
            .Execute Replace:=wdReplaceAll
        End With
    ElseIf A1 = 1 Then
        Set B1 = ThisDocument.Content
        With B1.Find
            .Text = A2
            .Execute
            If .Found = True Then
                ThisDocument.Tables.Add Range:=B1, NumRows:=1, NumColumns:=1
                With ThisDocument.Tables(ThisDocument.Tables.Count)
                    With .Cell(0, 0)
                        With .Range
                            With .Font
                                .Size = 11
                                .Bold = True
                            End With
                            .Text = A3
                        End With
                        With .Borders
                            .Enable = True
                            .OutsideColor = wdColorGray25
                        End With
                        .VerticalAlignment = wdCellAlignVerticalCenter
                    End With
                    .Rows.Alignment = wdAlignRowCenter
                    .TopPadding = CentimetersToPoints(0.1)
                    .RightPadding = CentimetersToPoints(0.1)
                    .BottomPadding = CentimetersToPoints(0.1)
                    .LeftPadding = CentimetersToPoints(0.1)
                End With
            End If
        End With
    ElseIf A1 = 2 Then
        Set B1 = ThisDocument.Content
        With B1.Find
            .Text = A2
            .Execute
            If .Found = True Then
                ThisDocument.InlineShapes.AddPicture Range:=B1, FileName:=A3, LinkToFile:=False, SaveWithDocument:=True
            End If
            .Replacement.Text = ""
            .Execute Replace:=wdReplaceAll
        End With
    End If
End Sub

Sub Sub3(ByVal A1 As Integer, Optional ByVal A2 As String)
    With UserForm1
        If A1 = 0 Then
            .Caption = Const1
            With .Label1
                .Caption = A2
                .AutoSize = True
                .AutoSize = False
            End With
            .Width = (.Label1.Width + 63)
            .Show
            .Repaint
        ElseIf A1 = 1 Then
            .Hide
        End If
    End With
End Sub

Sub Sub5(ByVal A1 As String, ByVal A2 As String, ByVal A3 As String)
    With ThisDocument
        If .Variables("A1") = 0 Then
            .Variables("B1") = A1
            Call Sub2(0, "%1%", .Variables("B1"))
            .Variables("A1") = 1
        End If
        If .Variables("A2") = 0 Then
            .Variables("B2") = A2
            Call Sub2(0, "%2%", .Variables("B2"))
            .Variables("A2") = 1
        End If
        If .Variables("A4") = 0 Then
            .Variables("B4") = A3
            Call Sub2(0, "%4%", .Variables("B4"))
            .Variables("A4") = 1
        End If
    End With
End Sub

Sub Sub16(ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    Dim C4 As Object, C5 As Object, C6 As Object, C7 As Object
    Set C4 = CreateObject("Scripting.Dictionary")
    Set C5 = CreateObject("Scripting.Dictionary")
    Set C6 = CreateObject("Scripting.Dictionary")
    Set C7 = CreateObject("Scripting.Dictionary")
    With ThisDocument
        For Each D2 In C1.Keys
            If C1.Count > 1 Then
                MsgBox Prompt:="As próximas caixas de diálogo serão referentes ao cartão " + C1(D2) + ".", Title:=Const1
            End If
            C3(D2)("Index1") = DateValue(CStr(Function1("Entre com a data da contratação do cartão:", 4, True)))
            Set C3(D2)("Index2") = Function8("contratação do cartão", False)
            C3(D2)("Index3") = Function9("Houve contratação de saque autorizado?")
            If C3(D2)("Index3") = True Then
                Set C3(D2)("Index4") = Function2(0)
            End If
            C3(D2)("Index5") = Function1("Entre com a quantidade de saques complementares:", 10, True)
            If C3(D2)("Index5") > 0 Then
                For A4 = 1 To C3(D2)("Index5")
                    If A4 = 1 Then
                        If C3(D2)("Index3") = True Then
                            C4.Add A4, Function2(1, A4, C3(D2)("Index5"), C3(D2)("Index4")("Index3"), C3(D2)("Index4")("Index4"), C3(D2)("Index4")("Index5"), C3(D2)("Index4")("Index6"))
                        Else
                            C4.Add A4, Function2(1, A4, C3(D2)("Index5"))
                        End If
                    Else
                        C4.Add A4, Function2(1, A4, C3(D2)("Index5"), C4(A4 - 1)("Index3"), C4(A4 - 1)("Index4"), C4(A4 - 1)("Index5"), C4(A4 - 1)("Index6"))
                    End If
                Next A4
                Set C3(D2)("Index6") = Function5(C4)
            End If
            C3(D2)("Index7") = (IIf(C3(D2)("Index3") = True, 1, 0) + C3(D2)("Index5"))
            If C3(D2)("Index7") > 1 Then
                B1 = True
                If C3(D2)("Index3") = True Then
                    For A5 = 1 To C3(D2)("Index5")
                        If C3(D2)("Index4")("Index3") <> C3(D2)("Index6")(A5)("Index3") Or C3(D2)("Index4")("Index4") <> C3(D2)("Index6")(A5)("Index4") Or C3(D2)("Index4")("Index5") <> C3(D2)("Index6")(A5)("Index5") Or C3(D2)("Index4")("Index6") <> C3(D2)("Index6")(A5)("Index6") Then
                            B1 = False
                            Exit For
                        End If
                    Next A5
                Else
                    For A5 = 2 To C3(D2)("Index5")
                        If C3(D2)("Index6")(1)("Index3") <> C3(D2)("Index6")(A5)("Index3") Or C3(D2)("Index6")(1)("Index4") <> C3(D2)("Index6")(A5)("Index4") Or C3(D2)("Index6")(1)("Index5") <> C3(D2)("Index6")(A5)("Index5") Or C3(D2)("Index6")(1)("Index6") <> C3(D2)("Index6")(A5)("Index6") Then
                            B1 = False
                            Exit For
                        End If
                    Next A5
                End If
                C3(D2)("Index8") = B1
            End If
            C3(D2)("Index9") = Function1("Entre com a quantidade de saques eletrônicos:", 10, True)
            If C3(D2)("Index9") > 0 Then
                For A6 = 1 To C3(D2)("Index9")
                    C5.Add A6, Function2(2, A6, C3(D2)("Index9"))
                Next A6
                Set C3(D2)("Index10") = Function5(C5)
            End If
            C3(D2)("Index11") = (C3(D2)("Index7") + C3(D2)("Index9"))
            If C3(D2)("Index11") > 0 Then
                Set C3(D2)("Index12") = Function8("saques", True)
            End If
            C3(D2)("Index13") = Function9("Houve descontos em folha?")
            If C3(D2)("Index13") = True Then
                Do
                    C3(D2)("Index14") = DateValue(CStr(Function1("Entre com a data do primeiro desconto em folha:", 4, True)))
                    C3(D2)("Index15") = Function1("Entre com o valor do primeiro desconto em folha:", 5, True)
                    C3(D2)("Index16") = DateValue(CStr(Function1("Entre com a data do último desconto em folha:", 4, True)))
                    C3(D2)("Index17") = Function1("Entre com o valor do último desconto em folha:", 5, True)
                    If C3(D2)("Index14") > C3(D2)("Index16") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index14") > C3(D2)("Index16")
            End If
            Set C3(D2)("Index18") = Function8(IIf(C3(D2)("Index13") = True, "descontos em folha", "ausência de descontos em folha"), True)
            C3(D2)("Index19") = Function9("O cartão está apto para uso?")
            If C3(D2)("Index19") = True Then
                Set C3(D2)("Index20") = Function8("status do cartão e da sua conta", False)
            End If
            C3(D2)("Index21") = Function1("Entre com a quantidade de atendimentos humanos:", 10, True)
            If C3(D2)("Index21") > 0 Then
                For A7 = 1 To C3(D2)("Index21")
                    C6.Add A7, Function7(0, A7, C3(D2)("Index21"))
                Next A7
                Set C3(D2)("Index22") = Function5(C6)
            End If
            C3(D2)("Index23") = Function1("Entre com a quantidade de atendimentos eletrônicos:", 10, True)
            If C3(D2)("Index23") > 0 Then
                For A8 = 1 To C3(D2)("Index23")
                    C7.Add A8, Function7(1, A8, C3(D2)("Index23"))
                Next A8
                Set C3(D2)("Index24") = Function5(C7)
            End If
            C3(D2)("Index25") = Function9("Há histórico de pré-venda?")
            If C3(D2)("Index25") = True Then
                C3(D2)("Index26") = Function1("Entre com o número da adesão:", 16, True)
                C3(D2)("Index27") = Function1("Entre com a data e a hora do contato:", 12, True)
                C3(D2)("Index28") = Function1("Entre com a descrição do contato (opcional):", 17, False)
            End If
            C3(D2)("Index29") = Function9("A forma de envio de faturas está disponível?")
            If C3(D2)("Index29") = True Then
                C3(D2)("Index30") = Function1("Entre com a forma de envio de faturas (1 = Online, 2 = Correio, 3 = Email, 4 = Correio + Email, 5 = SMS):", 15, True)
                Set C3(D2)("Index31") = Function8("forma de envio de faturas", False)
                If C3(D2)("Index30") = 2 Or C3(D2)("Index30") = 4 Then
                    Set C3(D2)("Index32") = Function8("endereço", False)
                End If
            End If
            C3(D2)("Index33") = Function9("Houve confirmações de leituras de faturas?")
            If C3(D2)("Index33") = True Then
                Do
                    C3(D2)("Index34") = DateValue(CStr(Function1("Entre com a data da primeira confirmação de leitura de fatura:", 4, True)))
                    C3(D2)("Index35") = DateValue(CStr(Function1("Entre com a data da última confirmação de leitura de fatura:", 4, True)))
                    If C3(D2)("Index34") > C3(D2)("Index35") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index34") > C3(D2)("Index35")
                Set C3(D2)("Index36") = Function8("confirmações de leituras de faturas", True)
            End If
            C3(D2)("Index37") = Function9("Houve compras?")
            If C3(D2)("Index37") = True Then
                Do
                    C3(D2)("Index38") = DateValue(CStr(Function1("Entre com a data da primeira compra:", 4, True)))
                    C3(D2)("Index39") = DateValue(CStr(Function1("Entre com a data da última compra:", 4, True)))
                    If C3(D2)("Index38") > C3(D2)("Index39") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index38") > C3(D2)("Index39")
                Set C3(D2)("Index40") = Function8("compras", True)
            End If
            C3(D2)("Index41") = Function9("Houve pagamentos de faturas?")
            If C3(D2)("Index41") = True Then
                Do
                    C3(D2)("Index42") = DateValue(CStr(Function1("Entre com a data do primeiro pagamento de fatura:", 4, True)))
                    C3(D2)("Index43") = DateValue(CStr(Function1("Entre com a data do último pagamento de fatura:", 4, True)))
                    If C3(D2)("Index42") > C3(D2)("Index43") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index42") > C3(D2)("Index43")
                Set C3(D2)("Index44") = Function8("pagamentos de faturas", True)
            End If
            C3(D2)("Index45") = Function1("Entre com a quantidade de acessos ao aplicativo:", 10, True)
            If C3(D2)("Index45") > 0 Then
                Do
                    C3(D2)("Index46") = DateValue(CStr(Function1("Entre com a data do primeiro acesso ao aplicativo:", 4, True)))
                    C3(D2)("Index47") = DateValue(CStr(Function1("Entre com a data do último acesso ao aplicativo:", 4, True)))
                    If C3(D2)("Index46") > C3(D2)("Index47") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index46") > C3(D2)("Index47")
                Set C3(D2)("Index48") = Function8("acessos ao aplicativo", True)
            End If
            C3(D2)("Index49") = Function1("Entre com a quantidade de envios de SMS:", 10, True)
            If C3(D2)("Index49") > 0 Then
                Do
                    C3(D2)("Index50") = DateValue(CStr(Function1("Entre com a data do primeiro envio de SMS:", 4, True)))
                    C3(D2)("Index51") = DateValue(CStr(Function1("Entre com a data do último envio de SMS:", 4, True)))
                    If C3(D2)("Index50") > C3(D2)("Index51") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index50") > C3(D2)("Index51")
                Set C3(D2)("Index52") = Function8("envios de SMS", True)
            End If
            C3(D2)("Index53") = Function1("Entre com a quantidade de envios de cópias de contratos:", 10, True)
            If C3(D2)("Index53") > 0 Then
                Do
                    C3(D2)("Index54") = DateValue(CStr(Function1("Entre com a data do primeiro envio de cópia de contrato:", 4, True)))
                    C3(D2)("Index55") = DateValue(CStr(Function1("Entre com a data do último envio de cópia de contrato:", 4, True)))
                    If C3(D2)("Index54") > C3(D2)("Index55") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index54") > C3(D2)("Index55")
                Set C3(D2)("Index56") = Function8("envios de cópias de contratos", True)
            End If
            
            'Código de rastreio
            C3(D2)("Index79") = Function1("Houve código de rastreio no envio do cartão? (0 = Não / 1 = Sim)", 10, True)
            If C3(D2)("Index79") > 0 Then
                'Do
                   'C3(D2)("Index74") = DateValue(CStr(Function1("Entre com a data do código de rastreio:", 4, True)))
                    'C3(D2)("Index75") = DateValue(CStr(Function1("Confirme com a data do código de rastreio:", 4, True)))
                    'If C3(D2)("Index74") > C3(D2)("Index75") Then
                    '    MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    'End If
                'Loop While C3(D2)("Index74") > C3(D2)("Index75")
                Set C3(D2)("Index72") = Function8("Código de rastreio", True)
            End If
            
            'Houve operação duvidosa
            C3(D2)("Index89") = Function1("Houve operação duvidosa? (0 = Não / 1 = Sim)", 10, True)
            If C3(D2)("Index89") > 0 Then
                'Do
                    'C3(D2)("Index84") = DateValue(CStr(Function1("Entre com a data do primeiro atendimento de operação duvidosa:", 4, True)))
                    'C3(D2)("Index85") = DateValue(CStr(Function1("Entre com a data do último atendimento de operação duvidosa:", 4, True)))
                    'If C3(D2)("Index84") > C3(D2)("Index85") Then
                     '   MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    'End If
                'Loop While C3(D2)("Index84") > C3(D2)("Index85")
                Set C3(D2)("Index82") = Function8("Operação Duvidosa", True)
            End If
            
            'Atendimento segunda instância
            C3(D2)("Index99") = Function1("Houve atendimento em segunda instância? (0 = Não / 1 = Sim)", 10, True)
            If C3(D2)("Index99") > 0 Then
                'Do
                    'C3(D2)("Index94") = DateValue(CStr(Function1("Entre com a data do primeiro atendimento na segunda instância:", 4, True)))
                    'C3(D2)("Index95") = DateValue(CStr(Function1("Entre com a data do último atendimento na segunda instância:", 4, True)))
                    'If C3(D2)("Index94") > C3(D2)("Index95") Then
                        'MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    'End If
                'Loop While C3(D2)("Index94") > C3(D2)("Index95")
                Set C3(D2)("Index92") = Function8("Segunda Instância", True)
            End If
            
        Next
    End With
End Sub

Sub Sub17(ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    Dim C4 As Object, C5 As Object, C6 As Object, C7 As Object, C8 As Object, C9 As Object
    Dim A1 As Integer, A2 As Boolean
    Set C4 = CreateObject("Scripting.Dictionary")
    Set C5 = CreateObject("Scripting.Dictionary")
    Set C6 = CreateObject("Scripting.Dictionary")
    Set C7 = CreateObject("Scripting.Dictionary")
    Set C8 = CreateObject("Scripting.Dictionary")
    Set C9 = CreateObject("Scripting.Dictionary")
    With ThisDocument
        For Each D2 In C1.Keys
            If C1.Count > 1 Then
                MsgBox Prompt:="As próximas caixas de diálogo serão referentes ao cartão " + C1(D2) + ".", Title:=Const1
            End If
            '
            'Entre com a data da contratação do cartão
            C3(D2)("Index1") = DateValue(CStr(Function1("Entre com a data da contratação do cartão:", 4, True)))
            'Imagem capturada
            Set C3(D2)("Index2") = Function8("contratação do cartão", False)
            '
            'Houve contratação de saque autorizado? [Sim/Não/Cancelar]
            'C3(D2)("Index3") = Function9("Houve contratação de saque autorizado?")
            C3(D2)("Index3") = False
            If C3(D2)("Index3") = True Then
                'Saques
                Set C3(D2)("Index4") = Function2(0)
            End If
            '
            'Entre com a quantidade de saques complementares
            'C3(D2)("Index5") = Function1("Entre com a quantidade de saques complementares:", 10, True)
            C3(D2)("Index5") = 0
            If C3(D2)("Index5") > 0 Then
                For A4 = 1 To C3(D2)("Index5")
                    If A4 = 1 Then
                        If C3(D2)("Index3") = True Then
                            'Saques
                            C4.Add A4, Function2(1, A4, C3(D2)("Index5"), C3(D2)("Index4")("Index3"), C3(D2)("Index4")("Index4"), C3(D2)("Index4")("Index5"), C3(D2)("Index4")("Index6"))
                        Else
                            'Saques
                            C4.Add A4, Function2(1, A4, C3(D2)("Index5"))
                        End If
                    Else
                        'Saques
                        C4.Add A4, Function2(1, A4, C3(D2)("Index5"), C4(A4 - 1)("Index3"), C4(A4 - 1)("Index4"), C4(A4 - 1)("Index5"), C4(A4 - 1)("Index6"))
                    End If
                Next A4
                Set C3(D2)("Index6") = Function5(C4)
            End If
            C3(D2)("Index7") = (IIf(C3(D2)("Index3") = True, 1, 0) + C3(D2)("Index5"))
            If C3(D2)("Index7") > 1 Then
                B1 = True
                If C3(D2)("Index3") = True Then
                    For A5 = 1 To C3(D2)("Index5")
                        If C3(D2)("Index4")("Index3") <> C3(D2)("Index6")(A5)("Index3") Or C3(D2)("Index4")("Index4") <> C3(D2)("Index6")(A5)("Index4") Or C3(D2)("Index4")("Index5") <> C3(D2)("Index6")(A5)("Index5") Or C3(D2)("Index4")("Index6") <> C3(D2)("Index6")(A5)("Index6") Then
                            B1 = False
                            Exit For
                        End If
                    Next A5
                Else
                    For A5 = 2 To C3(D2)("Index5")
                        If C3(D2)("Index6")(1)("Index3") <> C3(D2)("Index6")(A5)("Index3") Or C3(D2)("Index6")(1)("Index4") <> C3(D2)("Index6")(A5)("Index4") Or C3(D2)("Index6")(1)("Index5") <> C3(D2)("Index6")(A5)("Index5") Or C3(D2)("Index6")(1)("Index6") <> C3(D2)("Index6")(A5)("Index6") Then
                            B1 = False
                            Exit For
                        End If
                    Next A5
                End If
                C3(D2)("Index8") = B1
            End If
            '
            'Entre com a quantidade de saques eletrônicos
            'C3(D2)("Index9") = Function1("Entre com a quantidade de saques eletrônicos:", 10, True)
            C3(D2)("Index9") = 0
            If C3(D2)("Index9") > 0 Then
                For A6 = 1 To C3(D2)("Index9")
                    'Saques
                    C5.Add A6, Function2(2, A6, C3(D2)("Index9"))
                Next A6
                Set C3(D2)("Index10") = Function5(C5)
            End If
            'C3(D2)("Index11") = (C3(D2)("Index7") + C3(D2)("Index9"))
            C3(D2)("Index11") = 0
            If C3(D2)("Index11") > 0 Then
                'Imagem capturada
                Set C3(D2)("Index12") = Function8("saques", True)
            End If
            '
            'Houve emprestimos? [Sim/Não/Cancelar]
            'C3(D2)("Index76") = Function9("Houve emprestimos?")
            C3(D2)("Index76") = True
            If C3(D2)("Index76") = True Then
                A2 = True
                A1 = 0
                Do
                    A1 = A1 + 1
                    C8.Add A1, Function22(1, A1, A1)
                    A2 = Function9("Outro emprestimos?")
                Loop While A2
                Set C3(D2)("Index78") = Function5(C8)
            End If
            '
            'Houve pagamento mínimo? [Sim/Não/Cancelar]
            C3(D2)("Index57") = Function9("Houve pagamento mínimo?")
            If C3(D2)("Index57") = True Then
                A1 = 0
                Do
                    A1 = A1 + 1
                    C9.Add A1, Function24(1, A1, A1)
                Loop While A1 < 2
                Set C3(D2)("Index58") = Function5(C9)
            End If
            '
            'Houve descontos em folha? [Sim/Não/Cancelar]
            'C3(D2)("Index13") = Function9("Houve descontos em folha?")
            C3(D2)("Index13") = False
            If C3(D2)("Index13") = True Then
                Do
                    'Entre com a data do primeiro desconto em folha
                    C3(D2)("Index14") = DateValue(CStr(Function1("Entre com a data do primeiro desconto em folha:", 4, True)))
                    'Entre com o valor do primeiro desconto em folha
                    C3(D2)("Index15") = Function1("Entre com o valor do primeiro desconto em folha:", 5, True)
                    'Entre com a data do último desconto em folha
                    C3(D2)("Index16") = DateValue(CStr(Function1("Entre com a data do último desconto em folha:", 4, True)))
                    'Entre com o valor do último desconto em folha
                    C3(D2)("Index17") = Function1("Entre com o valor do último desconto em folha:", 5, True)
                    If C3(D2)("Index14") > C3(D2)("Index16") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index14") > C3(D2)("Index16")
            End If
            'Imagem capturada
            'Set C3(D2)("Index18") = Function8(IIf(C3(D2)("Index13") = True, "descontos em folha", "ausência de descontos em folha"), True)
            '
            'O cartão está apto para uso? [Sim/Não/Cancelar]
            C3(D2)("Index19") = Function9("O cartão está apto para uso?")
            If C3(D2)("Index19") = True Then
                'Imagem capturada
                Set C3(D2)("Index20") = Function8("status do cartão e da sua conta", False)
            End If
            '
            'Entre com a quantidade de atendimentos humanos
            C3(D2)("Index21") = Function1("Entre com a quantidade de atendimentos humanos:", 10, True)
            If C3(D2)("Index21") > 0 Then
                For A7 = 1 To C3(D2)("Index21")
                    'Atendimento
                    C6.Add A7, Function7(0, A7, C3(D2)("Index21"))
                Next A7
                Set C3(D2)("Index22") = Function5(C6)
            End If
            '
            'Entre com a quantidade de atendimentos eletrônicos
            C3(D2)("Index23") = Function1("Entre com a quantidade de atendimentos eletrônicos:", 10, True)
            If C3(D2)("Index23") > 0 Then
                For A8 = 1 To C3(D2)("Index23")
                    'Atendimento
                    C7.Add A8, Function7(1, A8, C3(D2)("Index23"))
                Next A8
                Set C3(D2)("Index24") = Function5(C7)
            End If
            '
            'Há histórico de pré-venda? [Sim/Não/Cancelar]
            C3(D2)("Index25") = Function9("Há histórico de pré-venda?")
            If C3(D2)("Index25") = True Then
                C3(D2)("Index26") = Function1("Entre com o número da adesão:", 16, True)
                C3(D2)("Index27") = Function1("Entre com a data e a hora do contato:", 12, True)
                C3(D2)("Index28") = Function1("Entre com a descrição do contato (opcional):", 17, False)
            End If
            '
            'A forma de envio de faturas está disponível? [Sim/Não/Cancelar]
            C3(D2)("Index29") = Function9("A forma de envio de faturas está disponível?")
            If C3(D2)("Index29") = True Then
                C3(D2)("Index30") = Function1("Entre com a forma de envio de faturas (1 = Online, 2 = Correio, 3 = Email, 4 = Correio + Email, 5 = SMS):", 15, True)
                'Imagem capturada
                Set C3(D2)("Index31") = Function8("forma de envio de faturas", False)
                If C3(D2)("Index30") = 2 Or C3(D2)("Index30") = 4 Then
                    'Imagem capturada
                    Set C3(D2)("Index32") = Function8("endereço", False)
                End If
            End If
            '
            'Houve confirmações de leituras de faturas? [Sim/Não/Cancelar]
            C3(D2)("Index33") = Function9("Houve confirmações de leituras de faturas?")
            If C3(D2)("Index33") = True Then
                Do
                    'Entre com a data da primeira confirmação de leitura de fatura
                    C3(D2)("Index34") = DateValue(CStr(Function1("Entre com a data da primeira confirmação de leitura de fatura:", 4, True)))
                    'Entre com a data da última confirmação de leitura de fatura
                    C3(D2)("Index35") = DateValue(CStr(Function1("Entre com a data da última confirmação de leitura de fatura:", 4, True)))
                    If C3(D2)("Index34") > C3(D2)("Index35") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index34") > C3(D2)("Index35")
                'Imagem capturada
                Set C3(D2)("Index36") = Function8("confirmações de leituras de faturas", True)
            End If
            
            'Houve compras? [Sim/Não/Cancelar]
            C3(D2)("Index37") = Function9("Houve compras?")
            If C3(D2)("Index37") = True Then
                Do
                    'Entre com a data da primeira compra
                    C3(D2)("Index38") = DateValue(CStr(Function1("Entre com a data da primeira compra:", 4, True)))
                    'Entre com a data da última compra
                    C3(D2)("Index39") = DateValue(CStr(Function1("Entre com a data da última compra:", 4, True)))
                    If C3(D2)("Index38") > C3(D2)("Index39") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index38") > C3(D2)("Index39")
                'Imagem capturada
                Set C3(D2)("Index40") = Function8("compras", True)
            End If
            '
            'Houve pagamentos de faturas? [Sim/Não/Cancelar]
            C3(D2)("Index41") = Function9("Houve pagamentos de faturas?")
            If C3(D2)("Index41") = True Then
                Do
                    'Entre com a data do primeiro pagamento de fatura
                    C3(D2)("Index42") = DateValue(CStr(Function1("Entre com a data do primeiro pagamento de fatura:", 4, True)))
                    'Entre com a data do último pagamento de fatura
                    C3(D2)("Index43") = DateValue(CStr(Function1("Entre com a data do último pagamento de fatura:", 4, True)))
                    If C3(D2)("Index42") > C3(D2)("Index43") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index42") > C3(D2)("Index43")
                'Imagem capturada
                Set C3(D2)("Index44") = Function8("pagamentos de faturas", True)
            End If
            '
            'Entre com a quantidade de acessos ao aplicativo
            C3(D2)("Index45") = Function1("Entre com a quantidade de acessos ao aplicativo:", 10, True)
            If C3(D2)("Index45") > 0 Then
                Do
                    'Entre com a data do primeiro acesso ao aplicativo
                    C3(D2)("Index46") = DateValue(CStr(Function1("Entre com a data do primeiro acesso ao aplicativo:", 4, True)))
                    'Entre com a data do último acesso ao aplicativo
                    C3(D2)("Index47") = DateValue(CStr(Function1("Entre com a data do último acesso ao aplicativo:", 4, True)))
                    If C3(D2)("Index46") > C3(D2)("Index47") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index46") > C3(D2)("Index47")
                'Imagem capturada
                Set C3(D2)("Index48") = Function8("acessos ao aplicativo", True)
            End If
            '
            'Entre com a quantidade de envios de SMS
            C3(D2)("Index49") = Function1("Entre com a quantidade de envios de SMS:", 10, True)
            If C3(D2)("Index49") > 0 Then
                Do
                    'Entre com a data do primeiro envio de SMS
                    C3(D2)("Index50") = DateValue(CStr(Function1("Entre com a data do primeiro envio de SMS:", 4, True)))
                    'Entre com a data do último envio de SMS
                    C3(D2)("Index51") = DateValue(CStr(Function1("Entre com a data do último envio de SMS:", 4, True)))
                    If C3(D2)("Index50") > C3(D2)("Index51") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index50") > C3(D2)("Index51")
                'Imagem capturada
                Set C3(D2)("Index52") = Function8("envios de SMS", True)
            End If
            '
            'Entre com a quantidade de envios de cópias de contratos
            C3(D2)("Index53") = Function1("Entre com a quantidade de envios de cópias de contratos:", 10, True)
            If C3(D2)("Index53") > 0 Then
                Do
                    'Entre com a data do primeiro envio de cópia de contrato
                    C3(D2)("Index54") = DateValue(CStr(Function1("Entre com a data do primeiro envio de cópia de contrato:", 4, True)))
                    'Entre com a data do último envio de cópia de contrato
                    C3(D2)("Index55") = DateValue(CStr(Function1("Entre com a data do último envio de cópia de contrato:", 4, True)))
                    If C3(D2)("Index54") > C3(D2)("Index55") Then
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                Loop While C3(D2)("Index54") > C3(D2)("Index55")
                'Imagem capturada
                Set C3(D2)("Index56") = Function8("envios de cópias de contratos", True)
            End If

            'Código de rastreio
            C3(D2)("Index79") = Function1("Houve código de rastreio no envio do cartão? (0 = Não / 1 = Sim)", 10, True)
            If C3(D2)("Index79") > 0 Then
                'Do
                    'C3(D2)("Index74") = DateValue(CStr(Function1("Entre com a data do código de rastreio:", 4, True)))
                    'C3(D2)("Index75") = DateValue(CStr(Function1("Confirme com a data do código de rastreio:", 4, True)))
                    'If C3(D2)("Index74") > C3(D2)("Index75") Then
                        'MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    'End If
                Loop While C3(D2)("Index74") > C3(D2)("Index75")
                Set C3(D2)("Index72") = Function8("Código de rastreio", True)
            End If
            
            'Houve operação duvidosa?
            C3(D2)("Index89") = Function1("Houve operação duvidosa? (0 = Não / 1 = Sim)", 10, True)
            If C3(D2)("Index89") > 0 Then
                'Do
                    'C3(D2)("Index84") = DateValue(CStr(Function1("Entre com a data do primeiro atendimento de operação duvidosa:", 4, True)))
                    'C3(D2)("Index85") = DateValue(CStr(Function1("Entre com a data do último atendimento de operação duvidosa:", 4, True)))
                    'If C3(D2)("Index84") > C3(D2)("Index85") Then
                        'MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    'End If
                'Loop While C3(D2)("Index84") > C3(D2)("Index85")
                Set C3(D2)("Index82") = Function8("Operação Duvidosa", True)
            End If
            
            'Atendimento segunda instância
            C3(D2)("Index99") = Function1("Houve atendimento em segunda instância?  (0 = Não / 1 = Sim)", 10, True)
            If C3(D2)("Index99") > 0 Then
                'Do
                    'C3(D2)("Index94") = DateValue(CStr(Function1("Entre com a data do primeiro atendimento na segunda instância:", 4, True)))
                    'C3(D2)("Index95") = DateValue(CStr(Function1("Entre com a data do último atendimento na segunda instância:", 4, True)))
                    'If C3(D2)("Index94") > C3(D2)("Index95") Then
                        'MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    'End If
                'Loop While C3(D2)("Index94") > C3(D2)("Index95")
                Set C3(D2)("Index92") = Function8("Segunda Instância", True)
            End If
            
        Next
    End With
End Sub

Sub Sub21(ByVal A9 As Integer, ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    With ThisDocument
        Call Sub2(1, "%8%", A9 & " - PARECER SUCINTO SOBRE " & IIf(.Variables("B5") = 1, "O", IIf(.Variables("B5") = 2, "A", "")) & " CLIENTE")
        With .Content
            If C1.Count > 1 Then
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                .InsertAfter Text:="Diante desta ação de " & IIf(ThisDocument.Variables("B6") = 1, "alegação de fraude", IIf(ThisDocument.Variables("B6") = 2, "não reconhecimento da modalidade", "")) & ", identificamos as contratações de [0]" & Function6(0, CLng(C1.Count)) & " cartões[/0]."
                For Each D4 In C1.Keys
                    If D4 > 1 Then
                        If D4 = C1.Count Then
                            .InsertAfter Text:=" e"
                        Else
                            .InsertAfter Text:=","
                        End If
                    End If
                    .InsertAfter Text:=" " & IIf(D4 = 1, "O", "o") & " cartão [0]" & C1(D4) & "[/0] é vinculado à matrícula [0]" & C2(D4) & "[/0]"
                Next
                .InsertAfter Text:="."
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            End If
            For Each D5 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D5) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                .InsertAfter Text:=IIf(C1.Count = 1, "Diante desta ação de " & IIf(ThisDocument.Variables("B6") = 1, "alegação de fraude", IIf(ThisDocument.Variables("B6") = 2, "não reconhecimento da modalidade", "")) & ", identificamos", "Identificamos") & " a contratação do cartão em [0]" & Format(C3(D5)("Index1"), "yyyy") & "[/0]."
                If C3(D5)("Index3") = True Then
                    .InsertAfter Text:=" No ato desta contratação, foi solicitado um [0]saque autorizado[/0] no valor de " & Function4(C3(D5)("Index4")("Index1")) & ", o qual foi disponibilizado"
                    If Format(C3(D5)("Index4")("Index2"), "yyyy") = Format(C3(D5)("Index1"), "yyyy") Then
                        .InsertAfter Text:=" neste mesmo ano"
                    Else
                        .InsertAfter Text:=" em [0]" & Format(C3(D5)("Index4")("Index2"), "yyyy") & "[/0]"
                    End If
                    If IIf(C3(D5)("Index7") > 1, C3(D5)("Index8"), False) = True Then
                        .InsertAfter Text:="."
                    Else
                        .InsertAfter Text:=" através de"
                        If C3(D5)("Index4")("Index3") = 1 Then
                            .InsertAfter Text:=" [0]transferência bancária[/0] " & IIf((C3(D5)("Index4")("Index4") = "085" Or C3(D5)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(C3(D5)("Index4")("Index4")) & "[/0], na agência [0]" & C3(D5)("Index4")("Index5") & "[/0], na conta [0]" & C3(D5)("Index4")("Index6") & "[/0]."
                        ElseIf C3(D5)("Index4")("Index3") = 2 Then
                            .InsertAfter Text:=" [0]ordem de pagamento[/0] " & IIf((C3(D5)("Index4")("Index4") = "085" Or C3(D5)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(C3(D5)("Index4")("Index4")) & "[/0], na agência [0]" & C3(D5)("Index4")("Index5") & "[/0]."
                        ElseIf C3(D5)("Index4")("Index3") = 3 Then
                            .InsertAfter Text:=" [0]cheque[/0] " & IIf((C3(D5)("Index4")("Index4") = "085" Or C3(D5)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(C3(D5)("Index4")("Index4")) & "[/0], na agência [0]" & C3(D5)("Index4")("Index5") & "[/0], na conta [0]" & C3(D5)("Index4")("Index6") & "[/0]."
                        End If
                    End If
                    If C3(D5)("Index4")("Index7") = True Then
                        .InsertAfter Text:=" Este saque foi parcelado em [0]" & C3(D5)("Index4")("Index8") & " parcelas[/0] de " & Function4(C3(D5)("Index4")("Index9")) & "."
                    End If
                End If
                If C3(D5)("Index5") > 0 Then
                    If Format(C3(D5)("Index6")(1)("Index2"), "yyyy") = Format(C3(D5)("Index6")(C3(D5)("Index5"))("Index2"), "yyyy") Then
                        .InsertAfter Text:=" Em [0]" & Format(C3(D5)("Index6")(1)("Index2"), "yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" Entre [0]" & Format(C3(D5)("Index6")(1)("Index2"), "yyyy") & "[/0] e [0]" & Format(C3(D5)("Index6")(C3(D5)("Index5"))("Index2"), "yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", " & IIf(C3(D5)("Index5") = 1, "foi disponibilizado", "foram disponibilizados") & " [0]" & Function6(0, CLng(C3(D5)("Index5"))) & " " & IIf(C3(D5)("Index5") = 1, "saque complementar", "saques complementares") & "[/0] " & IIf(C3(D5)("Index5") = 1, "no valor", "nos valores") & " de"
                    For Each D6 In C3(D5)("Index6").Keys
                        If D6 > 1 Then
                            If D6 = C3(D5)("Index5") Then
                                .InsertAfter Text:=" e"
                            Else
                                .InsertAfter Text:=","
                            End If
                        End If
                        .InsertAfter Text:=" " & Function4(CDbl(C3(D5)("Index6")(D6)("Index1")))
                        If IIf(C3(D5)("Index7") > 1, C3(D5)("Index8"), False) = False Then
                            .InsertAfter Text:=" através de"
                            If C3(D5)("Index6")(D6)("Index3") = 1 Then
                                .InsertAfter Text:=" [0]transferência bancária[/0] " & IIf((C3(D5)("Index6")(D6)("Index4") = "085" Or C3(D5)("Index6")(D6)("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D5)("Index6")(D6)("Index4"))) & "[/0], na agência [0]" & C3(D5)("Index6")(D6)("Index5") & "[/0], na conta [0]" & C3(D5)("Index6")(D6)("Index6") & "[/0]"
                            ElseIf C3(D5)("Index6")(D6)("Index3") = 2 Then
                                .InsertAfter Text:=" [0]ordem de pagamento[/0] " & IIf((C3(D5)("Index6")(D6)("Index4") = "085" Or C3(D5)("Index6")(D6)("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D5)("Index6")(D6)("Index4"))) & "[/0], na agência [0]" & C3(D5)("Index6")(D6)("Index5") & "[/0]"
                            ElseIf C3(D5)("Index6")(D6)("Index3") = 3 Then
                                .InsertAfter Text:=" [0]cheque[/0] " & IIf((C3(D5)("Index6")(D6)("Index4") = "085" Or C3(D5)("Index6")(D6)("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D5)("Index6")(D6)("Index4"))) & "[/0], na agência [0]" & C3(D5)("Index6")(D6)("Index5") & "[/0], na conta [0]" & C3(D5)("Index6")(D6)("Index6") & "[/0]"
                            End If
                        End If
                    Next
                    .InsertAfter Text:="."
                    If IIf(C3(D5)("Index7") > 1, C3(D5)("Index8"), False) = True Then
                        .InsertAfter Text:=" Todos estes saques foram disponibilizados através de"
                        If C3(D5)("Index3") = True Then
                            If C3(D5)("Index4")("Index3") = 1 Then
                                .InsertAfter Text:=" [0]transferência bancária[/0] " & IIf((C3(D5)("Index4")("Index4") = "085" Or C3(D5)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D5)("Index4")("Index4"))) & "[/0], na agência [0]" & C3(D5)("Index4")("Index5") & "[/0], na conta [0]" & C3(D5)("Index4")("Index6") & "[/0]"
                            ElseIf C3(D5)("Index4")("Index3") = 2 Then
                                .InsertAfter Text:=" [0]ordem de pagamento[/0] " & IIf((C3(D5)("Index4")("Index4") = "085" Or C3(D5)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D5)("Index4")("Index4"))) & "[/0], na agência [0]" & C3(D5)("Index4")("Index5") & "[/0]"
                            ElseIf C3(D5)("Index4")("Index3") = 3 Then
                                .InsertAfter Text:=" [0]cheque[/0] " & IIf((C3(D5)("Index4")("Index4") = "085" Or C3(D5)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D5)("Index4")("Index4"))) & "[/0], na agência [0]" & C3(D5)("Index4")("Index5") & "[/0], na conta [0]" & C3(D5)("Index4")("Index6") & "[/0]"
                            End If
                        Else
                            If C3(D5)("Index6")(1)("Index3") = 1 Then
                                .InsertAfter Text:=" [0]transferência bancária[/0] " & IIf((C3(D5)("Index6")(1)("Index4") = "085" Or C3(D5)("Index6")(1)("Index4") = "xz"), "na", "no") & " [0]" & Function3(CStr(C3(D5)("Index6")(1)("Index4"))) & "[/0], na agência [0]" & C3(D5)("Index6")(1)("Index5") & "[/0], na conta [0]" & C3(D5)("Index6")(1)("Index6") & "[/0]"
                            ElseIf C3(D5)("Index6")(1)("Index3") = 2 Then
                                .InsertAfter Text:=" [0]ordem de pagamento[/0] " & IIf((C3(D5)("Index6")(1)("Index4") = "085" Or C3(D5)("Index6")(1)("Index4") = "xz"), "na", "no") & " [0]" & Function3(CStr(C3(D5)("Index6")(1)("Index4"))) & "[/0], na agência [0]" & C3(D5)("Index6")(1)("Index5") & "[/0]"
                            ElseIf C3(D5)("Index6")(1)("Index3") = 3 Then
                                .InsertAfter Text:=" [0]cheque[/0] " & IIf((C3(D5)("Index6")(1)("Index4") = "085" Or C3(D5)("Index6")(1)("Index4") = "xz"), "na", "no") & " [0]" & Function3(CStr(C3(D5)("Index6")(1)("Index4"))) & "[/0], na agência [0]" & C3(D5)("Index6")(1)("Index5") & "[/0], na conta [0]" & C3(D5)("Index6")(1)("Index6") & "[/0]"
                            End If
                        End If
                        .InsertAfter Text:="."
                    End If
                End If
                If C3(D5)("Index9") > 0 Then
                    If Format(C3(D5)("Index10")(1)("Index2"), "yyyy") = Format(C3(D5)("Index10")(C3(D5)("Index9"))("Index2"), "yyyy") Then
                        .InsertAfter Text:=" " & IIf(C3(D5)("Index5") > 0, "Também identificamos que, em", "Em") & " [0]" & Format(C3(D5)("Index10")(1)("Index2"), "yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" " & IIf(C3(D5)("Index5") > 0, "Também identificamos que, entre", "Entre") & " [0]" & Format(C3(D5)("Index10")(1)("Index2"), "yyyy") & "[/0] e [0]" & Format(C3(D5)("Index10")(C3(D5)("Index9"))("Index2"), "yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", " & IIf(C3(D5)("Index9") = 1, "foi efetuado", "foram efetuados") & " [0]" & IIf(C3(D5)("Index9") = 1, "um saque eletrônico", "diversos saques eletrônicos") & "[/0] " & IIf(C3(D5)("Index9") = 1, "no valor", "nos valores") & " de"
                    For Each D7 In C3(D5)("Index10").Keys
                        If D7 > 1 Then
                            If D7 = C3(D5)("Index9") Then
                                .InsertAfter Text:=" e"
                            Else
                                .InsertAfter Text:=","
                            End If
                        End If
                        .InsertAfter Text:=" " & Function4(CDbl(C3(D5)("Index10")(D7)("Index1")))
                    Next
                    .InsertAfter Text:="."
                End If
                If C3(D5)("Index11") > 0 Then
                    .InsertAfter Text:=" Ressaltamos que " & IIf(C3(D5)("Index11") = 1, "o saque foi disponibilizado", "os saques foram disponibilizados") & " por recurso do saldo do cartão."
                Else
                    .InsertAfter Text:=" Não identificamos saques."
                End If
                If C3(D5)("Index13") = True Then
                    If C3(D5)("Index14") = C3(D5)("Index16") Then
                        .InsertAfter Text:=" O único desconto em folha ocorreu em [0]" & Format(C3(D5)("Index14"), "yyyy") & "[/0], no valor de " & Function4(C3(D5)("Index15")) & ". Ressaltamos que o desconto em folha se refere ao valor mínimo de sua fatura."
                    Else
                        .InsertAfter Text:=" O primeiro desconto em folha ocorreu em [0]" & Format(C3(D5)("Index14"), "yyyy") & "[/0], no valor de " & Function4(C3(D5)("Index15")) & " e o último desconto em folha ocorreu em [0]" & Format(C3(D5)("Index16"), "yyyy") & "[/0], no valor de " & Function4(C3(D5)("Index17")) & ". Ressaltamos que os descontos em folha se referem ao valor mínimo de suas faturas."
                    End If
                Else
                    .InsertAfter Text:=" Não identificamos descontos em folha."
                End If
                If C3(D5)("Index21") > 0 Then
                    If Format(C3(D5)("Index22")(1)("Index2"), "yyyy") = Format(C3(D5)("Index22")(C3(D5)("Index21"))("Index2"), "yyyy") Then
                        .InsertAfter Text:=" Em [0]" & Format(C3(D5)("Index22")(1)("Index2"), "yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" Entre [0]" & Format(C3(D5)("Index22")(1)("Index2"), "yyyy") & "[/0] e [0]" & Format(C3(D5)("Index22")(C3(D5)("Index21"))("Index2"), "yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", " & IIf(ThisDocument.Variables("B5") = 1, "o", IIf(ThisDocument.Variables("B5") = 2, "a", "")) & " cliente entrou em contato na central, e através " & IIf(C3(D5)("Index21") = 1, "do atendimento prestado", "dos atendimentos prestados") & ", foi possível identificar que " & IIf(ThisDocument.Variables("B5") = 1, "o mesmo", IIf(ThisDocument.Variables("B5") = 2, "a mesma", "")) & " possui ciência sobre a contratação do cartão."
                End If
                If C3(D5)("Index23") > 0 Then
                    If Format(C3(D5)("Index24")(1)("Index2"), "yyyy") = Format(C3(D5)("Index24")(C3(D5)("Index23"))("Index2"), "yyyy") Then
                        .InsertAfter Text:=" " & IIf(C3(D5)("Index21") > 0, "Também identificamos que, em", "Em") & " [0]" & Format(C3(D5)("Index24")(1)("Index2"), "yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" " & IIf(C3(D5)("Index21") > 0, "Também identificamos que, entre", "Entre") & " [0]" & Format(C3(D5)("Index24")(1)("Index2"), "yyyy") & "[/0] e [0]" & Format(C3(D5)("Index24")(C3(D5)("Index23"))("Index2"), "yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", " & IIf(ThisDocument.Variables("B5") = 1, "o", IIf(ThisDocument.Variables("B5") = 2, "a", "")) & " cliente navegou na URA da Central de Cartões."
                End If
                If C3(D5)("Index21") = 0 And C3(D5)("Index23") = 0 Then
                    .InsertAfter Text:=" Não identificamos atendimentos referentes ao cartão."
                End If
                If C3(D5)("Index25") = True Then
                    .InsertAfter Text:=" Identificamos que há histórico de pré-venda em [0]" & Format(C3(D5)("Index27"), "yyyy") & "[/0], onde " & IIf(ThisDocument.Variables("B5") = 1, "o", IIf(ThisDocument.Variables("B5") = 2, "a", "")) & " cliente confirmou a adesão ao produto."
                Else
                    .InsertAfter Text:=" Não identificamos histórico de pré-venda referente ao cartão."
                End If
                If C3(D5)("Index33") = True Then
                    If Format(C3(D5)("Index34"), "yyyy") = Format(C3(D5)("Index35"), "yyyy") Then
                        .InsertAfter Text:=" Em [0]" & Format(C3(D5)("Index34"), "yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" Entre [0]" & Format(C3(D5)("Index34"), "yyyy") & "[/0] e [0]" & Format(C3(D5)("Index35"), "yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", houve confirmações de leituras de faturas."
                End If
                If C3(D5)("Index37") = True Then
                    .InsertAfter Text:=" Identificamos que houve utilização do cartão para realização de compras"
                    If Format(C3(D5)("Index38"), "yyyy") = Format(C3(D5)("Index39"), "yyyy") Then
                        .InsertAfter Text:=" em [0]" & Format(C3(D5)("Index38"), "yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" entre [0]" & Format(C3(D5)("Index38"), "yyyy") & "[/0] e [0]" & Format(C3(D5)("Index39"), "yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:="."
                End If
                If C3(D5)("Index41") = True Then
                    If C3(D5)("Index37") = True Then
                        .InsertAfter Text:=" Também identificamos"
                    Else
                        .InsertAfter Text:=" Identificamos"
                    End If
                    .InsertAfter Text:=" que houve pagamentos de faturas"
                    If Format(C3(D5)("Index42"), "yyyy") = Format(C3(D5)("Index43"), "yyyy") Then
                        .InsertAfter Text:=" em [0]" & Format(C3(D5)("Index42"), "yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" entre [0]" & Format(C3(D5)("Index42"), "yyyy") & "[/0] e [0]" & Format(C3(D5)("Index43"), "yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:="."
                End If
                If C3(D5)("Index37") = False And C3(D5)("Index41") = False Then
                    .InsertAfter Text:=" Não identificamos compras realizadas através do cartão ou pagamentos de faturas."
                End If
                If C3(D5)("Index45") > 0 Then
                    .InsertAfter Text:=" O aplicativo do BMG Card foi acessado [0]" & Function6(1, CLng(C3(D5)("Index45"))) & " " & IIf(C3(D5)("Index45") = 1, "vez", "vezes") & "[/0]"
                    If Format(C3(D5)("Index46"), "yyyy") = Format(C3(D5)("Index47"), "yyyy") Then
                        .InsertAfter Text:=" em [0]" & Format(C3(D5)("Index46"), "yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" entre [0]" & Format(C3(D5)("Index46"), "yyyy") & "[/0] e [0]" & Format(C3(D5)("Index47"), "yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:="."
                End If
                If C3(D5)("Index49") > 0 Then
                    .InsertAfter Text:=" Identificamos que,"
                    If Format(C3(D5)("Index50"), "yyyy") = Format(C3(D5)("Index51"), "yyyy") Then
                        .InsertAfter Text:=" em [0]" & Format(C3(D5)("Index50"), "yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" entre [0]" & Format(C3(D5)("Index50"), "yyyy") & "[/0] e [0]" & Format(C3(D5)("Index51"), "yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", " & IIf(C3(D5)("Index49") = 1, "foi enviado", "foram enviados") & " [0]" & Function6(0, CLng(C3(D5)("Index49"))) & " SMS[/0] para " & IIf(ThisDocument.Variables("B5") = 1, "o", IIf(ThisDocument.Variables("B5") = 2, "a", "")) & " cliente."
                End If
                If C3(D5)("Index53") > 0 Then
                    .InsertAfter Text:=" Identificamos que,"
                    If Format(C3(D5)("Index54"), "yyyy") = Format(C3(D5)("Index55"), "yyyy") Then
                        .InsertAfter Text:=" em [0]" & Format(C3(D5)("Index54"), "yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" entre [0]" & Format(C3(D5)("Index54"), "yyyy") & "[/0] e [0]" & Format(C3(D5)("Index55"), "yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", " & IIf(C3(D5)("Index53") = 1, "foi enviado", "foram enviados") & " [0]" & Function6(1, CLng(C3(D5)("Index53"))) & " " & IIf(C3(D5)("Index53") = 1, "cópia de contrato", "cópias de contratos") & "[/0] para " & IIf(ThisDocument.Variables("B5") = 1, "o", IIf(ThisDocument.Variables("B5") = 2, "a", "")) & " cliente."
                End If
                
                    If C3(D5)("Index79") > 0 Then
                    .InsertAfter Text:=" Identificamos que foi disponibilizado o código de rastreio."
                    End If
                
                    If C3(D5)("Index89") > 0 Then
                    .InsertAfter Text:=" Identificamos que foi realizado atendimento referente a operação duvidosa."
                    End If

                    If C3(D5)("Index99") > 0 Then
                    .InsertAfter Text:=" Identificamos que foi realizado atendimento em segunda instância."
                    End If
                    
                    If C3(D5)("Index19") = True Then
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .InsertAfter Text:="Conforme verificado, identificamos que o cartão e a sua respectiva conta se encontram, ambos, com o status [0]Normal[/0], estando este cartão apto para uso."
                    If C3(D5)("Index20").Count > 0 Then
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="%8%"
                        Call Sub2(2, "%8%", C3(D5)("Index20")(1))
                        .InsertAfter Text:="[2]Tela demonstrando o status do cartão e da sua respectiva conta.[/2]"
                    End If
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
    End With
End Sub

Sub Sub22(ByVal A9 As Integer, ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    With ThisDocument
        Call Sub2(1, "%8%", A9 & " - HISTÓRICO DE ATENDIMENTOS PRESTADOS " & IIf(.Variables("B5") = 1, "AO CONSUMIDOR", IIf(.Variables("B5") = 2, "À CONSUMIDORA", "")))
        With .Content
            For Each D8 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D8) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                If C3(D8)("Index21") > 0 Then
                    If Format(C3(D8)("Index22")(1)("Index2"), "dd/mm/yyyy") = Format(C3(D8)("Index22")(C3(D8)("Index21"))("Index2"), "dd/mm/yyyy") Then
                        .InsertAfter Text:="Em [0]" & Format(C3(D8)("Index22")(1)("Index2"), "dd/mm/yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:="Entre [0]" & Format(C3(D8)("Index22")(1)("Index2"), "dd/mm/yyyy") & "[/0] e [0]" & Format(C3(D8)("Index22")(C3(D8)("Index21"))("Index2"), "dd/mm/yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", " & IIf(ThisDocument.Variables("B5") = 1, "o", IIf(ThisDocument.Variables("B5") = 2, "a", "")) & " cliente entrou em contato na central, e através " & IIf(C3(D8)("Index21") = 1, "do atendimento prestado", "dos atendimentos prestados") & ", foi possível identificar que " & IIf(ThisDocument.Variables("B5") = 1, "o mesmo", IIf(ThisDocument.Variables("B5") = 2, "a mesma", "")) & " possui ciência sobre a contratação do cartão."
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    For Each D9 In C3(D8)("Index22").Keys
                        If D9 > 1 Then
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                        End If
                        .InsertAfter Text:="[0]Protocolo " & IIf(IsNull(C3(D8)("Index22")(D9)("Index1")), "", C3(D8)("Index22")(D9)("Index1")) & "[/0]"
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Data:[/0] " & Format(C3(D8)("Index22")(D9)("Index2"), "dd/mm/yyyy")
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Hora:[/0] " & Format(C3(D8)("Index22")(D9)("Index2"), "hh:mm")
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Motivo:[/0] " & IIf(IsNull(C3(D8)("Index22")(D9)("Index3")), "", C3(D8)("Index22")(D9)("Index3"))
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Bina:[/0] " & IIf(IsNull(C3(D8)("Index22")(D9)("Index4")), "", C3(D8)("Index22")(D9)("Index4"))
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Atendente:[/0] " & IIf(IsNull(C3(D8)("Index22")(D9)("Index5")), "", C3(D8)("Index22")(D9)("Index5"))
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Central:[/0] " & IIf(C3(D8)("Index22")(D9)("Index6") = 1, "SAC", IIf(C3(D8)("Index22")(D9)("Index6") = 2, "RELACIONAMENTO CARTÃO", ""))
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[1]DESCRIÇÃO DA SOLICITAÇÃO " & IIf(ThisDocument.Variables("B5") = 1, "DO", IIf(ThisDocument.Variables("B5") = 2, "DA", "")) & " CLIENTE:[/1]"
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        If Not IsNull(C3(D8)("Index22")(D9)("Index7")) Then
                            .InsertAfter Text:=C3(D8)("Index22")(D9)("Index7")
                        End If
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[1]INFORMAÇÃO PASSADA PELO ATENDENTE:[/1]"
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        If Not IsNull(C3(D8)("Index22")(D9)("Index8")) Then
                            .InsertAfter Text:=C3(D8)("Index22")(D9)("Index8")
                        End If
                    Next
                Else
                    .InsertAfter Text:="Não identificamos atendimentos humanos referentes ao cartão."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
        Call Sub2(1, "%8%", A9 & ".1 - HISTÓRICO DE ATENDIMENTOS ELETRÔNICOS (URA)")
        With .Content
            For Each D10 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D10) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                If C3(D10)("Index23") > 0 Then
                    If Format(C3(D10)("Index24")(1)("Index2"), "dd/mm/yyyy") = Format(C3(D10)("Index24")(C3(D10)("Index23"))("Index2"), "dd/mm/yyyy") Then
                        .InsertAfter Text:="Em [0]" & Format(C3(D10)("Index24")(1)("Index2"), "dd/mm/yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:="Entre [0]" & Format(C3(D10)("Index24")(1)("Index2"), "dd/mm/yyyy") & "[/0] e [0]" & Format(C3(D10)("Index24")(C3(D10)("Index23"))("Index2"), "dd/mm/yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", " & IIf(ThisDocument.Variables("B5") = 1, "o", IIf(ThisDocument.Variables("B5") = 2, "a", "")) & " cliente navegou na URA da Central de Cartões."
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    For Each D11 In C3(D10)("Index24").Keys
                        If D11 > 1 Then
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                        End If
                        .InsertAfter Text:="[0]Protocolo " & IIf(IsNull(C3(D10)("Index24")(D11)("Index1")), "", C3(D10)("Index24")(D11)("Index1")) & "[/0]"
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Data:[/0] " & Format(C3(D10)("Index24")(D11)("Index2"), "dd/mm/yyyy")
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Hora:[/0] " & Format(C3(D10)("Index24")(D11)("Index2"), "hh:mm")
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Motivo:[/0] " & IIf(IsNull(C3(D10)("Index24")(D11)("Index3")), "", C3(D10)("Index24")(D11)("Index3"))
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Bina:[/0] " & IIf(IsNull(C3(D10)("Index24")(D11)("Index4")), "", C3(D10)("Index24")(D11)("Index4"))
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Atendente:[/0] URA"
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="[0]Central:[/0] RELACIONAMENTO CARTÃO"
                        If C3(D10)("Index24")(D11)("Index5").Count > 0 Then
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", CStr(C3(D10)("Index24")(D11)("Index5")(1)))
                            .InsertAfter Text:="[2]Tela demonstrando o atendimento eletrônico.[/2]"
                        End If
                    Next
                Else
                    .InsertAfter Text:="Não identificamos atendimentos eletrônicos referentes ao cartão."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
        Call Sub2(1, "%8%", A9 & ".2 - HISTÓRICO DE PRÉ-VENDA")
        With .Content
            For Each D12 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D12) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                If C3(D12)("Index25") = True Then
                    .InsertAfter Text:="Identificamos que há histórico de pré-venda, onde " & IIf(ThisDocument.Variables("B5") = 1, "o", IIf(ThisDocument.Variables("B5") = 2, "a", "")) & " cliente confirmou a adesão ao produto."
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .InsertAfter Text:="[0]Número da Adesão:[/0] " & C3(D12)("Index26")
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .InsertAfter Text:="[0]Data do Contato:[/0] " & Format(C3(D12)("Index27"), "dd/mm/yyyy")
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .InsertAfter Text:="[0]Hora do Contato:[/0] " & Format(C3(D12)("Index27"), "hh:mm")
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .InsertAfter Text:="[1]DESCRIÇÃO DO CONTATO:[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    If Not IsNull(C3(D12)("Index28")) Then
                        .InsertAfter Text:=C3(D12)("Index28")
                    End If
                Else
                    .InsertAfter Text:="Não identificamos histórico de pré-venda referente ao cartão."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
    End With
End Sub

Sub Sub23(ByVal A9 As Integer, ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    With ThisDocument
        Call Sub2(1, "%8%", A9 & " - TELAS DE CARTÃO, SAQUES E DESCONTOS EM FOLHA")
        With .Content
            For Each D13 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D13) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                .InsertAfter Text:="Identificamos a contratação do cartão em [0]" & Format(C3(D13)("Index1"), "dd/mm/yyyy") & "[/0]."
                If C3(D13)("Index3") = True Then
                    .InsertAfter Text:=" No ato desta contratação, foi solicitado um [0]saque autorizado[/0] no valor de " & Function4(C3(D13)("Index4")("Index1")) & ", o qual foi disponibilizado"
                    If C3(D13)("Index4")("Index2") = C3(D13)("Index1") Then
                        .InsertAfter Text:=" neste mesmo dia"
                    Else
                        .InsertAfter Text:=" em [0]" & Format(C3(D13)("Index4")("Index2"), "dd/mm/yyyy") & "[/0]"
                    End If
                    If IIf(C3(D13)("Index7") > 1, C3(D13)("Index8"), False) = True Then
                        .InsertAfter Text:="."
                    Else
                        .InsertAfter Text:=" através de"
                        If C3(D13)("Index4")("Index3") = 1 Then
                            .InsertAfter Text:=" [0]transferência bancária[/0] " & IIf((C3(D13)("Index4")("Index4") = "085" Or C3(D13)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(C3(D13)("Index4")("Index4")) & "[/0], na agência [0]" & C3(D13)("Index4")("Index5") & "[/0], na conta [0]" & C3(D13)("Index4")("Index6") & "[/0]."
                        ElseIf C3(D13)("Index4")("Index3") = 2 Then
                            .InsertAfter Text:=" [0]ordem de pagamento[/0] " & IIf((C3(D13)("Index4")("Index4") = "085" Or C3(D13)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(C3(D13)("Index4")("Index4")) & "[/0], na agência [0]" & C3(D13)("Index4")("Index5") & "[/0]."
                        ElseIf C3(D13)("Index4")("Index3") = 3 Then
                            .InsertAfter Text:=" [0]cheque[/0] " & IIf((C3(D13)("Index4")("Index4") = "085" Or C3(D13)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(C3(D13)("Index4")("Index4")) & "[/0], na agência [0]" & C3(D13)("Index4")("Index5") & "[/0], na conta [0]" & C3(D13)("Index4")("Index6") & "[/0]."
                        End If
                    End If
                    If C3(D13)("Index4")("Index7") = True Then
                        .InsertAfter Text:=" Este saque foi parcelado em [0]" & C3(D13)("Index4")("Index8") & " parcelas[/0] de " & Function4(C3(D13)("Index4")("Index9")) & "."
                    End If
                End If
                If C3(D13)("Index5") > 0 Then
                    For Each D14 In C3(D13)("Index6").Keys
                        .InsertAfter Text:=" Em [0]" & Format(C3(D13)("Index6")(D14)("Index2"), "dd/mm/yyyy") & "[/0], foi disponibilizado [0]um saque complementar[/0] no valor de " & Function4(CDbl(C3(D13)("Index6")(D14)("Index1")))
                        If IIf(C3(D13)("Index7") > 1, C3(D13)("Index8"), False) = False Then
                            .InsertAfter Text:=" através de"
                            If C3(D13)("Index6")(D14)("Index3") = 1 Then
                                .InsertAfter Text:=" [0]transferência bancária[/0] " & IIf((C3(D13)("Index6")(D14)("Index4") = "085" Or C3(D13)("Index6")(D14)("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D13)("Index6")(D14)("Index4"))) & "[/0], na agência [0]" & C3(D13)("Index6")(D14)("Index5") & "[/0], na conta [0]" & C3(D13)("Index6")(D14)("Index6") & "[/0]"
                            ElseIf C3(D13)("Index6")(D14)("Index3") = 2 Then
                                .InsertAfter Text:=" [0]ordem de pagamento[/0] " & IIf((C3(D13)("Index6")(D14)("Index4") = "085" Or C3(D13)("Index6")(D14)("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D13)("Index6")(D14)("Index4"))) & "[/0], na agência [0]" & C3(D13)("Index6")(D14)("Index5") & "[/0]"
                            ElseIf C3(D13)("Index6")(D14)("Index3") = 3 Then
                                .InsertAfter Text:=" [0]cheque[/0] " & IIf((C3(D13)("Index6")(D14)("Index4") = "085" Or C3(D13)("Index6")(D14)("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D13)("Index6")(D14)("Index4"))) & "[/0], na agência [0]" & C3(D13)("Index6")(D14)("Index5") & "[/0], na conta [0]" & C3(D13)("Index6")(D14)("Index6") & "[/0]"
                            End If
                        End If
                        .InsertAfter Text:="."
                    Next
                    If IIf(C3(D13)("Index7") > 1, C3(D13)("Index8"), False) = True Then
                        .InsertAfter Text:=" Todos estes saques foram disponibilizados através de"
                        If C3(D13)("Index3") = True Then
                            If C3(D13)("Index4")("Index3") = 1 Then
                                .InsertAfter Text:=" [0]transferência bancária[/0] " & IIf((C3(D13)("Index4")("Index4") = "085" Or C3(D13)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D13)("Index4")("Index4"))) & "[/0], na agência [0]" & C3(D13)("Index4")("Index5") & "[/0], na conta [0]" & C3(D13)("Index4")("Index6") & "[/0]"
                            ElseIf C3(D13)("Index4")("Index3") = 2 Then
                                .InsertAfter Text:=" [0]ordem de pagamento[/0] " & IIf((C3(D13)("Index4")("Index4") = "085" Or C3(D13)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D13)("Index4")("Index4"))) & "[/0], na agência [0]" & C3(D13)("Index4")("Index5") & "[/0]"
                            ElseIf C3(D13)("Index4")("Index3") = 3 Then
                                .InsertAfter Text:=" [0]cheque[/0] " & IIf((C3(D13)("Index4")("Index4") = "085" Or C3(D13)("Index4")("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D13)("Index4")("Index4"))) & "[/0], na agência [0]" & C3(D13)("Index4")("Index5") & "[/0], na conta [0]" & C3(D13)("Index4")("Index6") & "[/0]"
                            End If
                        Else
                            If C3(D13)("Index6")(1)("Index3") = 1 Then
                                .InsertAfter Text:=" [0]transferência bancária[/0] " & IIf((C3(D13)("Index6")(1)("Index4") = "085" Or C3(D13)("Index6")(1)("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D13)("Index6")(1)("Index4"))) & "[/0], na agência [0]" & C3(D13)("Index6")(1)("Index5") & "[/0], na conta [0]" & C3(D13)("Index6")(1)("Index6") & "[/0]"
                            ElseIf C3(D13)("Index6")(1)("Index3") = 2 Then
                                .InsertAfter Text:=" [0]ordem de pagamento[/0] " & IIf((C3(D13)("Index6")(1)("Index4") = "085" Or C3(D13)("Index6")(1)("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D13)("Index6")(1)("Index4"))) & "[/0], na agência [0]" & C3(D13)("Index6")(1)("Index5") & "[/0]"
                            ElseIf C3(D13)("Index6")(1)("Index3") = 3 Then
                                .InsertAfter Text:=" [0]cheque[/0] " & IIf((C3(D13)("Index6")(1)("Index4") = "085" Or C3(D13)("Index6")(1)("Index4") = "104"), "na", "no") & " [0]" & Function3(CStr(C3(D13)("Index6")(1)("Index4"))) & "[/0], na agência [0]" & C3(D13)("Index6")(1)("Index5") & "[/0], na conta [0]" & C3(D13)("Index6")(1)("Index6") & "[/0]"
                            End If
                        End If
                        .InsertAfter Text:="."
                    End If
                End If
                If C3(D13)("Index9") > 0 Then
                    For Each D15 In C3(D13)("Index10").Keys
                        .InsertAfter Text:=" Em [0]" & Format(C3(D13)("Index10")(D15)("Index2"), "dd/mm/yyyy") & "[/0], foi efetuado [0]um saque eletrônico[/0] no valor de " & Function4(CDbl(C3(D13)("Index10")(D15)("Index1"))) & "."
                    Next
                End If
                If C3(D13)("Index11") > 0 Then
                    .InsertAfter Text:=" Ressaltamos que " & IIf(C3(D13)("Index11") = 1, "o saque foi disponibilizado", "os saques foram disponibilizados") & " por recurso do saldo do cartão."
                Else
                    .InsertAfter Text:=" Não identificamos saques."
                End If
                If C3(D13)("Index2").Count > 0 Then
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .InsertAfter Text:="%8%"
                    Call Sub2(2, "%8%", C3(D13)("Index2")(1))
                    .InsertAfter Text:="[2]Tela demonstrando a contratação do cartão.[/2]"
                End If
                If C3(D13)("Index11") > 0 Then
                    If C3(D13)("Index12").Count > 0 Then
                        For Each D16 In C3(D13)("Index12")
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", C3(D13)("Index12")(D16))
                            .InsertAfter Text:="[2]Tela demonstrando " & IIf(C3(D13)("Index11") = 1, "a contratação do saque", "as contratações dos saques") & ".[/2]"
                        Next
                    End If
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C3(D13)("Index13") = True Then
                    If C3(D13)("Index14") = C3(D13)("Index16") Then
                        .InsertAfter Text:="O único desconto em folha ocorreu em [0]" & Format(C3(D13)("Index14"), "dd/mm/yyyy") & "[/0], no valor de " & Function4(C3(D13)("Index15")) & ". Ressaltamos que o desconto em folha se refere ao valor mínimo de sua fatura."
                    Else
                        .InsertAfter Text:="O primeiro desconto em folha ocorreu em [0]" & Format(C3(D13)("Index14"), "dd/mm/yyyy") & "[/0], no valor de " & Function4(C3(D13)("Index15")) & " e o último desconto em folha ocorreu em [0]" & Format(C3(D13)("Index16"), "dd/mm/yyyy") & "[/0], no valor de " & Function4(C3(D13)("Index17")) & ". Ressaltamos que os descontos em folha se referem ao valor mínimo de suas faturas."
                    End If
                Else
                    .InsertAfter Text:="Não identificamos descontos em folha."
                End If
                If C3(D13)("Index18").Count > 0 Then
                    For Each D17 In C3(D13)("Index18")
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="%8%"
                        Call Sub2(2, "%8%", C3(D13)("Index18")(D17))
                        .InsertAfter Text:="[2]Tela demonstrando " & IIf(C3(D13)("Index13") = True, IIf(C3(D13)("Index14") = C3(D13)("Index16"), "o desconto em folha", "os descontos em folha"), "a ausência de descontos em folha") & ".[/2]"
                    Next
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
    End With
End Sub

Sub Sub24(ByVal A9 As Integer, ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    With ThisDocument
        Call Sub2(1, "%8%", A9 & " - TELAS DE FORMA DE ENVIO DE FATURAS E DE CONFIRMAÇÕES DE LEITURAS DE FATURAS")
        With .Content
            For Each D18 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D18) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                If C3(D18)("Index29") = True Then
                    .InsertAfter Text:="Identificamos que a forma de envio de faturas está definida como " & IIf(C3(D18)("Index30") = 1, "[0]Online[/0] (através do site)", IIf(C3(D18)("Index30") = 2, "[0]Correio[/0]", IIf(C3(D18)("Index30") = 3, "[0]Email[/0]", IIf(C3(D18)("Index30") = 4, "[0]Correio + Email[/0]", IIf(C3(D18)("Index30") = 5, "[0]SMS[/0]", ""))))) & "."
                    If C3(D18)("Index31").Count > 0 Then
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .Collapse Direction:=wdCollapseEnd
                        .InsertBreak Type:=wdLineBreak
                        .InsertAfter Text:="%8%"
                        Call Sub2(2, "%8%", C3(D18)("Index31")(1))
                        .InsertAfter Text:="[2]Tela demonstrando a forma de envio de faturas.[/2]"
                    End If
                    If C3(D18)("Index30") = 2 Or C3(D18)("Index30") = 4 Then
                        If C3(D18)("Index32").Count > 0 Then
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", C3(D18)("Index32")(1))
                            .InsertAfter Text:="[2]Tela demonstrando o endereço.[/2]"
                        End If
                    End If
                Else
                    .InsertAfter Text:="Não foi possível identificar a forma de envio de faturas, pois o cartão não foi encontrado no IntergrALL."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C3(D18)("Index33") = True Then
                    If C3(D18)("Index34") = C3(D18)("Index35") Then
                        .InsertAfter Text:="Em [0]" & Format(C3(D18)("Index34"), "dd/mm/yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:="Entre [0]" & Format(C3(D18)("Index34"), "dd/mm/yyyy") & "[/0] e [0]" & Format(C3(D18)("Index35"), "dd/mm/yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", houve confirmações de leituras de faturas."
                    If C3(D18)("Index36").Count > 0 Then
                        For Each D19 In C3(D18)("Index36")
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", C3(D18)("Index36")(D19))
                            .InsertAfter Text:="[2]Tela demonstrando as confirmações de leituras de faturas.[/2]"
                        Next
                    End If
                Else
                    .InsertAfter Text:="Não identificamos confirmações de leituras de faturas."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
    End With
End Sub

Sub Sub25(ByVal A9 As Integer, ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    With ThisDocument
        Call Sub2(1, "%8%", A9 & " - TELAS DE FATURAS")
        With .Content
            For Each D20 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D20) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                If C3(D20)("Index37") = True Then
                    .InsertAfter Text:="Identificamos que houve utilização do cartão para realização de compras"
                    If C3(D20)("Index38") = C3(D20)("Index39") Then
                        .InsertAfter Text:=" em [0]" & Format(C3(D20)("Index38"), "dd/mm/yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" entre [0]" & Format(C3(D20)("Index38"), "dd/mm/yyyy") & "[/0] e [0]" & Format(C3(D20)("Index39"), "dd/mm/yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:="."
                End If
                If C3(D20)("Index41") = True Then
                    If C3(D20)("Index37") = True Then
                        .InsertAfter Text:=" Também identificamos"
                    Else
                        .InsertAfter Text:="Identificamos"
                    End If
                    .InsertAfter Text:=" que houve pagamentos de faturas"
                    If C3(D20)("Index42") = C3(D20)("Index43") Then
                        .InsertAfter Text:=" em [0]" & Format(C3(D20)("Index42"), "dd/mm/yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" entre [0]" & Format(C3(D20)("Index42"), "dd/mm/yyyy") & "[/0] e [0]" & Format(C3(D20)("Index43"), "dd/mm/yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:="."
                End If
                If C3(D20)("Index37") = True Then
                    If C3(D20)("Index40").Count > 0 Then
                        For Each D21 In C3(D20)("Index40")
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", C3(D20)("Index40")(D21))
                            .InsertAfter Text:="[2]Tela demonstrando as compras efetuadas através do cartão.[/2]"
                        Next
                    End If
                End If
                If C3(D20)("Index41") = True Then
                    If C3(D20)("Index44").Count > 0 Then
                        For Each D22 In C3(D20)("Index44")
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", C3(D20)("Index44")(D22))
                            .InsertAfter Text:="[2]Tela demonstrando os pagamentos de faturas.[/2]"
                        Next
                    End If
                End If
                If C3(D20)("Index37") = False And C3(D20)("Index41") = False Then
                    .InsertAfter Text:="Não identificamos compras realizadas através do cartão ou pagamentos de faturas."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
    End With
End Sub

Sub Sub26(ByVal A9 As Integer, ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    With ThisDocument
        Call Sub2(1, "%8%", A9 & " - TELAS DE ACESSOS AO APLICATIVO")
        With .Content
            For Each D23 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D23) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                If C3(D23)("Index45") > 0 Then
                    .InsertAfter Text:="O aplicativo do BMG Card foi acessado [0]" & Function6(1, CLng(C3(D23)("Index45"))) & " " & IIf(C3(D23)("Index45") = 1, "vez", "vezes") & "[/0]"
                    If C3(D23)("Index46") = C3(D23)("Index47") Then
                        .InsertAfter Text:=" em [0]" & Format(C3(D23)("Index46"), "dd/mm/yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" entre [0]" & Format(C3(D23)("Index46"), "dd/mm/yyyy") & "[/0] e [0]" & Format(C3(D23)("Index47"), "dd/mm/yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:="."
                    If C3(D23)("Index48").Count > 0 Then
                        For Each D24 In C3(D23)("Index48")
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", C3(D23)("Index48")(D24))
                            .InsertAfter Text:="[2]Tela demonstrando " & IIf(C3(D23)("Index45") = 1, "o acesso ao aplicativo", "os acessos ao aplicativo") & ".[/2]"
                        Next
                    End If
                Else
                    .InsertAfter Text:="Não identificamos acessos ao aplicativo."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
    End With
End Sub

Sub Sub27(ByVal A9 As Integer, ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    With ThisDocument
        Call Sub2(1, "%8%", A9 & " - TELAS DE ENVIOS DE SMS")
        With .Content
            For Each D25 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D25) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                If C3(D25)("Index49") > 0 Then
                    .InsertAfter Text:="Identificamos que,"
                    If C3(D25)("Index50") = C3(D25)("Index51") Then
                        .InsertAfter Text:=" em [0]" & Format(C3(D25)("Index50"), "dd/mm/yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" entre [0]" & Format(C3(D25)("Index50"), "dd/mm/yyyy") & "[/0] e [0]" & Format(C3(D25)("Index51"), "dd/mm/yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", " & IIf(C3(D25)("Index49") = 1, "foi enviado", "foram enviados") & " [0]" & Function6(0, CLng(C3(D25)("Index49"))) & " SMS[/0] para " & IIf(ThisDocument.Variables("B5") = 1, "o", IIf(ThisDocument.Variables("B5") = 2, "a", "")) & " cliente."
                    If C3(D25)("Index52").Count > 0 Then
                        For Each D26 In C3(D25)("Index52")
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", C3(D25)("Index52")(D26))
                            .InsertAfter Text:="[2]Tela demonstrando " & IIf(C3(D25)("Index49") = 1, "o envio de SMS", "os envios de SMS") & ".[/2]"
                        Next
                    End If
                Else
                    .InsertAfter Text:="Não identificamos envios de SMS."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
    End With
End Sub

Sub Sub28(ByVal A9 As Integer, ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    With ThisDocument
        Call Sub2(1, "%8%", A9 & " - TELAS DE ENVIOS DE CÓPIAS DE CONTRATOS")
        With .Content
            For Each D1 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D1) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                If C3(D1)("Index53") > 0 Then
                    .InsertAfter Text:="Identificamos que,"
                    If C3(D1)("Index54") = C3(D1)("Index55") Then
                        .InsertAfter Text:=" em [0]" & Format(C3(D1)("Index54"), "dd/mm/yyyy") & "[/0]"
                    Else
                        .InsertAfter Text:=" entre [0]" & Format(C3(D1)("Index54"), "dd/mm/yyyy") & "[/0] e [0]" & Format(C3(D1)("Index55"), "dd/mm/yyyy") & "[/0]"
                    End If
                    .InsertAfter Text:=", " & IIf(C3(D1)("Index53") = 1, "foi enviado", "foram enviados") & " [0]" & Function6(1, CLng(C3(D1)("Index53"))) & " " & IIf(C3(D1)("Index53") = 1, "cópia de contrato", "cópias de contratos") & "[/0] para " & IIf(ThisDocument.Variables("B5") = 1, "o", IIf(ThisDocument.Variables("B5") = 2, "a", "")) & " cliente."
                    If C3(D1)("Index56").Count > 0 Then
                        For Each D2 In C3(D1)("Index56")
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", C3(D1)("Index56")(D2))
                            .InsertAfter Text:="[2]Tela demonstrando " & IIf(C3(D1)("Index53") = 1, "o envio de cópia de contrato", "os envios de cópias de contratos") & ".[/2]"
                        Next
                    End If
                Else
                    .InsertAfter Text:="Não identificamos envios de cópias de contratos."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
    End With
End Sub

Sub Sub29(ByVal A9 As Integer, ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    With ThisDocument
        Call Sub2(1, "%8%", A9 & " - CÓDIGO DE RASTREIO")
        With .Content
            For Each D25 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D25) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                If C3(D25)("Index79") > 0 Then
                    .InsertAfter Text:="Identificamos que houve disponibilização do código de rastreio."
                    If C3(D25)("Index72").Count > 0 Then
                        For Each D26 In C3(D25)("Index72")
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", C3(D25)("Index72")(D26))
                            .InsertAfter Text:="[2]Tela demonstrando " & IIf(C3(D25)("Index79") = 1, "o código de rastreio", "os códigos de rastreio") & ".[/2]"
                        Next
                    End If
                Else
                    .InsertAfter Text:="Não identificamos código de rastreio."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
    End With
End Sub

Sub Sub30(ByVal A9 As Integer, ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    With ThisDocument
        Call Sub2(1, "%8%", A9 & " - ATENDIMENTO DE OPERAÇÃO DUVIDOSA")
        With .Content
            For Each D25 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D25) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                If C3(D25)("Index89") > 0 Then
                    .InsertAfter Text:="Identificamos que houve atendimento referente a operação duvidosa."
                    If C3(D25)("Index82").Count > 0 Then
                        For Each D26 In C3(D25)("Index82")
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", C3(D25)("Index82")(D26))
                            .InsertAfter Text:="[2]Tela demonstrando " & IIf(C3(D25)("Index89") = 1, "o atendimento de operação duvidosa", "os atendimentos de operação duvidosa") & ".[/2]"
                        Next
                    End If
                Else
                    .InsertAfter Text:="Não identificamos atendimento referente a operação duvidosa."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
    End With
End Sub

Sub Sub31(ByVal A9 As Integer, ByVal C1 As Object, ByVal C2 As Object, ByRef C3() As Object)
    With ThisDocument
        Call Sub2(1, "%8%", A9 & " - ATENDIMENTO DE SEGUNDA INSTÂNCIA")
        With .Content
            For Each D25 In C1.Keys
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
                If C1.Count > 1 Then
                    .InsertAfter Text:="[1]CARTÃO " & C1(D25) & "[/1]"
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                    .Collapse Direction:=wdCollapseEnd
                    .InsertBreak Type:=wdLineBreak
                End If
                If C3(D25)("Index99") > 0 Then
                    .InsertAfter Text:="Identificamos que houve atendimento em segunda instância."
                    If C3(D25)("Index92").Count > 0 Then
                        For Each D26 In C3(D25)("Index92")
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .Collapse Direction:=wdCollapseEnd
                            .InsertBreak Type:=wdLineBreak
                            .InsertAfter Text:="%8%"
                            Call Sub2(2, "%8%", C3(D25)("Index92")(D26))
                            .InsertAfter Text:="[2]Tela demonstrando " & IIf(C3(D25)("Index99") = 1, "o atendimento em segunda instância", "os atendimentos em segunda instância") & ".[/2]"
                        Next
                    End If
                Else
                    .InsertAfter Text:="Não identificamos atendimento em segunda instância."
                End If
                .Collapse Direction:=wdCollapseEnd
                .InsertBreak Type:=wdLineBreak
            Next
            .InsertAfter Text:="%8%"
        End With
    End With
End Sub

Function Function1(ByVal A1 As String, ByVal A2 As Integer, ByVal A3 As Boolean, Optional ByVal A4 As String = "")
    Dim B1 As String, B2 As String, B3 As String, B4(1 To 2) As String
    Dim C1 As Object
    Dim D1 As Double
    Dim E1 As Integer
    Dim F1 As Boolean
    Set C1 = CreateObject("VBScript.RegExp")
    Do
        If A2 = 17 Then
            B1 = Function10(A1, 2, A4)
        ElseIf A2 = 22 Then
            B1 = Function10(A1, 1, A4)
        Else
            B1 = Function10(A1, 0, A4)
        End If
        
        If StrPtr(B1) = 0 Then
            If MsgBox("Tem certeza que deseja cancelar?", vbYesNo, Const1) = vbYes Then
                End
            End If
        ElseIf B1 = "" And A3 = False Then
            Function1 = Null
            Exit Do
        Else
            B2 = UCase(B1)
            If A2 = 0 Then
                With C1
                    .Pattern = ".*?([ A-ZÁÂÃÇÉÊÍÓÔÕÚÜ]+).*"
                    If .Execute(B2).Count > 0 Then
                        B3 = .Replace(B2, "$1")
                        .Pattern = "^ +"
                        B3 = .Replace(B3, "")
                        .Pattern = " +$"
                        B3 = .Replace(B3, "")
                        .Pattern = " +"
                        .Global = True
                        B3 = .Replace(B3, " ")
                        If B3 <> "" Then
                            Function1 = B3
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 1 Then
                With C1
                    .Pattern = ".*?(\d{3})\.?(\d{3})\.?(\d{3})-?(\d{2}).*"
                    If .Execute(B2).Count > 0 Then
                        Function1 = .Replace(B2, "$1.$2.$3-$4")
                        Exit Do
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 2 Then
                With C1
                    .Pattern = ".*?(\d{4})\.?[0-9X]{4}\.?[0-9X]{4}\.?(\d{4})\.?.*"
                    If .Execute(B2).Count > 0 Then
                        Function1 = .Replace(B2, "$1.XXXX.XXXX.$2")
                        Exit Do
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 3 Then
                With C1
                    .Pattern = ".*?CIV(\d{7}).*"
                    If .Execute(B2).Count > 0 Then
                        Function1 = .Replace(B2, "CIV$1")
                        Exit Do
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 4 Then
                With C1
                    .Pattern = ".*?(\d{2})/(\d{2})/(\d{4}).*"
                    If .Execute(B2).Count > 0 Then
                        B3 = .Replace(B2, "$1/$2/$3")
                        If IsDate(B3) = True Then
                            Function1 = B3
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 5 Then
                With C1
                    .Pattern = ".*?(\d{1,6}),(\d{2}).*"
                    If .Execute(B2).Count > 0 Then
                        D1 = CDbl(.Replace(B2, "$1,$2"))
                        If D1 > 0 And D1 <= 999999.99 Then
                            Function1 = D1
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 6 Then
                With C1
                    .Pattern = ".*?(\d{1}).*"
                    If .Execute(B2).Count > 0 Then
                        E1 = CInt(.Replace(B2, "$1"))
                        If E1 >= 1 And E1 <= 3 Then
                            Function1 = E1
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 7 Then
                With C1
                    .Pattern = ".*?(\d{3}).*"
                    If .Execute(B2).Count > 0 Then
                        Function1 = .Replace(B2, "$1")
                        Exit Do
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 8 Then
                With C1
                    .Pattern = ".*?(\d{1,5}).*"
                    If .Execute(B2).Count > 0 Then
                        Function1 = .Replace(B2, "$1")
                        Exit Do
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 9 Then
                With C1
                    .Pattern = ".*?(\d{1,20}(-(\d{1,2}|[PX])|\d)).*"
                    If .Execute(B2).Count > 0 Then
                        Function1 = .Replace(B2, "$1")
                        Exit Do
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 10 Then
                With C1
                    .Pattern = ".*?(\d{1,4}).*"
                    If .Execute(B2).Count > 0 Then
                        E1 = CInt(.Replace(B2, "$1"))
                        If E1 >= 0 And E1 <= 1000 Then
                            Function1 = E1
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 11 Then
                With C1
                    .Pattern = ".*?(\d{5,30}).*"
                    If .Execute(B2).Count > 0 Then
                        Function1 = .Replace(B2, "$1")
                        Exit Do
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 12 Then
                With C1
                    .Pattern = ".*?(\d{2})/(\d{2})/(\d{4}).*?(\d{2}):(\d{2}).*"
                    If .Execute(B2).Count > 0 Then
                        B3 = .Replace(B2, "$1/$2/$3 $4:$5")
                        If IsDate(B3) = True Then
                            Function1 = B3
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 13 Then
                With C1
                    .Pattern = ".*?([1-9]{2}).*?([1-9]\d{3,4}).*?(\d{4}).*"
                    If .Execute(B2).Count > 0 Then
                        Function1 = .Replace(B2, "($1) $2-$3")
                        Exit Do
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 14 Then
                With C1
                    .Pattern = ".*?(\d{1}).*"
                    If .Execute(B2).Count > 0 Then
                        E1 = CInt(.Replace(B2, "$1"))
                        If E1 >= 1 And E1 <= 2 Then
                            Function1 = E1
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 15 Then
                With C1
                    .Pattern = ".*?(\d{1}).*"
                    If .Execute(B2).Count > 0 Then
                        E1 = CInt(.Replace(B2, "$1"))
                        If E1 >= 1 And E1 <= 5 Then
                            Function1 = E1
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 16 Then
                With C1
                    .Pattern = ".*?(\d{8}).*"
                    If .Execute(B2).Count > 0 Then
                        Function1 = .Replace(B2, "$1")
                        Exit Do
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 17 Then
                B3 = Trim(B1)
                If B3 <> "" Then
                    With C1
                        .Pattern = "\[(.+?)\]"
                        .Global = True
                        Function1 = .Replace(B3, "[0]$1[/0]")
                        Exit Do
                    End With
                Else
                    MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                End If
            ElseIf A2 = 18 Then
                With C1
                    .Pattern = ".*?(\d{1,3}).*"
                    If .Execute(B2).Count > 0 Then
                        E1 = CInt(.Replace(B2, "$1"))
                        If E1 >= 2 And E1 <= 120 Then
                            Function1 = E1
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 19 Then
                With C1
                    .Pattern = ".*?(\d{1}).*"
                    If .Execute(B2).Count > 0 Then
                        E1 = CInt(.Replace(B2, "$1"))
                        If E1 >= 1 And E1 <= 5 Then
                            Function1 = E1
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 20 Then
                With C1
                    .Pattern = ".*?(\d{1,20}(-\d{1,2}|\d)[A-Z]?).*"
                    If .Execute(B2).Count > 0 Then
                        Function1 = .Replace(B2, "$1")
                        Exit Do
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 21 Then
                If B1 <> "" Then
                    Function1 = B1
                    Exit Do
                Else
                    MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                End If
            ElseIf A2 = 22 Then
                If B1 <> "" Then
                    Function1 = B1
                    Exit Do
                Else
                    MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                End If
            ElseIf A2 = 23 Then
                With C1
                    .Pattern = ".*?(\[(\{""Index1"":""\d+"",""Index2"":""\d{2}/\d{2}/\d{4} \d{2}:\d{2}"",""Index3"":""[ A-Z]*"",""Index4"":""[ 0-9A-Z]*""\},?)+\]).*"
                    If .Execute(B1).Count > 0 Then
                        B4(1) = .Replace(B1, "$1")
                        .Pattern = "^\[(.+)\]$"
                        B4(2) = .Replace(B4(1), "$1")
                        .Pattern = "\{""Index1"":""\d+"",""Index2"":""(\d{2}/\d{2}/\d{4} \d{2}:\d{2})"",""Index3"":""[ A-Z]*"",""Index4"":""[ 0-9A-Z]*""\}"
                        .Global = True
                        F1 = True
                        For Each G1 In .Execute(B4(2))
                            If IsDate(.Replace(G1.Value, "$1")) = False Then
                                F1 = False
                                Exit For
                            End If
                        Next
                        If F1 = True Then
                            Function1 = B4(1)
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 24 Then
                With C1
                    .Pattern = ".*?(\d{1}).*"
                    If .Execute(B2).Count > 0 Then
                        E1 = CInt(.Replace(B2, "$1"))
                        If E1 >= 1 And E1 <= 2 Then
                            Function1 = E1
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 25 Then
                With C1
                    .Pattern = ".*?(\d{1}).*"
                    If .Execute(B2).Count > 0 Then
                        E1 = CInt(.Replace(B2, "$1"))
                        If E1 >= 1 And E1 <= 2 Then
                            Function1 = E1
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 26 Then
                With C1
                    .Pattern = ".*?(\d{1}).*"
                    If .Execute(B2).Count > 0 Then
                        E1 = CInt(.Replace(B2, "$1"))
                        If E1 >= 1 And E1 <= 4 Then
                            Function1 = E1
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            ElseIf A2 = 29 Then
                With C1
                    .Pattern = ".*?(\d{1}).*"
                    If .Execute(B2).Count >= 0 Then
                        E1 = CInt(.Replace(B2, "$1"))
                        If E1 >= 0 And E1 <= 1 Then
                            Function1 = E1
                            Exit Do
                        Else
                            MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                        End If
                    Else
                        MsgBox Prompt:="Entre com os dados corretamente.", Title:=Const1
                    End If
                End With
            End If
        End If
    Loop
End Function

Function Function2(ByVal A1 As Integer, Optional ByVal A2 As Integer, Optional ByVal A3 As Integer, Optional ByVal A4 As Integer = 0, Optional ByVal A5 As String = "", Optional ByVal A6 As String = "", Optional ByVal A7 As String = "") As Object
    Dim B1 As Object
    Set B1 = CreateObject("Scripting.Dictionary")
    B1.Add "Index1", Function1("Entre com o valor do saque " & IIf(A1 = 0, "autorizado", IIf(A1 = 1, "complementar (" & A2 & " de " & A3 & ")", IIf(A1 = 2, "eletrônico (" & A2 & " de " & A3 & ")", ""))) & ":", 5, True)
    B1.Add "Index2", DateValue(CStr(Function1("Entre com a data do saque " & IIf(A1 = 0, "autorizado", IIf(A1 = 1, "complementar (" & A2 & " de " & A3 & ")", IIf(A1 = 2, "eletrônico (" & A2 & " de " & A3 & ")", ""))) & ":", 4, True)))
    If A1 = 0 Or A1 = 1 Then
        B1.Add "Index3", Function1("Entre com a forma de crédito do saque " & IIf(A1 = 0, "autorizado", IIf(A1 = 1, "complementar (" & A2 & " de " & A3 & ")", "")) & " (1 = DOC/TED/CC, 2 = OP, 3 = Cheque):", 6, True, IIf(A4 <> 0, A4, ""))
        If B1("Index3") = 1 Then
            B1.Add "Index4", Function1("Entre com o banco do saque " & IIf(A1 = 0, "autorizado", IIf(A1 = 1, "complementar (" & A2 & " de " & A3 & ")", "")) & ":", 7, True, IIf(A4 = 1, A5, ""))
            B1.Add "Index5", Function1("Entre com a agência do saque " & IIf(A1 = 0, "autorizado", IIf(A1 = 1, "complementar (" & A2 & " de " & A3 & ")", "")) & ":", 8, True, IIf(A4 = 1 And A5 = B1("Index4"), A6, ""))
            B1.Add "Index6", Function1("Entre com a conta do saque " & IIf(A1 = 0, "autorizado", IIf(A1 = 1, "complementar (" & A2 & " de " & A3 & ")", "")) & ":", 9, True, IIf(A4 = 1 And A5 = B1("Index4") And A6 = B1("Index5"), A7, ""))
        ElseIf B1("Index3") = 2 Then
            B1.Add "Index4", Function1("Entre com o banco do saque " & IIf(A1 = 0, "autorizado", IIf(A1 = 1, "complementar (" & A2 & " de " & A3 & ")", "")) & ":", 7, True, IIf(A4 = 2, A5, ""))
            B1.Add "Index5", Function1("Entre com a agência do saque " & IIf(A1 = 0, "autorizado", IIf(A1 = 1, "complementar (" & A2 & " de " & A3 & ")", "")) & ":", 8, True, IIf(A4 = 2 And A5 = B1("Index4"), A6, ""))
        ElseIf B1("Index3") = 3 Then
            B1.Add "Index4", Function1("Entre com o banco do saque " & IIf(A1 = 0, "autorizado", IIf(A1 = 1, "complementar (" & A2 & " de " & A3 & ")", "")) & ":", 7, True, IIf(A4 = 3, A5, ""))
            B1.Add "Index5", Function1("Entre com a agência do saque " & IIf(A1 = 0, "autorizado", IIf(A1 = 1, "complementar (" & A2 & " de " & A3 & ")", "")) & ":", 8, True, IIf(A4 = 3 And A5 = B1("Index4"), A6, ""))
            B1.Add "Index6", Function1("Entre com a conta do saque " & IIf(A1 = 0, "autorizado", IIf(A1 = 1, "complementar (" & A2 & " de " & A3 & ")", "")) & ":", 9, True, IIf(A4 = 3 And A5 = B1("Index4") And A6 = B1("Index5"), A7, ""))
        End If
    End If
    If A1 = 0 Then
        B1.Add "Index7", Function9("O saque autorizado foi parcelado?")
        If B1("Index7") = True Then
            B1.Add "Index8", Function1("Entre com a quantidade de parcelas do saque autorizado:", 18, True)
            B1.Add "Index9", Function1("Entre com o valor das parcelas do saque autorizado:", 5, True)
        End If
    End If
    Set Function2 = B1
End Function

Function Function3(ByVal A1 As String)
    Dim B1 As Object
    Set B1 = CreateObject("Scripting.Dictionary")
    B1.Add "001", "Banco do Brasil"
    B1.Add "003", "Banco da Amazônia"
    B1.Add "004", "Banco do Nordeste do Brasil"
    B1.Add "021", "Banco do Estado do Espírito Santo"
    B1.Add "027", "Banco do Estado de Santa Catarina"
    B1.Add "025", "Banco Alfa"
    B1.Add "029", "Banco Itaú Consignado"
    B1.Add "033", "Banco Santander"
    B1.Add "037", "Banco do Estado do Pará"
    B1.Add "041", "Banco do Estado do Rio Grande do Sul"
    B1.Add "047", "Banco do Estado de Sergipe"
    B1.Add "070", "Banco de Brasília"
    B1.Add "077", "Banco Inter"
    B1.Add "085", "Cooperativa Central de Crédito Urbano"
    B1.Add "104", "Caixa Econômica Federal"
    B1.Add "121", "Banco Agibank"
    B1.Add "151", "Banco Nossa Caixa"
    B1.Add "224", "Banco Fibra"
    B1.Add "229", "Banco Cruzeiro do Sul"
    B1.Add "260", "Banco Nu Pagamentos"
    B1.Add "237", "Banco Bradesco"
    B1.Add "318", "Banco BMG"
    B1.Add "336", "Banco C6"
    B1.Add "341", "Banco Itaú Unibanco"
    B1.Add "356", "Banco ABN Amro Real"
    B1.Add "380", "Banco PicPay"
    B1.Add "389", "Banco Mercantil do Brasil"
    B1.Add "399", "HSBC Bank Brasil"
    B1.Add "409", "Banco Unibanco"
    B1.Add "422", "Banco Safra"
    B1.Add "453", "Banco Rural"
    B1.Add "623", "Banco Pan"
    B1.Add "626", "Banco C6 Consignado"
    B1.Add "655", "Banco Votorantim"
    B1.Add "707", "Banco Daycoval"
    B1.Add "748", "Banco Cooperativo Sicredi"
    B1.Add "756", "Banco Cooperativo do Brasil"
    If B1.Exists(A1) Then
        Function3 = B1(A1)
    Else
        Function3 = "Banco " & A1
    End If
End Function

Function Function4(ByVal A1 As Double)
    Dim B1 As String
    Dim C1 As Long, C2 As Long
    C1 = Int(A1)
    C2 = ((A1 - C1) * 100)
    B1 = "[0]" & FormatCurrency(A1, 2) & "[/0] ("
    If C1 > 0 Then
        B1 = B1 & Function6(0, C1) & IIf(C1 = 1, " real", " reais")
    End If
    If C2 > 0 Then
        B1 = B1 & IIf(C1 > 0, " e ", "") & Function6(0, C2) & IIf(C2 = 1, " centavo", " centavos")
    End If
    B1 = B1 & ")"
    Function4 = B1
End Function

Function Function5(ByVal A1 As Object) As Object
    Dim B1 As Object, B2 As Object
    Dim C1 As Integer, C2 As Integer
    Dim D1 As Boolean
    Set B2 = CreateObject("Scripting.Dictionary")
    Set B1 = A1
    C1 = 1
    Do While B1.Count > 0
        D1 = False
        For Each E1 In B1.Keys
            If D1 = False Then
                C2 = E1
                D1 = True
            Else
                If B1(E1)("Index2") < B1(C2)("Index2") Then
                    C2 = E1
                End If
            End If
        Next
        B2.Add C1, B1(C2)
        B1.Remove C2
        C1 = C1 + 1
    Loop
    Set Function5 = B2
End Function

Function Function6(ByVal A1 As Integer, ByVal A2 As Long)
    Dim B1 As Object, B2 As Object, B3 As Object
    Dim C1 As Long, C2 As Long, C3 As Long, C4 As Long, C5 As Long, C6 As Long
    Dim D1 As String
    Set B1 = CreateObject("Scripting.Dictionary")
    Set B2 = CreateObject("Scripting.Dictionary")
    Set B3 = CreateObject("Scripting.Dictionary")
    B1.Add 1, "um"
    B1.Add 2, "dois"
    B1.Add 3, "três"
    B1.Add 4, "quatro"
    B1.Add 5, "cinco"
    B1.Add 6, "seis"
    B1.Add 7, "sete"
    B1.Add 8, "oito"
    B1.Add 9, "nove"
    B1.Add 10, "dez"
    B1.Add 20, "vinte"
    B1.Add 30, "trinta"
    B1.Add 40, "quarenta"
    B1.Add 50, "cinquenta"
    B1.Add 60, "sessenta"
    B1.Add 70, "setenta"
    B1.Add 80, "oitenta"
    B1.Add 90, "noventa"
    B1.Add 100, "cem"
    B1.Add 200, "duzentos"
    B1.Add 300, "trezentos"
    B1.Add 400, "quatrocentos"
    B1.Add 500, "quinhentos"
    B1.Add 600, "seiscentos"
    B1.Add 700, "setecentos"
    B1.Add 800, "oitocentos"
    B1.Add 900, "novecentos"
    B2.Add "dez e um", "onze"
    B2.Add "dez e dois", "doze"
    B2.Add "dez e três", "treze"
    B2.Add "dez e quatro", "quatorze"
    B2.Add "dez e cinco", "quinze"
    B2.Add "dez e seis", "dezesseis"
    B2.Add "dez e sete", "dezessete"
    B2.Add "dez e oito", "dezoito"
    B2.Add "dez e nove", "dezenove"
    B2.Add "cem e", "cento e"
    B3.Add "um", "uma"
    B3.Add "dois", "duas"
    B3.Add "duzentos", "duzentas"
    B3.Add "trezentos", "trezentas"
    B3.Add "quatrocentos", "quatrocentas"
    B3.Add "quinhentos", "quinhentas"
    B3.Add "seiscentos", "seiscentas"
    B3.Add "setecentos", "setecentas"
    B3.Add "oitocentos", "oitocentas"
    B3.Add "novecentos", "novecentas"
    C1 = (Int(A2 / 100000) * 100)
    C2 = (Int((A2 - (C1 * 1000)) / 10000) * 10)
    C3 = Int((A2 - (C1 * 1000) - (C2 * 1000)) / 1000)
    C4 = (Int((A2 - (C1 * 1000) - (C2 * 1000) - (C3 * 1000)) / 100) * 100)
    C5 = (Int((A2 - (C1 * 1000) - (C2 * 1000) - (C3 * 1000) - C4) / 10) * 10)
    C6 = (A2 - (C1 * 1000) - (C2 * 1000) - (C3 * 1000) - C4 - C5)
    D1 = ""
    If C1 > 0 Then
        D1 = D1 & B1(C1)
    End If
    If C2 > 0 Then
        D1 = D1 & IIf(C1 > 0, " e ", "") & B1(C2)
    End If
    If C3 > 0 Then
        D1 = D1 & IIf(C1 > 0 Or C2 > 0, " e ", "") & B1(C3)
    End If
    If C1 > 0 Or C2 > 0 Or C3 > 0 Then
        D1 = D1 & " mil"
        If C4 > 0 Or C5 > 0 Or C6 > 0 Then
            If (C4 = 0 And (C5 > 0 Or C6 > 0)) Or (C4 > 0 And C5 = 0 And C6 = 0) Then
                D1 = D1 & " e "
            Else
                D1 = D1 & ", "
            End If
        End If
    End If
    If C4 > 0 Then
        D1 = D1 & B1(C4)
    End If
    If C5 > 0 Then
        D1 = D1 & IIf(C4 > 0, " e ", "") & B1(C5)
    End If
    If C6 > 0 Then
        D1 = D1 & IIf(C4 > 0 Or C5 > 0, " e ", "") & B1(C6)
    End If
    For Each E1 In B2.Keys
        D1 = Replace(D1, E1, B2(E1))
    Next
    If A1 = 1 Then
        For Each E2 In B3.Keys
            D1 = Replace(D1, E2, B3(E2))
        Next
    End If
    Function6 = D1
End Function

Function Function7(ByVal A1 As Integer, Optional ByVal A2 As Integer, Optional ByVal A3 As Integer) As Object
    Dim B1 As Object
    Dim C1() As String
    Set B1 = CreateObject("Scripting.Dictionary")
    B1.Add "Index1", Function1("Entre com o protocolo do atendimento " & IIf(A1 = 0, "humano", IIf(A1 = 1, "eletrônico", "")) & " (opcional) (" & A2 & " de " & A3 & "):", 11, False)
    C1 = Split(CStr(Function1("Entre com a data e a hora do atendimento " & IIf(A1 = 0, "humano", IIf(A1 = 1, "eletrônico", "")) & " (" & A2 & " de " & A3 & "):", 12, True)), " ")
    B1.Add "Index2", (DateValue(C1(0)) + TimeValue(C1(1)))
    If A1 = 0 Then
        B1.Add "Index3", Function1("Entre com o motivo do atendimento humano (opcional) (" & A2 & " de " & A3 & "):", 0, False)
        B1.Add "Index4", Function1("Entre com a bina do atendimento humano (opcional) (" & A2 & " de " & A3 & "):", 13, False)
        B1.Add "Index5", Function1("Entre com o atendente do atendimento humano (opcional) (" & A2 & " de " & A3 & "):", 0, False)
        B1.Add "Index6", Function1("Entre com a central do atendimento humano (opcional) (" & A2 & " de " & A3 & ") (1 = SAC, 2 = RELACIONAMENTO CARTÃO):", 14, False)
        B1.Add "Index7", Function1("Entre com a descrição da solicitação do cliente do atendimento humano (opcional) (" & A2 & " de " & A3 & "):", 17, False)
        B1.Add "Index8", Function1("Entre com a informação passada pelo atendente do atendimento humano (opcional) (" & A2 & " de " & A3 & "):", 17, False)
    ElseIf A1 = 1 Then
        B1.Add "Index3", Function1("Entre com o motivo do atendimento eletrônico (opcional) (" & A2 & " de " & A3 & "):", 0, False)
        B1.Add "Index4", Function1("Entre com a bina do atendimento eletrônico (opcional) (" & A2 & " de " & A3 & "):", 13, False)
        B1.Add "Index5", Function8("atendimento eletrônico (" & A2 & " de " & A3 & "):", False)
    End If
    Set Function7 = B1
End Function

Function Function8(ByVal A1 As String, ByVal A2 As Boolean) As Object
    Dim B1 As Object, B2 As Object
    Dim C1 As Integer, C2 As Boolean
    Set B1 = Application.FileDialog(msoFileDialogFilePicker)
    Set B2 = CreateObject("Scripting.Dictionary")
    With B1
        .Title = Const1 & " - Selecione a captura de tela (" & A1 & ")"
        .AllowMultiSelect = A2
        With .Filters
            .Clear
            .Add "Arquivos de Imagem", "*.png"
        End With
        .InitialFileName = ThisDocument.Path & "\"
        C2 = True
        .Show
        If .SelectedItems.Count > 0 Then
            For C1 = 1 To .SelectedItems.Count
                B2.Add C1, .SelectedItems(C1)
            Next C1
        End If
    End With
    Set Function8 = B2
End Function

Function Function9(ByVal A1 As String) As Boolean
    Dim B1 As Integer
    Do
        B1 = MsgBox(A1, vbYesNoCancel, Const1)
        If B1 = vbCancel Then
            If MsgBox("Tem certeza que deseja cancelar?", vbYesNo, Const1) = vbYes Then
                End
            End If
        ElseIf B1 = vbYes Then
            Function9 = True
            Exit Do
        ElseIf B1 = vbNo Then
            Function9 = False
            Exit Do
        End If
    Loop
End Function

Function Function10(ByVal A1 As String, ByVal A2 As Integer, ByVal A3 As String) As String
    Dim B1 As New UserForm2
    With B1
        .Caption = Const1
        With .Label1
            .Caption = A1
            .AutoSize = True
            .AutoSize = False
        End With
        .TextBox1.Top = (.Label1.Height + 19.25)
        With .TextBox1
            If A2 = 1 Then
                .PasswordChar = "*"
            ElseIf A2 = 2 Then
                .MultiLine = True
                .Height = 130
                .WordWrap = True
            End If
            .Value = A3
        End With
        If A2 = 0 Or A2 = 1 Then
            .CommandButton1.Top = (.Label1.Height + 45.25)
            .CommandButton2.Top = (.Label1.Height + 45.25)
            .Height = (.Label1.Height + 105)
        ElseIf A2 = 2 Then
            .CommandButton1.Top = (.Label1.Height + 159)
            .CommandButton2.Top = (.Label1.Height + 159)
            .Height = (.Label1.Height + 219.25)
        End If
        .Show
        Function10 = .A1
    End With
End Function

Function Function11(ByVal A1 As Integer, ByVal A2 As String, Optional ByVal A3 As String = "", Optional ByVal A4 As String = "", Optional ByVal A5 As Boolean) As Boolean
    Dim B1 As Object
    Set B1 = CreateObject("VBScript.RegExp")
    With ThisDocument
        If A1 = 1 Then
            If A5 = True Then
                If .A2.Exists(A2 & "," & A4) = True Then
                    .A3 = .A2(A2 & "," & A4)
                    Function11 = True
                    Exit Function
                End If
            End If
        End If
        With .A1
            If A1 = 0 Then
                .Open "GET", A2, True
            ElseIf A1 = 1 Then
                .Open "POST", A2, True
            End If
            .Option(4) = 13056
            .SetRequestHeader "User-Agent", "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0"
            If A3 <> "" Then
                .SetRequestHeader "Content-Type", A3
            End If
            .SetTimeouts 0, 120000, 60000, 60000
            Do
                B1.Pattern = "^https?:\/\/(.+?)\/.*"
                Call Sub3(0, "Comunicando com " & B1.Replace(A2, "$1") & "...")
                On Error Resume Next
                If A1 = 0 Then
                    .Send
                ElseIf A1 = 1 Then
                    .Send A4
                End If
                .WaitForResponse
                Call Sub3(1)
                If Err = 0 Then
                    If .Status <> 500 Then
                        If A1 = 1 Then
                            If A5 = True Then
                                ThisDocument.A2.Add A2 & "," & A4, .ResponseText
                                ThisDocument.A3 = .ResponseText
                            End If
                        End If
                        Function11 = True
                        Exit Do
                    Else
                        If MsgBox("Ocorreu um erro de servidor. Deseja tentar novamente?", vbYesNo, Const1) = vbNo Then
                            Function11 = False
                            Exit Do
                        End If
                    End If
                Else
                    If MsgBox("Ocorreu um erro de comunicação. Deseja tentar novamente?", vbYesNo, Const1) = vbNo Then
                        Function11 = False
                        Exit Do
                    End If
                End If
            Loop
        End With
    End With
End Function

Function Function12(ByVal A1 As String) As Variant
    Dim B1 As Object, B2 As Object
    Dim C1() As String, C2() As String
    Dim D1 As Integer
    Set B1 = CreateObject("VBScript.RegExp")
    Set B2 = CreateObject("Scripting.Dictionary")
    With B1
        .Pattern = "^\{(.+)\}$"
        If .Execute(A1).Count > 0 Then
            C1 = Split(.Replace(A1, "$1"), ",")
            For Each E1 In C1
                C2 = Split(E1, ":")
                B2.Add Function12(C2(0)), Function12(C2(1))
            Next
            Set Function12 = B2
        Else
            .Pattern = "^\[(.+)\]$"
            If .Execute(A1).Count > 0 Then
                C1 = Split(.Replace(A1, "$1"), ",")
                For D1 = LBound(C1) To UBound(C1)
                    B2.Add (D1 + 1), Function12(C1(D1))
                Next D1
                Set Function12 = B2
            Else
                .Pattern = "^""(.+)""$"
                If .Execute(A1).Count > 0 Then
                    Function12 = .Replace(A1, "$1")
                Else
                    .Pattern = "^\d+$"
                    If .Execute(A1).Count > 0 Then
                        Function12 = CInt(A1)
                    Else
                        If A1 = "false" Then
                            Function12 = False
                        ElseIf A1 = "true" Then
                            Function12 = True
                        End If
                    End If
                End If
            End If
        End If
    End With
End Function

Function Function13(ByVal A1 As Integer, ByVal A2 As String) As String
    Dim B1 As Object
    Dim C1 As String
    Set B1 = CreateObject("VBScript.RegExp")
    With B1
        .Global = True
        If A1 = 0 Then
            .Pattern = "[-\.]"
            Function13 = .Replace(A2, "")
        ElseIf A1 = 1 Then
            .Pattern = " "
            C1 = .Replace(A2, "")
            .Pattern = "[ÁÂÃ]"
            C1 = .Replace(C1, "A")
            .Pattern = "Ç"
            C1 = .Replace(C1, "C")
            .Pattern = "[ÉÊ]"
            C1 = .Replace(C1, "E")
            .Pattern = "Í"
            C1 = .Replace(C1, "I")
            .Pattern = "[ÓÔÕ]"
            C1 = .Replace(C1, "O")
            .Pattern = "[ÚÜ]"
            Function13 = .Replace(C1, "U")
        End If
    End With
End Function

Function Function14(ByVal A1 As String) As Object
    Dim B1() As String
    Dim C1 As Integer
    Dim D1 As Object
    Set D1 = CreateObject("Scripting.Dictionary")
    B1 = Split(A1, ",")
    For C1 = LBound(B1) To UBound(B1)
        D1.Add (C1 + 1), B1(C1)
    Next C1
    Set Function14 = D1
End Function

Function Function15(ByVal A1 As Object) As Boolean
    Dim B1 As Boolean
    B1 = False
    If ThisDocument.Variables("B6") = 1 Then
        If A1("Index3") = True Then
            If A1("Index4")("Index3") = 2 Or A1("Index4")("Index3") = 3 Then
                B1 = True
            End If
        End If
        If B1 = False Then
            If A1("Index5") > 0 Then
                For Each C1 In A1("Index6").Keys
                    If A1("Index6")(C1)("Index3") = 2 Or A1("Index6")(C1)("Index3") = 3 Then
                        B1 = True
                        Exit For
                    End If
                Next C1
            End If
        End If
    End If
    Function15 = B1
End Function

Function Function22(ByVal A1 As Integer, Optional ByVal A2 As Integer, Optional ByVal A3 As Integer, Optional ByVal A4 As Integer = 0, Optional ByVal A5 As String = "", Optional ByVal A6 As String = "", Optional ByVal A7 As String = "") As Object
    Dim B1 As Object, B2 As Object, B3 As Object
    Dim C1 As Integer, C2 As Boolean
    Set B1 = CreateObject("Scripting.Dictionary")
    Set B2 = CreateObject("Scripting.Dictionary")
    Set B3 = CreateObject("Scripting.Dictionary")
    B1.Add "Index2", Function1("Entre com o número do contrato:", 11, True)
    B1.Add "Index3", DateValue(CStr(Function1("Entre com a data do emprestimo:", 4, True)))
    B1.Add "Index4", Function1("Entre com o valor do emprestimo:", 5, True)
    B1.Add "Index5", Function1("Entre com a quantidade de parcelas:", 10, True)
    B1.Add "Index6", Function1("Entre com o valor da parcela:", 5, True)
    B1.Add "Index7", Function1("Entre com o valor disponibilizado:", 5, True)
    B1.Add "Index8", Function9("Têm seguro?")
    If B1("Index8") Then
        B1.Add "Index9", Function1("Entre com o valor do seguro:", 5, True)
    End If
    B1.Add "Index10", 1 'Function1("Entre com a forma de crédito (1 = DOC/TED/CC, 2 = OP, 3 = Cheque):", 6, True, IIf(A4 <> 0, A4, ""))
    B1.Add "Index11", Function1("Entre com o banco da transferência bancária:", 7, True)
    B1.Add "Index12", Function1("Entre com a agência da transferência bancária:", 8, True)
    B1.Add "Index13", Function1("Entre com a conta da transferência bancária:", 9, True)
    B1.Add "Index14", Function8("emprestimo", False)
    B1.Add "Index16", Function9("Têm desconto do empréstimo em conta?")
    If B1("Index16") Then
        B1.Add "Index17", DateValue(CStr(Function1("Entre com a primeira data do desconto:", 4, True)))
        B1.Add "Index18", Function1("Entre com o primeiro valor do desconto:", 5, True)
        B1.Add "Index19", DateValue(CStr(Function1("Entre com a última data do desconto:", 4, True)))
        B1.Add "Index20", Function1("Entre com o ultimo valor do desconto:", 5, True)
        B1.Add "Index21", Function8("Desconto", False)
    End If
    B1.Add "Index26", Function9("Houve refinanciamento?")
    If B1("Index26") = True Then
        C2 = True
        C1 = 0
        Do
            C1 = C1 + 1
            B3.Add C1, Function23(1, C1, C1)
            C2 = Function9("Outro refinanciamento?")
        Loop While C2
        B1.Add "Index27", Function5(B3)
    End If
    Set Function22 = B1
End Function